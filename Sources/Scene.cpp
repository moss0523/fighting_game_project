
#include <sstream>

#include "../Headers/Scene.h"
#include "../GameLib/Headers/DxAudio.h"
#include "../GameLib/Headers/DxSystem.h"
#include "../GameLib/Headers/InputManager.h"
#include "../GameLib/Headers/ImGui.h"

Scene* Scene::Execute(HWND hwnd)
{




	MSG hMsg = { 0 };
	float Interval = 1.0f;
	DWORD before = GetTickCount();
	int fps = 0;
	

	// デバイス初期化


	//サウンド読み込み
	dxAudio->SoundLoad(L"Assets/Sound/Sound.xwb", 1);

	Init();


	//メインループ
	while (hMsg.message != WM_QUIT) {



		if (PeekMessage(&hMsg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&hMsg);
			DispatchMessage(&hMsg);
		}
		else
		{
			//フレームレート計算、表示/////////////////////////////////////////////////////////
			DxSystem::elapsedTime =
				(GetTickCount() - before) * 0.001f;

			before = GetTickCount();
			float mspf = 1000.0f / fps;

			Interval -= DxSystem::elapsedTime;
			fps++;
			if (Interval < 0) {
				std::ostringstream outs;
				outs.precision(6);
				outs << "FPS : " << fps << " / " << "Frame Time : " << mspf << " (ms)";
				SetWindowTextA(hwnd, outs.str().c_str());

				Interval += 1.0f;
				fps = 0;
			}
			/////////////////////////////////////////////////////////////////////////////
			
		
			//インプットマネージャー更新(移動値計算のため1フレ前の座標を取得
			inputManager->oldCursorPos=inputManager->GetCursorPosXY();
			inputManager->oldStick[0].x = inputManager->GetPadAddress()[0].leftX;//キートリガー
			inputManager->oldStick[0].y = inputManager->GetPadAddress()[0].leftY;//キートリガー
			inputManager->oldStick[1].x = inputManager->GetPadAddress()[1].leftX;//キートリガー
			inputManager->oldStick[1].y = inputManager->GetPadAddress()[1].leftY;//キートリガー
			inputManager->oldRTrigger[0] = inputManager->GetPadAddress()[0].right;//キートリガー
			inputManager->oldRTrigger[0] = inputManager->GetPadAddress()[1].left;//キートリガー
			inputManager->oldRTrigger[1] = inputManager->GetPadAddress()[0].right;//キートリガー
			inputManager->oldRTrigger[1] = inputManager->GetPadAddress()[1].left;//キートリガー

			inputManager->Update(hwnd);

			//更新と描画
			Update();
			Render();

			// 終了チェック
			if (nextScene) 
				break;
		}

	}

	// 終了処理
	UnInit();

	return nextScene;

}

// シーン変更処理
void Scene::SceneChange(Scene *scene) { 
	nextScene = scene; 
}   

//シーン変更、存在判定
void SceneManager::Execute(HWND hwnd ,Scene *scene)
{


	// デバイス初期化
	if (FAILED(DxSystem::Initialize(hwnd, DxSystem::SCREEN_WIDTH, DxSystem::SCREEN_HEIGHT)))
	{
		return;
	}
	//Imguiの初期化

	// setup imgui
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	//日本語用フォントの設定
	io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\meiryo.ttc", 13.0f, nullptr, glyphRangesJapanese);
	// setup platform/renderer
	ImGui_ImplWin32_Init(hwnd);
	ImGui_ImplDX11_Init(DxSystem::device, DxSystem::deviceContext);
	ImGui::StyleColorsDark();

	//インプットマネージャー初期化
	inputManager->Init();


	while (scene)
	{
		scene = scene->Execute(hwnd);
	}

	//システムとImGuiの終了処理
	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
	DxSystem::Release();
}
