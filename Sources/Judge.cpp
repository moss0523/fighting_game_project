#include "../Headers/Judge.h"
#include"../Headers/SceneBattle.h"
#include "../Headers/Effect.h"

//******************************************************************************
//
//								あたり判定クラス
//
//******************************************************************************

//当たり判定本体(バトル中に実際に呼ぶやつ
void Judge::Execute()
{

	//プレイヤー情報へのポインタ
	Player* p1 = sceneBattle->player1;// 1P
	Player* p2 = sceneBattle->player2;// 2P


	//コンボリセット
	if (p2->GetRigidFrame() <= 0)
		p1->SetComboCnt(0);
	if (p1->GetRigidFrame() <= 0)
		p2->SetComboCnt(0);



	//当たり判定を行います
	//投げが優先なため投げから判定
	JudgeThrow(p1, p2);
	JudgeThrow(p2, p1);

	//攻撃判定
	JudgeHit(p1, p2);
	JudgeHit(p2, p1);

	//飛び道具判定
	JudgeShot(p1, p2);
	JudgeShot(p2, p1);




	//押し合い
	//触れている場合スピードの速いほうが押しフラグオン
	JudgePush(p1, p2);

}





//******************************************************************************
//							プライベートメソッド
//******************************************************************************


//アクティブでないやつは無視
bool Judge::IsIgnore(Player * player, int num, const int RECT_TYPE)
{
	if (player->GetRectData(num).motionName != player->GetMesh()->motionName)return true;//モーションが違えば無視
	if (player->GetRectData(num).type != RECT_TYPE)return true;//攻撃判定でなければ無視
	if (player->GetFrameCnt() < player->GetRectData(num).start ||
		(player->GetFrameCnt() > player->GetRectData(num).start + player->GetRectData(num).keep))return true;//持続フレーム間でなければ無視

	return false;
}

//攻撃判定
void Judge::JudgeHit(Player*attackPl, Player*damagePl)
{

	if (IsNoHit(attackPl, damagePl))return;
	if ((attackPl->GetGrdType() == GRD_TYPE::THROW))return;

	//1P->2Pへの攻撃
	for (int attackNum = 0; attackPl->GetRectData(attackNum).size.x > 0; attackNum++)
	{

		//一括で条件外のものを弾く
		if (IsIgnore(attackPl, attackNum, TYPE_HIT))continue;


		for (int damageNum = 0; damagePl->GetRectData(damageNum).size.x > 0; damageNum++)
		{

			//一括で条件外のものを弾く
			if (IsIgnore(damagePl, damageNum, TYPE_HURT))continue;

			//当たり判定が重なっていれば
			if (IsHitPlayer(
				attackPl->GetRectData(attackNum),
				attackPl,
				damagePl->GetRectData(damageNum),
				damagePl))
			{


				//ヒットフラグオン
				attackPl->SetIsHit(true);

				//描画優先度決定決定
				damagePl->SetIsPrioritize(false);
				attackPl->SetIsPrioritize(true);


				CreateHitEffect(attackPl, damagePl, damageNum);


				//ダメージでなければガード
				if (JudgeGuard(damagePl))
				{
					//ガード予約
					damagePl->SetIsGuard(true);
					damagePl->GuardReserve();

				}
				else {
					//ダメージ予約
					damagePl->SetIsDamage(true);
					damagePl->DamageReserve();
					//ヒットストップ
					attackPl->AddComboCnt();
				}
			}
		}
	}
}


void Judge::JudgeShot(Player*attackPl, Player*damagePl)
{
	if ((damagePl->GetIsInvisible()))return;


	//1P->2Pへの攻撃
	for (auto& p : *effectManager)
	{
		//当たり判定がないものは無視する
		if (p.hitRect.size.x <= 0)continue;


		for (int damageNum = 0; damagePl->GetRectData(damageNum).size.x > 0; damageNum++)
		{

			//一括で条件外のものを弾く
			if (IsIgnore(damagePl, damageNum, TYPE_HURT))continue;

			//当たり判定が重なっていれば
			if (IsHit2D(
				&p,
				damagePl->GetRectData(damageNum),
				damagePl))
			{
				//描画優先度決定決定
				damagePl->SetIsPrioritize(false);
				attackPl->SetIsPrioritize(true);

				//エフェクト生成
				CreateHitEffect(attackPl, damagePl, damageNum);
				//当たったので削除
				p.Clear();

				//ダメージでなければガード
				if (JudgeGuard(damagePl))
				{
					//ガード予約
					damagePl->SetIsGuard(true);
					damagePl->GuardReserve(true);

				}
				else {
					//ダメージ予約
					damagePl->SetIsDamage(true);
					damagePl->DamageReserve(true);
					//コンボ加算
					attackPl->AddComboCnt();

				}
			}
		}
	}
}


void Judge::JudgeThrow(Player*attackPl, Player*damagePl)
{

	if (IsNoHit(attackPl, damagePl))return;
	if ((attackPl->GetGrdType() != GRD_TYPE::THROW))return;

	//1P->2Pへの攻撃
	for (int attacNum = 0; attackPl->GetRectData(attacNum).size.x > 0; attacNum++)
	{

		//一括で条件外のものを弾く
		if (IsIgnore(attackPl, attacNum, TYPE_HIT))continue;


		for (int damageNum = 0; damagePl->GetRectData(damageNum).size.x > 0; damageNum++)
		{
			//一括で条件外のものを弾く
			if (IsIgnore(damagePl, damageNum, TYPE_HURT))continue;




			//当たっていればTRUE
			if (!attackPl->GetIsHit() &&
				IsHitPlayer(
					attackPl->GetRectData(attacNum),
					attackPl,
					damagePl->GetRectData(damageNum),
					damagePl))
			{



				if (!attackPl->GetIsThrowComp() &&
					(damagePl->GetMoveState() == PLAYER_MOVE::ATTACK_THROW))
				{
					attackPl->SetIsThrowComp(true);
					attackPl->MoveThrowComp();

					attackPl->GetMesh()->Play(PLAYER_MOTION::DAMAGE);
					damagePl->GetMesh()->Play(PLAYER_MOTION::DAMAGE);

					attackPl->SetMoveState(PLAYER_MOVE::DAMAGE);
					damagePl->SetMoveState(PLAYER_MOVE::DAMAGE);

					//ヒットエフェクト生成
					{
						Obj2D* child = effectManager->Create(EffectManager::Move::Hit, (attackPl->GetPosition() + damagePl->GetPosition()) / 2);
						child->parent3D = attackPl;
					}

					//	投げ相殺硬直(お互い同じ
					attackPl->SetRigidFrame(THROW_COMP_FRAME);
					damagePl->SetRigidFrame(THROW_COMP_FRAME);

					sceneBattle->SetHitStop(attackPl->GetStopFrame());
					continue;
				}


				//ヒットフラグオン
				attackPl->SetIsHit(true);

				//描画優先度決定決定
				damagePl->SetIsPrioritize(false);
				attackPl->SetIsPrioritize(true);

				attackPl->SetIsInvisible(true);

				//ヒットエフェクト生成
				effectManager->Create(EffectManager::Move::Hit, VECTOR2(damagePl->GetDir()*(damagePl->GetRectData(damageNum).position.x) + damagePl->GetPosition().x,
					damagePl->GetRectData(damageNum).position.y + damagePl->GetPosition().y));

				if (attackPl->GetIsPowerUp())
					effectManager->Create(EffectManager::Move::HitPow, VECTOR2(damagePl->GetDir()*(damagePl->GetRectData(damageNum).position.x) + damagePl->GetPosition().x,
						damagePl->GetRectData(damageNum).position.y + damagePl->GetPosition().y));


				//ダメージ予約
				damagePl->DamageReserve();

				damagePl->SetIsDamage(true);
				attackPl->AddComboCnt();

			}
		}
	}

}
//押し合い判定
void Judge::JudgePush(Player * p1, Player * p2)
{

	//押し合い判定を行わない時は無視する
	if (p1->GetIsNotPush() || p2->GetIsNotPush())
	{
		p1->SetIsPush(false);
		p2->SetIsPush(false);
		return;
	}
	if (IsHitPlayerPush(
		p1->GetPushRectData(),
		p1,
		p2->GetPushRectData(),
		p2)) {


		p1->SetIsPush(true);
		p2->SetIsPush(true);

	}
	else {
		p1->SetIsPush(false);
		p2->SetIsPush(false);
	}
}

//ガード可能かどうか
bool Judge::JudgeGuard(Player * pl)
{

	DWORD keyState = pl->GetKeyState();//キーステート

	//ガード不能状態ならreturn false
	if (pl->GetMoveState() == PLAYER_MOVE::DAMAGE)return false;		//ダメージ中
	if (pl->GetPlayerState() == PLAYER_STATE::ATTACK)return false;	//攻撃中
	if (pl->GetIsAir())return false;								//空中
	if (!(keyState&Input::PAD_LEFT))return false;					//そもそもガードしてない
	if (pl->GetMoveState() == PLAYER_MOVE::ATEMI_READY)return false;

	//ガード可能かどうかを判定
	//可能でなければ return false
	switch (pl->GetEnemy()->GetGrdType())
	{
		//下段
	case GRD_TYPE::LOW:

		if (!(keyState&Input::PAD_DOWN))return false;
		break;
		//上段
	case GRD_TYPE::UP:
		if ((keyState&Input::PAD_DOWN))return false;
		break;
		//その他
	case GRD_TYPE::ALL:
		break;

	}
	return true;


}
//ヒットエフェクト生成
void Judge::CreateHitEffect(Player*attackPl, Player*damagePl, int damageNum)
{
	Obj2D* child = effectManager->Create(EffectManager::Move::Hit, VECTOR2(damagePl->GetDir()*(damagePl->GetRectData(damageNum).position.x) + damagePl->GetPosition().x,
		damagePl->GetRectData(damageNum).position.y + damagePl->GetPosition().y));
	child->parent3D = attackPl;

	if (attackPl->GetIsPowerUp())
		effectManager->Create(EffectManager::Move::HitPow, VECTOR2(damagePl->GetDir()*(damagePl->GetRectData(damageNum).position.x) + damagePl->GetPosition().x,
			damagePl->GetRectData(damageNum).position.y + damagePl->GetPosition().y));

}

//無敵や高度制限で攻撃が当たらない時
bool Judge::IsNoHit(Player * attackPl, Player * damagePl)
{

	//ジャンプ中は投げ無敵
	if ((attackPl->GetGrdType() == GRD_TYPE::THROW) &&
		damagePl->GetIsAir())return true;
	//高度制限
	if ((attackPl->GetPosition().y > AIR_ATTACK_LIMIT) &&
		attackPl->GetIsAir())return true;
	//無敵
	if ((damagePl->GetIsInvisible()))return true;

	//ダメージ硬直中、ガード硬直中は投げ無敵
	if ((attackPl->GetGrdType() == GRD_TYPE::THROW) &&
		(damagePl->GetMoveState() == PLAYER_MOVE::DAMAGE))return true;
	if ((attackPl->GetGrdType() == GRD_TYPE::THROW) &&
		(damagePl->GetMoveState() == PLAYER_MOVE::GUARD))return true;


	return false;
}

//矩形vs矩形で当たってるかどうか
bool Judge::IsHitPlayer(JudgeRect rectA, Player* parentA, JudgeRect rectD, Player* parentD)
{

	//すでに当たっている場合判定しない
	if (parentA->GetIsHit())return false;


	//プレイヤーの座標
	VECTOR2 posA = parentA->GetPosition();
	VECTOR2 posD = parentD->GetPosition();

	//プレイヤーがどちらを向いているか
	float dirA = parentA->GetDir();
	float dirD = parentD->GetDir();


	//座標データ---------------------------------------------------------------------

	//攻撃側
	float rectLeft_A = posA.x + (rectA.position.x)*dirA - rectA.size.x / 2.f;
	float rectRight_A = posA.x + (rectA.position.x)*dirA + rectA.size.x / 2.f;
	float rectTop_A = posA.y + (rectA.position.y) - rectA.size.y / 2.f;
	float rectBottom_A = posA.y + (rectA.position.y) + rectA.size.y / 2.f;
	//ダメージ側
	float rectLeft_D = posD.x + (rectD.position.x)*dirD - rectD.size.x / 2.f;
	float rectRight_D = posD.x + (rectD.position.x)*dirD + rectD.size.x / 2.f;
	float rectTop_D = posD.y + (rectD.position.y) - rectD.size.y / 2.f;
	float rectBottom_D = posD.y + (rectD.position.y) + rectD.size.y / 2.f;



	//当たってなければ早期リターン
	if (rectRight_A < rectLeft_D) return false;
	if (rectRight_D < rectLeft_A) return false;
	if (rectBottom_A < rectTop_D) return false;
	if (rectBottom_D < rectTop_A) return false;


	//ここまでくれば当たってる
	return true;
}

bool Judge::IsHitPlayerPush(JudgeRect rectA, Player* parentA, JudgeRect rectD, Player* parentD)
{


	//プレイヤーの座標
	VECTOR2 posA = parentA->GetPosition();
	VECTOR2 posD = parentD->GetPosition();

	//プレイヤーがどちらを向いているか
	float dirA = parentA->GetDir();
	float dirD = parentD->GetDir();


	//座標データ---------------------------------------------------------------------

	//攻撃側
	float rectLeft_A = posA.x + (rectA.position.x)*dirA - rectA.size.x / 2.f;
	float rectRight_A = posA.x + (rectA.position.x)*dirA + rectA.size.x / 2.f;
	float rectTop_A = posA.y + (rectA.position.y) - rectA.size.y / 2.f;
	float rectBottom_A = posA.y + (rectA.position.y) + rectA.size.y / 2.f;
	//ダメージ側
	float rectLeft_D = posD.x + (rectD.position.x)*dirD - rectD.size.x / 2.f;
	float rectRight_D = posD.x + (rectD.position.x)*dirD + rectD.size.x / 2.f;
	float rectTop_D = posD.y + (rectD.position.y) - rectD.size.y / 2.f;
	float rectBottom_D = posD.y + (rectD.position.y) + rectD.size.y / 2.f;



	//当たってなければ早期リターン
	if (rectRight_A < rectLeft_D) return false;
	if (rectRight_D < rectLeft_A) return false;
	if (rectBottom_A < rectTop_D) return false;
	if (rectBottom_D < rectTop_A) return false;


	//ここまでくれば当たってる
	return true;
}



//矩形vs2Dobjで当たってるかどうか
bool Judge::IsHit2D(Obj2D * obj, JudgeRect rectD, Player * parentD)
{


	//プレイヤーの座標
	VECTOR2 posA = obj->position;
	VECTOR2 posD = parentD->GetPosition();

	//親情報
	Player*parent = obj->parent3D;

	//方向フラグ
	float dirD = parentD->GetDir();


	//座標データ---------------------------------------------------------------------

	//攻撃側
	float rectLeft_A = posA.x - obj->hitRect.size.x / 2;
	float rectRight_A = posA.x + obj->hitRect.size.x / 2;
	float rectTop_A = posA.y - obj->hitRect.size.y / 2;
	float rectBottom_A = posA.y + obj->hitRect.size.y / 2;
	//ダメージ側
	float rectLeft_D = posD.x + (rectD.position.x)*dirD - rectD.size.x / 2.f;
	float rectRight_D = posD.x + (rectD.position.x)*dirD + rectD.size.x / 2.f;
	float rectTop_D = posD.y + (rectD.position.y) - rectD.size.y / 2.f;
	float rectBottom_D = posD.y + (rectD.position.y) + rectD.size.y / 2.f;

	//自分が生成したものとは行わない
	if (parent == parentD)return false;

	//当たってなければ早期リターン
	if (rectRight_A < rectLeft_D) return false;
	if (rectRight_D < rectLeft_A) return false;
	if (rectBottom_A < rectTop_D) return false;
	if (rectBottom_D < rectTop_A) return false;


	//ここまでくれば当たってる
	return true;
}



