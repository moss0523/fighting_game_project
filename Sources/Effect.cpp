#include "../Headers/Effect.h"
#include "../Headers/SceneBattle.h"
#include "../Headers/SpriteData.h"

#include "../Headers/Common.h"


//*******************************************************
//
//	定数
//
//*******************************************************

//ヒットエフェクトの大きさ
static const VECTOR2 HIT_EFFECT_SCALE_MIN = VECTOR2(0.8f, 0.8f);//小
static const VECTOR2 HIT_EFFECT_SCALE_MID = VECTOR2(1.2f, 1.2f);//中
static const VECTOR2 HIT_EFFECT_SCALE_MAX = VECTOR2(1.3f, 1.3f);//大

//閾値(この値より大きくなったらエフェクト可変
static const float EFFECT_CHANGE_ATTRCK_MIN = 15.f;
static const float EFFECT_CHANGE_ATTRCK_MAX = 30.f;

static const float SHOT_SPEED = 10;//弾の速さ
static const float SHOT_LIFE_TIME = 140;//生存時間

static const VECTOR2 SHOT_RECT_SIZE = VECTOR2(250.f, 250.f);//大

void  EffectManager::Move::Hit(Obj2D * obj)
{
	switch (obj->state) {

	case MOVE_STATE::INIT:


		obj->scale = ORIGINAL_SCALE;

		obj->isAdd = true;
		obj->color = VECTOR4(1, 1, 1, 1);
		obj->isScroll = true;

		obj->state++;
		//break; 意図的

	case MOVE_STATE::MAIN:


		AnimeData*anime = nullptr;

		anime = AnimeHitEffect;
		if (obj->AnimeUpdate(anime))obj->Clear();
		break;
	}


}
void  EffectManager::Move::HitPow(Obj2D * obj)
{
	switch (obj->state) {

	case MOVE_STATE::INIT:


		obj->scale = VECTOR2(3,3);

		obj->isAdd = true;
		obj->color = VECTOR4(1, 0.3f, 0.3f, 1);
		obj->isScroll = true;

		obj->state++;
		//break; 意図的

	case MOVE_STATE::MAIN:


		AnimeData*anime = nullptr;

		anime = AnimeHitEffect;
		if (obj->AnimeUpdate(anime))obj->Clear();
		break;
	}


}
void  EffectManager::Move::Storm(Obj2D * obj)
{

	switch (obj->state) {

	case MOVE_STATE::INIT:

		obj->isScroll = true;
		obj->isAdd = true;
		obj->isRect = true;
		obj->scale = HALF_UP_SCALE;
		obj->hitRect.size = SHOT_RECT_SIZE;
		obj->dir = (int)obj->parent3D->GetDir();
		obj->state++;
		//break; 意図的

	case MOVE_STATE::MAIN:

		AnimeData*anime = nullptr;
		anime = AnimeWind;
		obj->AnimeUpdate(anime);
		obj->position.x += SHOT_SPEED * obj->dir;

		break;
	}
	//持続時間を過ぎれば消滅する
	if (++obj->timer > SHOT_LIFE_TIME)obj->Clear();


}

void EffectManager::Move::Cancel(Obj2D * obj)
{

	switch (obj->state) {

	case MOVE_STATE::INIT:
		obj->isScroll = true;
		obj->isAdd = true;
		obj->scale = HALF_UP_SCALE;
		obj->state++;
		//break; 意図的

	case MOVE_STATE::MAIN:

		AnimeData*anime = nullptr;
		anime = AnimeCanel;
		if (obj->AnimeUpdate(anime))obj->Clear();

		break;
	}
	//持続時間を過ぎれば消滅する

}


//パワーアップ
void EffectManager::Move::PowerUp(Obj2D * obj)
{

	switch (obj->state) {

	case MOVE_STATE::INIT:
		obj->isScroll = true;
		obj->isAdd = true;
		obj->scale = VECTOR2(3,3);
		obj->isFront = true;

		obj->state++;
		//break; 意図的

	case MOVE_STATE::MAIN:

		AnimeData*anime = nullptr;
		anime = AnimeFrame;
		obj->AnimeUpdate(anime);

		obj->position.x = obj->parent3D->GetPosition().x;
		obj->position.y = obj->parent3D->GetPosition().y-100;

		if (!obj->parent3D->GetIsPowerUp())
			obj->Clear();

		break;
	}

}

//加算合成するか否か設定できるようにした描画処理
void EffectManager::Render(BlendState blend)
{

	auto it = end();
	while (it != begin()) {
		it--;

		if (it->isAdd)
			blend.Set(blend.BS_ADD);

		it->Render();
		blend.Set(blend.BS_ALPHA);

	};

}

