#include "../Headers/Player.h"
#include "../Headers/SceneBattle.h"
#include "../Headers/UIMenu.h"
#include "../GameLib/Math/Math.h"
#include "../Headers/Effect.h"


//*********************************************************
//
//		移動処理とその中で使っている関数群
//
//*********************************************************



void Player::PlyerMove(int keyTrg, int keyState)
{

	

	if (isCancelStandBy) {

		//慣性残すためにスピード保存
		speedTmp = speed;

		moveState = PLAYER_MOVE::CANCEL;
		plMesh->Play(PLAYER_MOTION::APPEAL);
		isPowerUp = true;

		effectManager->Create(EffectManager::Move::Cancel, position);
		{
			Obj2D* obj = effectManager->Create(EffectManager::Move::PowerUp, position);
			obj->parent3D = this;
		}
		artsPoint -= (ARTS_MAX*0.25);
		isCancelStandBy = false;
	}
	if (isRollingSB) {
		moveState = PLAYER_MOVE::ROLLING;
		plMesh->Play(PLAYER_MOTION::ROLLING);
		isNotPush = true;
		isRollingSB = false;
		enemy->isNotPush = true;
		frameCnt = 0;

	}


	using namespace Input;

	//方向決定(空中では行わない
	if (!isAir) {
		DecideDirection();
	}

	//方向によって向き変更
	switch ((int)dir)
	{
	case DIR_RIGHT:
		plMesh->rotation = INIT_ROTATION_1P;
		plMesh->scale = INIT_SCALE_1P;
		break;
	case DIR_LEFT:
		plMesh->rotation = INIT_ROTATION_2P;
		plMesh->scale = INIT_SCALE_2P;
		break;
	}



	//-----------------プレイヤー行動分岐--------------------
	switch (moveState)
	{



	case PLAYER_MOVE::NEUTRAL:

		//アニメーション
		plMesh->Animate();

		//残っているとまずいやつらを初期化
		isAir = false;
		isActive = true;
		isHit = false;
		isCommand = true;
		isNotPush = false;
		isNotReturn = false;
		isInvisible = false;
		isDown = false;
		isCancel = true;
		enemy->isStop = false;
		frameCnt = 0;
		isDark = false;
		speed.x = {};
		knockbackSpeed = {};
		motionMoveCnt = 0;

		if (position.y < GROUND)
			ReturnAir();

		//トレーニングモードだとHP回復
		if ((hp < HP_MAX) &&
			(menuUI->mode == GAME_MODE::TRANING))
			hp++;


		//移動遷移
		if (keyState&PAD_DOWN) {

			plMesh->Play(PLAYER_MOTION::LOW_INIT, 0, true);
			moveState = PLAYER_MOVE::SIT;
		}
		//右キー
		else if (keyState&PAD_RIGHT) {
			plMesh->Play(PLAYER_MOTION::WALKFRONT);
			moveState = PLAYER_MOVE::WALKFRONT;
			speed.x = commonParam[PLAYER_PARAM::WALK_SPEED];
		}
		//左キー(後ろ歩きなのでちょい遅めに
		else if (keyState&PAD_LEFT) {
			plMesh->Play(PLAYER_MOTION::WALKBACK);
			moveState = PLAYER_MOVE::WALKBACK;
			speed.x = -commonParam[PLAYER_PARAM::WALK_SPEED] * 0.8f;
		}
		//上キー
		if (keyState&PAD_UP) {

			plMesh->Play(PLAYER_MOTION::JUMP_INIT);
			moveState = PLAYER_MOVE::JUMP;
			isAir = true;

			//左右キーが押されていれば
			//その方向へジャンプ
			if (keyState&PAD_RIGHT)
				speed.x = commonParam[PLAYER_PARAM::WALK_SPEED];
			else if (keyState&PAD_LEFT)
				speed.x = -commonParam[PLAYER_PARAM::WALK_SPEED];
		}




		playerState = PLAYER_STATE::NOT_MOVE;
		break;


	case PLAYER_MOVE::WALKFRONT:

		//歩き中でもジャンプ可能
		JumpCancel(keyState);

		//キー離れたらニュートラルに戻る
		if (!(keyState&PAD_RIGHT))
		{
			ReturnNeutral(true);
		}

		ArtsAdd(0.1f);

		plMesh->Animate();
		playerState = PLAYER_STATE::NOT_MOVE;

		break;

	case PLAYER_MOVE::WALKBACK:

		//歩き中でもジャンプ可能
		JumpCancel(keyState);

		//キー離れたらニュートラルに戻る
		if (!(keyState&PAD_LEFT))
		{
			ReturnNeutral(true);
		}


		plMesh->Animate();
		playerState = PLAYER_STATE::NOT_MOVE;

		break;

		//ジャンプ
	case PLAYER_MOVE::JUMP:


		switch (subState)
		{
			//ジャンプ移行まで
		case PLAYER_JUMP::INIT:
			isActive = false;

			//ジャンプ移行フレーム
			if (++motionMoveCnt == JUMP_INIT_FRAME) {
				speed.y = commonParam[PLAYER_PARAM::JUMP_SPEED];
				isActive = true;
				plMesh->Play(PLAYER_MOTION::JUMP);
				subState++;
				motionMoveCnt = 0;

			}
			plMesh->Animate();

			break;
			//ジャンプ落下
		case PLAYER_JUMP::FALL:

			isActive = true;

			//設地した場合
			if (position.y >= GROUND) {
				isAir = false;
			}

			plMesh->Animate();

			//設地した場合
			if (position.y >= GROUND) {
				isAir = false;
			}

			//設地した場合
			if (!isAir)
			{

				//方向再決定
				DecideDirection();

				plMesh->Play(PLAYER_MOTION::JUMP_END);
				subState++;
			}
			break;
		case 2:

			//設地した場合
			if (plMesh->Animate())
			{

				//方向再決定
				DecideDirection();
				//ニュートラルに戻る
				ReturnNeutral();
				subState = 0;
			}
			break;
		}

		playerState = PLAYER_STATE::JUMP;

		break;

		//しゃがみ
	case PLAYER_MOVE::SIT:


		switch (subState)
		{
		case 0:

			if (plMesh->Animate()) {
				plMesh->Play(PLAYER_MOTION::LOW_NEUTRAL);
				subState++;
			}
			break;
		case 1:

			plMesh->Animate();
			break;
		}
		speed = {};
		if (!(keyState&PAD_DOWN))
		{
			//ニュートラルに戻る(補間あり
			ReturnNeutral(true);
			subState = 0;
		}


		playerState = PLAYER_STATE::NOT_MOVE;





		break;

	case PLAYER_MOVE::RUN:

		JumpCancel(keyState);


		if (keyState&PAD_RIGHT) {
			speed.x = commonParam[PLAYER_PARAM::RUN_SPEED];
			plMesh->Animate();
		}
		else {
			ReturnNeutral(true);
		}
		ArtsAdd(0.2f);

		playerState = PLAYER_STATE::NOT_MOVE;

		break;

	case PLAYER_MOVE::RUNAIR:

		speed.y = 0;
		ArtsAdd(0.2f);

		//設地した場合
		if (!isAir)
		{
			speed.x = 0;
		}
		//モーション終わり	
		if (plMesh->Animate())
		{
			ReturnAir();
			frameCnt = 0;
			speed.x = 12.f;
		}
		playerState = PLAYER_STATE::NOT_MOVE;

		break;

	case PLAYER_MOVE::BACK_DASH:

		speed.y = 0;
		//設地した場合
		if (!isAir)
		{
			speed.x = 0;
		}
		//モーション終わり
		if (plMesh->Animate())
		{
			ReturnAir();
			frameCnt = 0;
			speed.x = -12.f;
		}
		playerState = PLAYER_STATE::NOT_MOVE;

		break;
	case PLAYER_MOVE::BACKSTEP:

		if (plMesh->nowFrame < BACK_STEP_AIR_FRAME)
		{
			isAir = true;
		}
		//設地した場合
		if (!isAir)
		{
			speed.x = 0;
		}

		if (plMesh->Animate())
		{
			ReturnNeutral();
		}

		playerState = PLAYER_STATE::NOT_MOVE;

		break;

	case PLAYER_MOVE::DAMAGE:

		//動けません
		isActive = false;
		isCancel = false;
		isCommand = false;


		//一定フレームだけノックバック
		if (++moveCnt > KNOCK_BACK_FRAME)
		{
			knockbackSpeed.x = 0;
			enemy->knockbackSpeed.x = 0;
		}



		//モーション再生(硬直フレームまたは全体フレーム
		if ((--rigidFrame <= 0) || plMesh->Animate(0.5f))
		{

			isThrowed = false;
			isThrowComp = false;

			moveCnt = 0;

			ReturnNeutral(true);
		}

		playerState = PLAYER_STATE::DAMAGE;
		break;
	case PLAYER_MOVE::DAMAGE_AIR:

		//動けません
		isActive = false;
		isCancel = false;
		isCommand = false;


		knockbackSpeed.x = 0;
		enemy->knockbackSpeed.x = 0;

		if (position.y >= GROUND)
		{
			isAir = false;
		}

		if (!isAir) {
			ReturnNeutral();
			return;
		}
		//モーション再生(硬直フレームまたは全体フレーム
		if ((--rigidFrame <= 0) || plMesh->Animate())
		{

			isThrowed = false;
			isThrowComp = false;

			moveCnt = 0;
			ReturnRecover();


		}

		playerState = PLAYER_STATE::DAMAGE;
		break;


	case PLAYER_MOVE::DOWN:

		//動けません
		isActive = false;
		isCancel = false;
		isCommand = false;
		isNotReturn = false;
		isNotPush = false;
		isAir = true;
		//一定フレームだけノックバック
		if (++moveCnt > KNOCK_BACK_FRAME)
		{
			knockbackSpeed.x = 0;
			enemy->knockbackSpeed.x = 0;
		}
		//モーション再生(硬直フレームまたは全体フレーム
		if (plMesh->Animate())
		{
			isAir = false;
			isThrowed = false;
			isThrowComp = false;
			moveCnt = 0;
			ReturnNeutral();
		}

		playerState = PLAYER_STATE::DAMAGE;
		break;


	case PLAYER_MOVE::THROWED:

		//動けません
		isActive = false;
		isCommand = false;
		isNotReturn = false;
		plMesh->Animate();

		playerState = PLAYER_STATE::DAMAGE;

		break;
	case PLAYER_MOVE::GUARD:

		//動けません
		isActive = false;
		isCommand = false;
		isCancel = false;

		ArtsAdd(1);

		if (++moveCnt > KNOCK_BACK_FRAME)
		{
			knockbackSpeed.x = 0;
			enemy->knockbackSpeed.x = 0;
		}



		//モーション再生
		plMesh->Animate();
		if (--rigidFrame <= 0)
		{
			rigidFrame = 0;

			moveCnt = 0;

			ReturnNeutral();

		}

		playerState = PLAYER_STATE::GUARD;

		break;

		//空中くらい状態
	case PLAYER_MOVE::COMBO_AIR:


		isAir = true;

		//硬直フレーム経過で受け身
		if ((--rigidFrame <= 0))
		{
			ReturnRecover();
		}

		plMesh->Animate();

		break;


		//空中受け身
	case PLAYER_MOVE::RECOVER_AIR:


		if (position.y >= GROUND) {
			isAir = false;
			ReturnNeutral();
			speed = {};
		}

		isActive = false;
		isCommand = false;
		isCancel = false;


		plMesh->Animate();
		break;




		//死亡時
	case PLAYER_MOVE::DEATH:

		//スピード一括０代入
		knockbackSpeed = {};
		enemy->knockbackSpeed = {};
		speed = {};
		//動けません
		isActive = false;

		//シーンマネージャーに死亡したことを伝える
		//死亡モーションなので一度だけ再生
		if (plMesh->AnimateOnece())
		{
			sceneBattle->isDead = true;
		}

		break;

		//勝利時
	case PLAYER_MOVE::WIN:

		//動けません
		isActive = false;


		plMesh->AnimateOnece();

		break;
		//------------------------------------------------------
		// 
		// 通常攻撃
		// 
		//------------------------------------------------------

	case PLAYER_MOVE::ATTACK_NA:

		grdType = GRD_TYPE::ALL;
		attackLast = PLAYER_ATTACK::NA;

		//スピードがある場合減衰
		SpeedDown();


		//動けません
		isActive = false;

		//モーション終わり
		if (plMesh->Animate())
		{
			isPowerUp = false;

			ReturnRigit(commonParam[PLAYER_PARAM::RIGID_NA]);
		}

		playerState = PLAYER_STATE::ATTACK;

		break;

		//投げ
	case PLAYER_MOVE::ATTACK_THROW:

		grdType = GRD_TYPE::THROW;
		attackLast = PLAYER_ATTACK::THROW;


		//動けません
		isActive = false;
		isCommand = false;
		//スピードがある場合減衰
		SpeedDown();

		//モーション終わり
		if (plMesh->Animate())
		{
			isInvisible = false;
			isPowerUp = false;

			ReturnNeutral();
		}

		break;


	case PLAYER_MOVE::ATTACK_NB:

		grdType = GRD_TYPE::ALL;
		attackLast = PLAYER_ATTACK::NB;
		//動けません
		isActive = false;

		//スピードがある場合減衰
		SpeedDown();


		//モーション終わり
		if (plMesh->Animate())
		{
			ReturnRigit(commonParam[PLAYER_PARAM::RIGID_NB]);
			isPowerUp = false;

		}

		playerState = PLAYER_STATE::ATTACK;

		break;

	case PLAYER_MOVE::ATTACK_NC:

		grdType = GRD_TYPE::ALL;
		attackLast = PLAYER_ATTACK::NC;
		//動けません
		isActive = false;

		//スピードがある場合減衰
		SpeedDown();


		//モーション終わり
		if (plMesh->Animate())
		{
			ReturnRigit(commonParam[PLAYER_PARAM::RIGID_NC]);
			isPowerUp = false;

		}

		playerState = PLAYER_STATE::ATTACK;

		break;
	case PLAYER_MOVE::STAND_ATTACK_EX:

		if (isPowerUp)
			grdType = GRD_TYPE::THROW;
		else
			grdType = GRD_TYPE::UP;


		//動けません
		isActive = false;

		//スピードがある場合減衰
		SpeedDown();


		//モーション終わり
		if (plMesh->Animate())
		{
			ReturnRigit(0);
			isPowerUp = false;

		}

		playerState = PLAYER_STATE::ATTACK;

		break;


	case PLAYER_MOVE::ATTACK_LA:

		grdType = GRD_TYPE::ALL;
		attackLast = PLAYER_ATTACK::LA;
		//動けません
		isActive = false;

		//スピードがある場合減衰
		SpeedDown();


		//モーション終わり
		if (plMesh->Animate())
		{
			ReturnRigit(commonParam[PLAYER_PARAM::RIGID_LA]);
			isPowerUp = false;

		}

		playerState = PLAYER_STATE::ATTACK;

		break;


	case PLAYER_MOVE::ATTACK_LB:

		grdType = GRD_TYPE::LOW;
		attackLast = PLAYER_ATTACK::LB;
		//動けません
		isActive = false;

		//スピードがある場合減衰
		SpeedDown();



		//モーション終わり
		if (plMesh->Animate())
		{
			ReturnRigit(commonParam[PLAYER_PARAM::RIGID_LB]);
			isPowerUp = false;
		}

		playerState = PLAYER_STATE::ATTACK;

		break;


	case PLAYER_MOVE::ATTACK_LC:

		grdType = GRD_TYPE::ALL;
		attackLast = PLAYER_ATTACK::LC;
		//動けません
		isActive = false;

		//スピードがある場合減衰
		SpeedDown();

		
		//モーション終わり
		if (plMesh->Animate())
		{
			ReturnRigit(commonParam[PLAYER_PARAM::RIGID_LC]);
			isPowerUp = false;

		}

		playerState = PLAYER_STATE::ATTACK;

		break;


	case PLAYER_MOVE::ATTACK_JA:

		//動けません
		isActive = false;

		grdType = GRD_TYPE::UP;
		attackLast = PLAYER_ATTACK::JA;



		if (position.y >= GROUND)
			isAir = false;

		//設地した場合
		if (!isAir) {
			ReturnNeutral();
		}

		//モーション終わり
		if (plMesh->Animate() || !isAir) {
			ReturnRigit(commonParam[PLAYER_PARAM::RIGID_JA]);
			isPowerUp = false;

		}

		playerState = PLAYER_STATE::JUMP;

		break;


	case PLAYER_MOVE::ATTACK_JB:

		//動けません
		isActive = false;

		grdType = GRD_TYPE::UP;
		attackLast = PLAYER_ATTACK::JB;



		if (position.y >= GROUND)
			isAir = false;

		//設地した場合
		if (!isAir) {
			ReturnNeutral();
		}

		//モーション終わり
		if (plMesh->Animate() || !isAir) {
			ReturnRigit(commonParam[PLAYER_PARAM::RIGID_JB]);
			isPowerUp = false;

		}

		playerState = PLAYER_STATE::JUMP;
		break;


	case PLAYER_MOVE::ATTACK_JC:


		//動けません
		isActive = false;

		grdType = GRD_TYPE::UP;
		attackLast = PLAYER_ATTACK::JC;


		if (position.y >= GROUND) {
			isAir = false;
			speed.x = 0;
		}

		//設地した場合
		if (position.y >= GROUND) {
			isAir = false;
		}

		//設地した場合
		
		//モーション終わり
		if (plMesh->Animate()||!isAir) {
			ReturnRigit(commonParam[PLAYER_PARAM::RIGID_JC]);
			isPowerUp = false;
		}

		playerState = PLAYER_STATE::JUMP;
		break;

	case PLAYER_MOVE::ARTS00A:

		speed.x = 0;
		knockbackSpeed.x = 0;
		if (plMesh->Animate()) {
			ReturnRigit(commonParam[PLAYER_PARAM::RIGID_JC]);
			isPowerUp = false;
		}
		break;

	case PLAYER_MOVE::ARTS01A:

		if (++frameCnt == 37)isHit = false;

		speed.x = 0;
		knockbackSpeed.x = 0;
		if (plMesh->Animate()) {
			frameCnt = 0;

			ReturnRigit(commonParam[PLAYER_PARAM::RIGID_JC]);
			isPowerUp = false;
		}
		break;

	case PLAYER_MOVE::ARTS02A:


		speed.x = 30;
		++frameCnt;
		if (frameCnt == 31)isHit = false;
		if (frameCnt == 51) {
			isHit = false; isDown = true;
		}

		
		if (frameCnt >=20) {
			ReturnRigit(10);
			frameCnt = 0;
			isPowerUp = false;
		}
		plMesh->Animate();
		break;

	case PLAYER_MOVE::ARTS02B:


		speed.x = 30;
		++frameCnt;
		if (frameCnt == 20)isHit = false;
		


		if (frameCnt >= 30) {
			ReturnRigit(10);
			frameCnt = 0;
			isPowerUp = false;
		}
		plMesh->Animate();
		break;

	case PLAYER_MOVE::ARTS02C:


		speed.x = 30;
		++frameCnt;
		if (frameCnt == 20)isHit = false;
		if (frameCnt == 30) {
			isHit = false; isDown = true;
		}


		if (frameCnt >= 40) {
			ReturnRigit(10);
			frameCnt = 0;
			isPowerUp = false;
		}
		plMesh->Animate();
		break;

	case PLAYER_MOVE::CANCEL:


		enemy->isStop = true;
		isDark = true;
		speed = {};
		knockbackSpeed = {};
		if (plMesh->Animate(0.3f)) {
			if (isAir) {
				ReturnAir();
				speed = speedTmp;
				enemy->isStop = false;
				frameCnt = 0;
				isDark = false;

			}
			else {
				ReturnNeutral();
				enemy->isStop = false;
				frameCnt = 0;
				isDark = false;

			}
		}
		break;


	case PLAYER_MOVE::ATEMI_READY:

		isActive = false;
		isCommand = false;
		isCancel = false;
		frameCnt++;
		if (frameCnt > 3 && frameCnt < 20)
		{
			isAtemi = true;
		}
		else
			isAtemi = false;

		speed.x = 0;

		if (frameCnt > 40) {

			ReturnNeutral();
			frameCnt = 0;
		}

		break;



	case PLAYER_MOVE::ATEMI:


		isCancel = false;

		speed.x = 0;
		knockbackSpeed.x = 0;
		if (plMesh->Animate())
		{
			enemy->isStop = false;
			ReturnNeutral();
		}


		break;

	case PLAYER_MOVE::ROLLING:


		isActive = false;
		isCancel = false;
		isCommand = false;

		++frameCnt;
		if (frameCnt < 20) {
			speed.x = 40;
		}
		else {
			speed.x = 0;
			isNotPush = false;
			enemy->isNotPush = false;
		}
		knockbackSpeed.x = 0;
		if (frameCnt > 30)
		{
			isNotPush = false;
			isRollingSB = false;
			enemy->isNotPush = false;

			frameCnt = 0;
			ReturnNeutral();
		}

		plMesh->Animate();
		break;


	case PLAYER_MOVE::RIGID:

		//何もできない
		moveCnt = 0;
		frameCnt = 0;
		isActive = false;
		isCommand = false;
		isCancel = false;

		if (--rigidFrame < 0) {

			if (isAir)
				ReturnAir();
			else
				ReturnNeutral();

		}

		plMesh->Animate();
		break;


	}




	//座標情報更新----------------------------------------

	//１フレーム前の座標保存(スクリーン範囲制限に仕様
	oldPosition = position;

	//座標更新
	position.x += (speed.x*dir + knockbackSpeed.x*dir);
	position.y += speed.y;

	//範囲制限
	ScreenLimit();

	//******************************************************************************
	//
	//  描画モデル更新
	//
	//*****************************************************************************

	PlMeshUpdate();
}



//**************************************************
//			プライベートメソッド
//**************************************************
//範囲制限
void Player::ScreenLimit()
{
	VECTOR2 pl1Pos = sceneBattle->player1->position;
	VECTOR2 pl2Pos = sceneBattle->player2->position;
	VECTOR2 centerPos = (pl1Pos + pl2Pos) / 2.0f;


	float distanceX = pl1Pos.x - pl2Pos.x;//プレイヤーの距離
	float adjust = pushRectData.size.x / 2;

	//お互いの距離が離れたとき
	if (fabs(distanceX) > DxSystem::SCREEN_WIDTH - adjust * 2) {
		position.x = oldPosition.x;//スクロールしない
	}

	//画面の右端到達
	if (position.x >= STAGE_MAX - adjust) {

		position.x = STAGE_MAX - adjust;//壁際で止まらせる
		isWall = true;

	}
	//画面の左端到達
	else if (position.x <= STAGE_MIN + adjust) {

		position.x = STAGE_MIN + adjust;//壁際で止まらせる
		isWall = true;
	}
	else
		isWall = false;


	//一定以上下がると地面位置に固定
	if (position.y > GROUND)
		position.y = GROUND;


}
//方向決定
void Player::DecideDirection()
{
	//すり抜けるフラグが立っていなければ
	if (isNotPush)return;
	if (isNotReturn)return;

	//プレイヤーの位置によって方向決定
	if (position.x - DIR_ADDIST > enemy->position.x + DIR_ADDIST)
	{
		dir = DIR_LEFT;
	}
	else if (position.x + DIR_ADDIST < enemy->position.x - DIR_ADDIST)
	{

		dir = DIR_RIGHT;
	}
}


//投げ相殺
void Player::MoveThrowComp()
{
	stopFrame = HITSTOP_FRAME_THROWCOMP;
	//お互いにノックバック
	position.x += THROWCOMP_SPEED*dir;
	enemy->position.x += THROWCOMP_SPEED*enemy->dir;
}

//ニュートラルに戻る
void Player::ReturnNeutral(bool isBlend)
{

	using namespace Input;
	plMesh->motionNameOld = plMesh->motionName;
	rigidFrame = 0;
	airMoveCnt = 1;
	grdType = GRD_TYPE::ALL;

	//しゃがみボタンが押されてたらしゃがみに移行
	if (keyState&PAD_DOWN) {
		plMesh->Play(PLAYER_MOTION::LOW_NEUTRAL, 0, isBlend);
		moveState = PLAYER_MOVE::SIT;
		isActive = true;
		isCommand = true;
		isNotReturn = false;
		moveCnt = 0;
		subState = 0;
		knockbackSpeed = {};
		isHit = false;
		isAtemi = false;
		isGetAttack = false;

	}
	//しゃがみボタンが押されてたらしゃがみに移行
	else {
		plMesh->Play(PLAYER_MOTION::NEUTRAL, 0, isBlend);
		moveState = PLAYER_MOVE::NEUTRAL;
		isActive = true;
		isCommand = true;
		moveCnt = 0;
		subState = 0;
		isHit = false;

	}
}

//空中落下状態に戻る
void Player::ReturnAir()
{
	moveCnt = 0;
	subState = PLAYER_JUMP::FALL;
	moveState = PLAYER_MOVE::JUMP;
	plMesh->Play(PLAYER_MOTION::JUMP);
	isHit = false;
}

//受け身取ります
void Player::ReturnRecover()
{
	speed = RECOVER_SPEED;
	plMesh->Play(PLAYER_MOTION::RECOVER_AIR);
	moveState = PLAYER_MOVE::RECOVER_AIR;

}


void Player::ReturnRigit(float rigidFrame)
{
	//硬直0の場合すぐにもどる
	if (rigidFrame == 0) {

		if (isAir) {
			ReturnAir();
			frameCnt = 0;
		}
		else {
			ReturnNeutral();
			frameCnt = 0;
		}
		return;
	}
	this->rigidFrame = rigidFrame;
	speed.x = 0;
	knockbackSpeed.x = 0;
	isNotPush = false;

	//空中なら落下、地上なら待機モーションのまま硬直
	if (isAir) {
		subState = PLAYER_JUMP::FALL;
		plMesh->Play(PLAYER_MOTION::JUMP);
	}
	else if (grdType == GRD_TYPE::LOW) {
		plMesh->Play(PLAYER_MOTION::LOW_NEUTRAL);
	}
	else
	{
		plMesh->Play(PLAYER_MOTION::NEUTRAL);
	}
	moveState = PLAYER_MOVE::RIGID;
}



//慣性残して移動する
void Player::SpeedDown()
{
	//スピードがあれば一定で減衰
	if (speed.x > 0) {
		speed.x += DOWN_SPEED;
	}
	else {
		speed.x = 0;
	}
}

//押し合い処理
void Player::PushMove()
{

	//完全に重なったままだと不都合が生じるので
	//0.01だけずらしたところまで補間しています


	if (isPush) {

		if (position.x > enemy->position.x) {
			//重ならない位置まで線形補間
			position.x = Math::LerpFloat(position.x, enemy->position.x - (enemy->pushRectData.size.x + 0.01f)*(float)DIR_LEFT, PUSH_LERP_RATE);
			//押し動かす
			//enemy->position.x -= speed.x / 2;
		}
		else if (position.x < enemy->position.x) {
			//重ならない位置まで線形補間
			position.x = Math::LerpFloat(position.x, enemy->position.x - (enemy->pushRectData.size.x + 0.01f), PUSH_LERP_RATE);
			//押し動かす
			//enemy->position.x += speed.x / 2;

		}
		else {

			if (dir == DIR_RIGHT)
			{
				//重ならない位置まで線形補間
				position.x = Math::LerpFloat(position.x, enemy->position.x - (enemy->pushRectData.size.x + 0.01f)*(float)DIR_LEFT, PUSH_LERP_RATE);
				//押し動かす
				//enemy->position.x -= speed.x / 2;
			}
			else
			{
				//重ならない位置まで線形補間
				position.x = Math::LerpFloat(position.x, enemy->position.x - (enemy->pushRectData.size.x + 0.01f), PUSH_LERP_RATE);
				//押し動かす
				//enemy->position.x += speed.x / 2;
			}
		}


	}
}

//敵に吹っ飛ばされる値を設定する
void Player::SetForceFromEnemy(float forceY, float forceX)
{
	speed.x = forceX;
	speed.y = forceY;
}

//******************************************************************************
//
//  描画モデル更新
//
//*****************************************************************************
static const float MESH_LERP_MAX_X = 185.f;//２D座標とのズレを補間するときの最大値(X座標)
static const float MESH_OFFSET_Y = -300;//２D座標とのズレを補間するときの最大値(Y座標)
static const float MESH_OFFSET_Z = 11.f;//Z座標(固定

void Player::PlMeshUpdate()
{
	//メッシュ側に描画位置を渡します
	//WVP変換による座標のずれも見た目に違和感がなくなるように調整します

	float ad = 0;	//補正値

	//補正値計算（カメラ位置からどちら側にいるか
	if (position.x < gameCamera->pos.x)
		ad = Math::LerpFloat(0,
			MESH_LERP_MAX_X,
			fabs(gameCamera->pos.x - position.x) / (DxSystem::SCREEN_WIDTH / 2));
	else
		ad = Math::LerpFloat(0,
			-MESH_LERP_MAX_X,
			fabs(gameCamera->pos.x - position.x) / (DxSystem::SCREEN_WIDTH / 2));

	//位置座標を3Dモデル側に渡す
	plMesh->position.x = position.x - ad;
	plMesh->position.y = position.y + MESH_OFFSET_Y;
	plMesh->position.z = -MESH_OFFSET_Z;
	//行列の更新
	plMesh->Update(true);

}



void Player::ArtsAdd(float increaseVal)
{
	artsPoint += increaseVal;

	if (artsPoint >= ARTS_MAX)
		artsPoint = ARTS_MAX;

}

