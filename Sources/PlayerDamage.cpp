#include "../Headers/Player.h"
#include "../GameLib/Headers/DxAudio.h"
#include "../Headers/SceneBattle.h"
#include "../Headers/Effect.h"

//*********************************************************
//
//				ダメージ、ガード関連
//
//*********************************************************

enum SOUND_NUM
{
	HIT = 4,
	GUARD = 8,
};


//ダメージ、ガードモーション実行
void Player::PlayDamage()
{
	//行動状態遷移
	//投げ
	if (isThrowed) {
		moveState = PLAYER_MOVE::THROWED;
		plMesh->Play(PLAYER_MOTION::THROWED);
	}
	else if (isGetAttack&&moveState != PLAYER_MOVE::ATEMI) {

		if (keyState&Input::PAD_RIGHT) {

			position.x = enemy->position.x + 300 * dir;

			float tmp = 0;
			tmp = GetArtsPoint();
			SetArtsPoint(enemy->GetArtsPoint());
			enemy->SetArtsPoint(tmp);

			sceneBattle->isCross = true;
			dir *= -1;

			if (enemy->GetIsWall())
				enemy->position.x += 200 * dir;
		}
		moveState = PLAYER_MOVE::ATEMI;
		plMesh->Play(PLAYER_MOTION::ATEMI);
		enemy->isStop = true;
		isGetAttack = false;
		speed.x = 0;
	}

	//ダウン
	else if (enemy->isDown) {
		subState = 0;
		moveState = PLAYER_MOVE::DOWN;
		plMesh->Play(PLAYER_MOTION::DOWN);
		enemy->isDown = false;
	}
	//空中くらい
	else if (isAir) {
		moveState = PLAYER_MOVE::DAMAGE_AIR;
		plMesh->Play(PLAYER_MOTION::DAMAGE_AIR);
	}
	//地上くらい
	else {
		plMesh->Play(PLAYER_MOTION::DAMAGE);
		moveState = PLAYER_MOVE::DAMAGE;
	}
	//死亡
	if (hp <= 0) {
		plMesh->Play(PLAYER_MOTION::DEATH);
		moveState = PLAYER_MOVE::DEATH;
	}


	isDamage = false;
	moveCnt = 0;

	dxAudio->SoundPlay(SOUND_NUM::HIT);

	DecideDirection();



}

void Player::PlayGuard()
{
	plMesh->Play(PLAYER_MOTION::GUARD);
	moveState = PLAYER_MOVE::GUARD;
	isCancel = false;

	if (isGetAttack&&moveState != PLAYER_MOVE::ATEMI) {

		if (keyState&Input::PAD_RIGHT) {

			position.x = enemy->position.x + 300 * dir;

			float tmp = 0;
			tmp = GetArtsPoint();
			SetArtsPoint(enemy->GetArtsPoint());
			enemy->SetArtsPoint(tmp);

			sceneBattle->isCross = true;
			dir *= -1;

			if (enemy->GetIsWall())
				enemy->position.x += 200 * dir;
		}
		moveState = PLAYER_MOVE::ATEMI;
		plMesh->Play(PLAYER_MOTION::ATEMI);
		enemy->isStop = true;
		isGetAttack = false;
		speed.x = 0;
	}
	//死亡
	else if (hp <= 0) {
		plMesh->Play(PLAYER_MOTION::DEATH);
		moveState = PLAYER_MOVE::DEATH;
	}

	isGuard = false;
	moveCnt = 0;
	speed = {};

	dxAudio->SoundPlay(SOUND_NUM::GUARD);

}


//ダメージ、ガードモーション予約
//(当たり判定を行ったタイミングで行う)
void Player::DamageReserve(bool isShot)
{
	speed.x = 0;
	//ダメージを受けた時の敵プレイヤーの状態によって
	//硬直、ノックバック、ダメージ、ヒットストップ、移動値を設定する

	//上に吹っ飛ばされる時に補正されてしまうので少し上に補正
	position.y -= 1.f;

	switch (enemy->moveState)
	{
	case  PLAYER_MOVE::ATTACK_NA:

		//パラメータ設定
		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_NA];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_NA];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_NA]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_NA];
		if (isAir)
			SetForceFromEnemy(enemy->commonParam[PLAYER_PARAM::FORCE_NA]);

		break;

	case  PLAYER_MOVE::ATTACK_LA:

		//パラメータ設定
		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_LA];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_LA];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_LA]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_LA];
		if (isAir)
			SetForceFromEnemy(enemy->commonParam[PLAYER_PARAM::FORCE_LA]);

		break;
	case  PLAYER_MOVE::ATTACK_JA:

		//パラメータ設定
		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_JA];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_JA];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_JA]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_JA];
		if (isAir)
			SetForceFromEnemy(enemy->commonParam[PLAYER_PARAM::FORCE_JA]);

		break;

	case  PLAYER_MOVE::ATTACK_NB:

		//パラメータ設定
		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_NB];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_NB];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_NB]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_NB];
		if (isAir)
			SetForceFromEnemy(enemy->commonParam[PLAYER_PARAM::FORCE_NB]);


		break;

	case  PLAYER_MOVE::ATTACK_LB:

		//パラメータ設定
		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_LB];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_LB];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_LB]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_LB];
		if (isAir)
			SetForceFromEnemy(enemy->commonParam[PLAYER_PARAM::FORCE_LB]);
		enemy->isDown = true;//ダウン

		break;

	case  PLAYER_MOVE::ATTACK_JB:

		//パラメータ設定
		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_JB];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_JB];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_JB]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_JB];
		if (isAir)
			SetForceFromEnemy(enemy->commonParam[PLAYER_PARAM::FORCE_JB]);


		break;

	case  PLAYER_MOVE::ATTACK_NC:

		//パラメータ設定
		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_NC];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_NC];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_NC]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_NC];
		if (isAir)
			SetForceFromEnemy(enemy->commonParam[PLAYER_PARAM::FORCE_NC]);



		break;

	case  PLAYER_MOVE::ATTACK_LC:

		//パラメータ設定
		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_LC];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_LC];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_NC]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_LC];
		if (isAir)
			SetForceFromEnemy(enemy->commonParam[PLAYER_PARAM::FORCE_LC]);



		break;

	case  PLAYER_MOVE::ATTACK_JC:

		//パラメータ設定
		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_JC];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_JC];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_JC]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_JC];
		if (isAir)
			SetForceFromEnemy(enemy->commonParam[PLAYER_PARAM::FORCE_JC]);


		break;


	case  PLAYER_MOVE::ATTACK_THROW:

		//パラメータ設定
		enemy->attack = 0;
		isThrowed = true;//投げられ状態フラグ
		stopFrame = 0;

		break;


	case  PLAYER_MOVE::ARTS00A:

		//パラメータ設定
		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_ARTS00];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_ARTS00];
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_ARTS00];



		break;

	case  PLAYER_MOVE::ARTS01A:

		//パラメータ設定
		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_ARTS01];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_ARTS01];
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_ARTS01];




		break;


	case  PLAYER_MOVE::ARTS02A:


		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_ARTS02];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_ARTS02];
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_ARTS02];

		break;
	case  PLAYER_MOVE::ARTS02B:


		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_ARTS02];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_ARTS02];
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_ARTS02];

		break;
	case  PLAYER_MOVE::ARTS02C:


		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_ARTS02];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_ARTS02];
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_ARTS02];

		SetForceFromEnemy(enemy->commonParam[PLAYER_PARAM::FORCE_ARTS01Y, PLAYER_PARAM::FORCE_ARTS01X]);

		break;
	case  PLAYER_MOVE::ATEMI:


		rigidFrame = 30;
		enemy->attack = 30;
		stopFrame = 20;
		enemy->isDown = true;//ダウン

		SetForceFromEnemy(-30, -100);

		break;


	case  PLAYER_MOVE::STAND_ATTACK_EX:

		//パラメータ設定
		rigidFrame = enemy->commonParam[PLAYER_PARAM::DMG_FRAME_NC];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_NC];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_NC]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_NC];
		if (isAir)
			SetForceFromEnemy(enemy->commonParam[PLAYER_PARAM::FORCE_NC]);




	}
	isGetAttack = false;

	if (isAtemi)
	{
		isAtemi = false;
		isGetAttack = true;
		stopFrame = 20;
		sceneBattle->SetHitStop(stopFrame);
		return;
	}






	//ダウン技ならダウン硬直設定
	if (enemy->isDown)
	{
		rigidFrame = DMG_FRAME_DOWN;
	}



	//ヒットストップ設定
	if (enemy->isPowerUp)
		sceneBattle->SetHitStop(stopFrame*1.5f);
	else
		sceneBattle->SetHitStop(stopFrame);



	//コンボ数に応じて火力補正
	float dmg = 0;
	if (enemy->comboCnt <= 1)
		dmg = enemy->attack;
	else
		dmg = enemy->attack - enemy->comboCnt*COMBO_CORRECT;//カウントが増えるとダメージ減少


	if (enemy->isPowerUp)
		dmg *= 2;


	//確実に１は減るように
	if (dmg <= 1)
		dmg = 1;

	//ダメージ与えます
	hp -= dmg;
}
//ダメージ、ガードモーション予約
//(当たり判定を行ったタイミングで行う)
void Player::GuardReserve(bool isShot)
{

	enemy->attack = 0;
	//ダメージを受けた時の敵プレイヤーの状態によって
	//硬直、ノックバック、ダメージ、ヒットストップ、移動値を設定する
	switch (enemy->moveState)
	{
	case PLAYER_MOVE::ATTACK_NA:


		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_NA];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_NA];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_NA]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_NA];

		break;

	case PLAYER_MOVE::ATTACK_LA:

		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_LA];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_LA];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_LA]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_LA];


		break;

	case PLAYER_MOVE::ATTACK_JA:

		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_JA];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_JA];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_JA]);


		break;

	case PLAYER_MOVE::ATTACK_NB:

		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_NB];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_NB];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_NB]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_NB];


		break;

	case PLAYER_MOVE::ATTACK_LB:


		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_LB];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_LB];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_LB]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_LB];

		break;

	case PLAYER_MOVE::ATTACK_JB:

		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_JB];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_JB];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_JB]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_JB];


		break;

	case PLAYER_MOVE::ATTACK_NC:

		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_NC];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_NC];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_NC]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_NC];

		break;

	case PLAYER_MOVE::ATTACK_LC:


		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_LC];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_LC];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_LC]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_LC];


		break;

	case PLAYER_MOVE::ATTACK_JC:


		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_JC];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_JC];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_JC]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_JC];


		break;

	case PLAYER_MOVE::ARTS00A:

		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_ARTS00];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_ARTS00];
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_ARTS00];

		break;

	case PLAYER_MOVE::ARTS01A:

		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_ARTS01];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_ARTS01];
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_ARTS01];

		break;

	case PLAYER_MOVE::ARTS02A:

		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_ARTS02];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_ARTS02];
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_ARTS02];

		break;

	case PLAYER_MOVE::ARTS02B:

		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_ARTS02];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_ARTS02];
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_ARTS02];

		break;

	case PLAYER_MOVE::ARTS02C:

		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_ARTS02];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_ARTS02];
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_ARTS02];

		break;

	case  PLAYER_MOVE::STAND_ATTACK_EX:

		//パラメータ設定
		rigidFrame = enemy->commonParam[PLAYER_PARAM::GRD_FRAME_ARTS02];
		enemy->attack = enemy->commonParam[PLAYER_PARAM::ATTACK_NC];
		SetNockBackSpeed(enemy->commonParam[PLAYER_PARAM::KOCKBACK_NC]);
		stopFrame = enemy->commonParam[PLAYER_PARAM::HITSTOP_FRAME_NC];
		if (isAir)
			SetForceFromEnemy(enemy->commonParam[PLAYER_PARAM::FORCE_NC]);


	}

	isCancel = false;
	if (isAtemi)
	{
		isAtemi = false;
		isGetAttack = true;
		stopFrame = 20;
		sceneBattle->SetHitStop(stopFrame);
		return;
	}
	else
		isGetAttack = false;





	//ダウン技ならダウン硬直設定
	if (enemy->isDown)
	{
		rigidFrame = DMG_FRAME_DOWN;
	}



	//ヒットストップ設定
	if (isPowerUp)
		sceneBattle->SetHitStop(stopFrame*1.5f);
	else
		sceneBattle->SetHitStop(stopFrame);

	//ダメージをGUARD_CORRECT分の一に減少
	float dmg = enemy->attack * 2 / GUARD_CORRECT;

	if (enemy->isPowerUp)
		dmg *= 2;

	hp -= dmg;

}

