#include "../Headers/SpriteData.h"


#define DELETE_IF(x) if(x)delete x


//********************************************************
//
//  画像データ、2Dアニメデータ置き場
//
//********************************************************

//Spriteの実体
namespace SprInstance {
	Sprite* Life;
	Sprite* ArrowH;
	Sprite* ArrowW;
	Sprite* ArrowX;
	Sprite* ArrowY;
	Sprite* BottunHit;
	Sprite* BottunHurt;
	Sprite* ControlerXYWH;
	Sprite* Counter;
	Sprite* Load;
	Sprite* ModeIcon;
	Sprite* RoundCall;
	Sprite* Wind;
	Sprite* GameFont;
	Sprite* ChSelectIcon;
	Sprite* WinnerTag;
	Sprite* HitEffect;
	Sprite* FireBall;
	Sprite* Cancel;
	Sprite* Frame;
	Sprite* Key;
	Sprite* ArrowKey;
	Sprite* BlackOut;

};


//SpriteDataの実体
SpriteData sprLife;
SpriteData sprLifeFrame;
SpriteData sprArts;
SpriteData sprArtsFrame;
SpriteData sprBottunHit;
SpriteData sprBottunHurt;
SpriteData sprAllControler;
SpriteData sprWControler;
SpriteData sprHControler;
SpriteData sprXControler;
SpriteData sprYControler;
SpriteData sprRoundCallRound1;
SpriteData sprRoundCallRound2;
SpriteData sprRoundCallRound3;
SpriteData sprRoundCallFight;
SpriteData sprRoundCallWin;
SpriteData sprRoundCallPlayer1;
SpriteData sprRoundCallPlayer2;
SpriteData sprWinTag;
SpriteData sprCounter;
SpriteData sprModeTraning;
SpriteData sprModeVersus;
SpriteData sprModeArcade;
SpriteData sprModeCharange;
SpriteData sprModeOption;
SpriteData sprSelectCur;
SpriteData sprChSelect;
SpriteData sprGameFont00;
SpriteData sprGameFont01;
SpriteData sprGameFont02;
SpriteData sprGameFont03;
SpriteData sprGameFont04;
SpriteData sprGameFont05;
SpriteData sprGameFont06;
SpriteData sprGameFont07;
SpriteData sprGameFont08;
SpriteData sprGameFont09;
SpriteData sprGameFontCombo;

//SpriteDataの実体(アニメーションのため配列
SpriteData sprLoading[12];
SpriteData sprHitEffect[16];
SpriteData sprWind[49];
SpriteData sprCancel[30];
SpriteData sprFrame[70];

SpriteData sprKeyX;
SpriteData sprKeyY;
SpriteData sprKeyB;
SpriteData sprKeyA;
SpriteData sprKeyR;
SpriteData sprKeyL;
SpriteData sprArrowKeyL;
SpriteData sprArrowKeyLU;
SpriteData sprArrowKeyU;
SpriteData sprArrowKeyRU;
SpriteData sprArrowKeyR;
SpriteData sprArrowKeyRD;
SpriteData sprArrowKeyD;
SpriteData sprArrowKeyLD;

SpriteData sprBlackOut;

SpriteData* sprGameFontNum[] =
{
	&sprGameFont00,
	&sprGameFont01,
	&sprGameFont02,
	&sprGameFont03,
	&sprGameFont04,
	&sprGameFont05,
	&sprGameFont06,
	&sprGameFont07,
	&sprGameFont08,
	&sprGameFont09,
};




//ロード関数
void SpriteLoadLoading()
{
	SprInstance::Load = new Sprite("Assets/Sprites/load.png");

	sprLoading->AllInit(SprInstance::Load, 12, 4, VECTOR2(300, 300));
}

void SpriteUnLoadLoading()
{
	DELETE_IF(SprInstance::Load);
}
void SpriteLoadBattle()
{
	SprInstance::Life = new Sprite("Assets/Sprites/Life.png");
	SprInstance::ArrowH = new Sprite("Assets/Sprites/ArrowH.png");
	SprInstance::ArrowW = new Sprite("Assets/Sprites/ArrowW.png");
	SprInstance::ArrowX = new Sprite("Assets/Sprites/ArrowX.png");
	SprInstance::ArrowY = new Sprite("Assets/Sprites/ArrowY.png");
	SprInstance::BottunHit = new Sprite("Assets/Sprites/BottunHit.png");
	SprInstance::BottunHurt = new Sprite("Assets/Sprites/BottunHurt.png");
	SprInstance::ControlerXYWH = new Sprite("Assets/Sprites/ControlerXYWH.png");
	SprInstance::Counter = new Sprite("Assets/Sprites/counter.png");
	SprInstance::HitEffect = new Sprite("Assets/Sprites/HitEffect.png");
	SprInstance::RoundCall = new Sprite("Assets/Sprites/RoundCall.png");
	SprInstance::Wind = new Sprite("Assets/Sprites/wind.png");
	SprInstance::GameFont = new Sprite("Assets/Sprites/GameFont.png");
	SprInstance::WinnerTag = new Sprite("Assets/Sprites/WinTag.png");
	SprInstance::FireBall = new Sprite("Assets/Sprites/FireBall.png");
	SprInstance::Cancel = new Sprite("Assets/Sprites/cancelEffect.png");
	SprInstance::Frame = new Sprite("Assets/Sprites/frame.png");
	SprInstance::Key = new Sprite("Assets/Sprites/KeyUI.png");
	SprInstance::ArrowKey = new Sprite("Assets/Sprites/arrowAll.png");
	SprInstance::BlackOut = new Sprite("Assets/Sprites/blackOut.png");




	sprLife.Init(SprInstance::Life, 768 * 0, 64 * 0, 768, 64, 0, 64 / 2);
	sprLifeFrame.Init(SprInstance::Life, 768 * 0, 64 * 1, 770, 68, 0, 68 / 2);
	sprArts.Init(SprInstance::Life, 360 * 0, 132, 360, 30, 0, 30 / 2);
	sprArtsFrame.Init(SprInstance::Life, 360 * 0,132+30, 360, 32, 0, 32 / 2);


	sprBottunHit.Init(SprInstance::BottunHit, 256 * 0, 128 * 0, 256, 128, 256 / 2, 128 / 2);
	sprBottunHurt.Init(SprInstance::BottunHurt, 256 * 0, 128 * 0, 256, 128, 256 / 2, 128 / 2);
	sprAllControler.Init(SprInstance::ControlerXYWH, 0, 0, 32, 32, 16, 16);
	sprWControler.Init(SprInstance::ArrowW, 0, 0, 96, 32, 48, 16);
	sprHControler.Init(SprInstance::ArrowH, 0, 0, 32, 96, 16, 48);
	sprXControler.Init(SprInstance::ArrowX, 0, 0, 96, 32, 48, 16);
	sprYControler.Init(SprInstance::ArrowY, 0, 0, 32, 96, 16, 48);
	sprRoundCallRound1.Init(SprInstance::RoundCall, 512 * 0, 128 * 0, 512, 128, 256, 64);
	sprRoundCallRound2.Init(SprInstance::RoundCall, 512 * 0, 128 * 1, 512, 128, 256, 64);
	sprRoundCallRound3.Init(SprInstance::RoundCall, 512 * 0, 128 * 6, 512, 128, 256, 64);
	sprRoundCallFight.Init(SprInstance::RoundCall, 512 * 0, 128 * 2, 512, 128, 256, 64);
	sprRoundCallWin.Init(SprInstance::RoundCall, 512 * 0, 128 * 3, 512, 128, 256, 64);
	sprRoundCallPlayer1.Init(SprInstance::RoundCall, 512 * 0, 128 * 4, 512, 128, 256, 64);
	sprRoundCallPlayer2.Init(SprInstance::RoundCall, 512 * 0, 128 * 5, 512, 128, 256, 64);
	sprWinTag.Init(SprInstance::WinnerTag, 256 * 0, 256 * 0, 256, 256, 256 / 2, 256 / 2);
	sprCounter.Init(SprInstance::Counter, 450 * 0, 130 * 0, 450, 130, 450 / 2, 130 / 2);
	sprGameFont00.Init(SprInstance::GameFont, 86 * 0, 64 * 0, 86, 64, 43, 32);
	sprGameFont01.Init(SprInstance::GameFont, 86 * 1, 64 * 0, 86, 64, 43, 32);
	sprGameFont02.Init(SprInstance::GameFont, 86 * 2, 64 * 0, 86, 64, 43, 32);
	sprGameFont03.Init(SprInstance::GameFont, 86 * 0, 64 * 1, 86, 64, 43, 32);
	sprGameFont04.Init(SprInstance::GameFont, 86 * 1, 64 * 1, 86, 64, 43, 32);
	sprGameFont05.Init(SprInstance::GameFont, 86 * 2, 64 * 1, 86, 64, 43, 32);
	sprGameFont06.Init(SprInstance::GameFont, 86 * 0, 64 * 2, 86, 64, 43, 32);
	sprGameFont07.Init(SprInstance::GameFont, 86 * 1, 64 * 2, 86, 64, 43, 32);
	sprGameFont08.Init(SprInstance::GameFont, 86 * 2, 64 * 2, 86, 64, 43, 32);
	sprGameFont09.Init(SprInstance::GameFont, 86 * 0, 64 * 3, 86, 64, 43, 32);
	sprGameFontCombo.Init(SprInstance::GameFont, 86 * 1, 64 * 3, 86 * 2, 64, 86, 32);


	sprHitEffect->AllInit(SprInstance::HitEffect, 16, 4, VECTOR2(1024, 1024));
	sprWind->AllInit(SprInstance::Wind, 49, 7, VECTOR2(300, 300));
	sprCancel->AllInit(SprInstance::Cancel, 30, 6, VECTOR2(700, 700));

	sprFrame->AllInit(SprInstance::Frame, 70, 14, VECTOR2(120, 220));


	sprKeyX.Init(SprInstance::Key, 161 * 0, 126 * 0, 161, 126, 80, 63);
	sprKeyY.Init(SprInstance::Key, 161 * 1, 126 * 0, 161, 126, 80, 63);
	sprKeyB.Init(SprInstance::Key, 161 * 2, 126 * 0, 161, 126, 80, 63);
	sprKeyA.Init(SprInstance::Key, 161 * 3, 126 * 0, 161, 126, 80, 63);
	sprKeyR.Init(SprInstance::Key, 161 * 4, 126 * 0, 161, 126, 80, 63);
	sprKeyL.Init(SprInstance::Key, 161 * 5, 126 * 0, 161, 126, 80, 63);

	sprArrowKeyL.Init(SprInstance::ArrowKey, 64 * 0, 64 * 0, 64, 64, 50, 32);
	sprArrowKeyLU.Init(SprInstance::ArrowKey, 64 * 1, 64 * 0, 64, 64,50, 32);
	sprArrowKeyU.Init(SprInstance::ArrowKey, 64 * 2, 64 * 0, 64, 64, 50, 32);
	sprArrowKeyRU.Init(SprInstance::ArrowKey, 64 * 3, 64 * 0, 64, 64,50, 32);
	sprArrowKeyR.Init(SprInstance::ArrowKey, 64 * 4, 64 * 0, 64, 64, 50, 32);
	sprArrowKeyRD.Init(SprInstance::ArrowKey, 64 * 5, 64 * 0, 64, 64,50, 32);
	sprArrowKeyD.Init(SprInstance::ArrowKey, 64 * 6, 64 * 0, 64, 64, 50, 32);
	sprArrowKeyLD.Init(SprInstance::ArrowKey, 64 * 7, 64 * 0, 64, 64,50, 32);
	sprBlackOut.Init(SprInstance::BlackOut, 1920 * 0, 1080 * 0, 1920, 1080, 960, 540);
}
void SpriteUnLoadBattle()
{
	DELETE_IF(SprInstance::Life);
	DELETE_IF(SprInstance::ArrowH);
	DELETE_IF(SprInstance::ArrowW);
	DELETE_IF(SprInstance::ArrowX);
	DELETE_IF(SprInstance::ArrowY);
	DELETE_IF(SprInstance::BottunHit);
	DELETE_IF(SprInstance::BottunHurt);
	DELETE_IF(SprInstance::ControlerXYWH);
	DELETE_IF(SprInstance::Counter);
	DELETE_IF(SprInstance::HitEffect);
	DELETE_IF(SprInstance::RoundCall);
	DELETE_IF(SprInstance::Wind);
	DELETE_IF(SprInstance::GameFont);
	DELETE_IF(SprInstance::WinnerTag);
	DELETE_IF(SprInstance::FireBall);
	DELETE_IF(SprInstance::Cancel);
	DELETE_IF(SprInstance::Frame);
	DELETE_IF(SprInstance::Key);
	DELETE_IF(SprInstance::ArrowKey);
	DELETE_IF(SprInstance::BlackOut);



}
void SpriteLoadTitle()
{}
void SpriteUnLoadTitle()
{

}
void SpriteUnLoadMenu()
{
	DELETE_IF(SprInstance::ModeIcon);



}
void SpriteLoadMenu()
{
	SprInstance::ModeIcon = new Sprite("Assets/Sprites/ModeIcon.png");
	sprModeTraning.Init(SprInstance::ModeIcon, 0, 128 * 0, 512, 128, 256, 64);
	sprModeVersus.Init(SprInstance::ModeIcon, 0, 128 * 1, 512, 128, 256, 64);
	sprModeArcade.Init(SprInstance::ModeIcon, 0, 128 * 2, 512, 128, 256, 64);
	sprModeCharange.Init(SprInstance::ModeIcon, 0, 128 * 3, 512, 128, 256, 64);
	sprModeOption.Init(SprInstance::ModeIcon, 0, 128 * 4, 512, 128, 256, 64);
}
void SpriteLoadSelect()
{
	SprInstance::ChSelectIcon = new Sprite("Assets/Sprites/ChSelectIcon.png");
	sprChSelect.Init(SprInstance::ChSelectIcon, 128 * 0, 128 * 0, 256, 128, 256 / 2, 128 / 2);
	sprSelectCur.Init(SprInstance::ChSelectIcon, 128 * 0, 128 * 1, 128, 128, 128 / 2, 128 / 2);
}
void SpriteUnLoadSelect()
{
	DELETE_IF(SprInstance::ChSelectIcon);

}


AnimeData AnimeUILoading[] =
{
	{ &sprLoading[0],3 },
	{ &sprLoading[1],3 },
	{ &sprLoading[2],3 },
	{ &sprLoading[3],3 },
	{ &sprLoading[4],3 },
	{ &sprLoading[5],3 },
	{ &sprLoading[6],3 },
	{ &sprLoading[7],3 },
	{ &sprLoading[8],3 },
	{ &sprLoading[9],3 },
	{ &sprLoading[10],3 },
	{ &sprLoading[11],3 },
	{ nullptr,-1 },
};

AnimeData AnimeHitEffect[] =
{
	{ &sprHitEffect[0],0 },
	{ &sprHitEffect[1],0 },
	{ &sprHitEffect[2],0 },
	{ &sprHitEffect[3],0 },
	{ &sprHitEffect[4],0 },
	{ &sprHitEffect[5],0 },
	{ &sprHitEffect[6],0 },
	{ &sprHitEffect[7],0 },
	{ &sprHitEffect[8],0 },
	{ &sprHitEffect[9],0 },
	{ &sprHitEffect[10],0 },
	{ &sprHitEffect[11],0 },
	{ &sprHitEffect[12],0 },
	{ &sprHitEffect[13],0 },
	{ &sprHitEffect[14],0 },
	{ &sprHitEffect[15],0 },
	{ nullptr,-1 },
};

AnimeData AnimeWind[] =
{
	{&sprWind[0],1},
	{&sprWind[1],1},
	{&sprWind[2],1},
	{&sprWind[3],1},
	{&sprWind[4],1},
	{&sprWind[5],1},
	{&sprWind[6],1},
	{&sprWind[7],1},
	{&sprWind[8],1},
	{&sprWind[9],1},
	{&sprWind[10],1},
	{&sprWind[11],1},
	{&sprWind[12],1},
	{&sprWind[13],1},
	{&sprWind[14],1},
	{&sprWind[15],1},
	{&sprWind[16],1},
	{&sprWind[17],1},
	{&sprWind[18],1},
	{&sprWind[19],1},
	{&sprWind[20],1},
	{&sprWind[21],1},
	{&sprWind[22],1},
	{&sprWind[23],1},
	{&sprWind[24],1},
	{&sprWind[25],1},
	{&sprWind[26],1},
	{&sprWind[27],1},
	{&sprWind[28],1},
	{&sprWind[29],1},
	{&sprWind[30],1},
	{&sprWind[31],1},
	{&sprWind[32],1},
	{&sprWind[33],1},
	{&sprWind[34],1},
	{&sprWind[35],1},
	{&sprWind[36],1},
	{&sprWind[37],1},
	{&sprWind[38],1},
	{&sprWind[39],1},
	{&sprWind[40],1},
	{&sprWind[41],1},
	{&sprWind[42],1},
	{&sprWind[43],1},
	{&sprWind[44],1},
	{&sprWind[45],1},
	{&sprWind[46],1},
	{&sprWind[47],1},
	{&sprWind[48],1},
	{nullptr,-1 }

};

AnimeData AnimeCanel[] =
{
	{ &sprCancel[0],1 },
	{ &sprCancel[1],1 },
	{ &sprCancel[2],1 },
	{ &sprCancel[3],1 },
	{ &sprCancel[4],1 },
	{ &sprCancel[5],1 },
	{ &sprCancel[6],1 },
	{ &sprCancel[7],1 },
	{ &sprCancel[8],1 },
	{ &sprCancel[9],1 },
	{ &sprCancel[10],1 },
	{ &sprCancel[11],1 },
	{ &sprCancel[12],1 },
	{ &sprCancel[13],1 },
	{ &sprCancel[14],1 },
	{ &sprCancel[15],1 },
	{ &sprCancel[16],1 },
	{ &sprCancel[17],1 },
	{ &sprCancel[18],1 },
	{ &sprCancel[19],1 },
	{ &sprCancel[20],1 },
	{ &sprCancel[21],1 },
	{ &sprCancel[22],1 },
	{ &sprCancel[23],1 },
	{ &sprCancel[24],1 },
	{ &sprCancel[25],1 },
	{ &sprCancel[26],1 },
	{ &sprCancel[27],1 },
	{ &sprCancel[28],1 },
	{ &sprCancel[29],1 },
	
	{ nullptr,-1 }

};

AnimeData AnimeFrame[] =
{
	{ &sprFrame[0],1 },
	{ &sprFrame[1],1 },
	{ &sprFrame[2],1 },
	{ &sprFrame[3],1 },
	{ &sprFrame[4],1 },
	{ &sprFrame[5],1 },
	{ &sprFrame[6],1 },
	{ &sprFrame[7],1 },
	{ &sprFrame[8],1 },
	{ &sprFrame[9],1 },
	{ &sprFrame[10],1 },
	{ &sprFrame[11],1 },
	{ &sprFrame[12],1 },
	{ &sprFrame[13],1 },
	{ &sprFrame[14],1 },
	{ &sprFrame[15],1 },
	{ &sprFrame[16],1 },
	{ &sprFrame[17],1 },
	{ &sprFrame[18],1 },
	{ &sprFrame[19],1 },
	{ &sprFrame[20],1 },
	{ &sprFrame[21],1 },
	{ &sprFrame[22],1 },
	{ &sprFrame[23],1 },
	{ &sprFrame[24],1 },
	{ &sprFrame[25],1 },
	{ &sprFrame[26],1 },
	{ &sprFrame[27],1 },
	{ &sprFrame[28],1 },
	{ &sprFrame[29],1 },
	{ &sprFrame[30],1 },
	{ &sprFrame[31],1 },
	{ &sprFrame[32],1 },
	{ &sprFrame[33],1 },
	{ &sprFrame[34],1 },
	{ &sprFrame[35],1 },
	{ &sprFrame[36],1 },
	{ &sprFrame[37],1 },
	{ &sprFrame[38],1 },
	{ &sprFrame[39],1 },
	{ &sprFrame[40],1 },
	{ &sprFrame[41],1 },
	{ &sprFrame[42],1 },
	{ &sprFrame[43],1 },
	{ &sprFrame[44],1 },
	{ &sprFrame[45],1 },
	{ &sprFrame[46],1 },
	{ &sprFrame[47],1 },
	{ &sprFrame[48],1 },
	{ &sprFrame[49],1 },
	{ &sprFrame[50],1 },
	{ &sprFrame[51],1 },
	{ &sprFrame[52],1 },
	{ &sprFrame[53],1 },
	{ &sprFrame[54],1 },
	{ &sprFrame[55],1 },
	{ &sprFrame[56],1 },
	{ &sprFrame[57],1 },
	{ &sprFrame[58],1 },
	{ &sprFrame[59],1 },
	{ &sprFrame[60],1 },
	{ &sprFrame[61],1 },
	{ &sprFrame[62],1 },
	{ &sprFrame[63],1 },
	{ &sprFrame[64],1 },
	{ &sprFrame[65],1 },
	{ &sprFrame[66],1 },
	{ &sprFrame[67],1 },
	{ &sprFrame[68],1 },
	{ &sprFrame[69],1 },

	{ nullptr,-1 }

};

