#include "../Headers/Player.h"

#include "../Headers/SceneBattle.h"
#include "../GameLib/Headers/RectRender.h"
#include "../GameLib/Headers/DxAudio.h"
#include "../Headers/Effect.h"
#include "../Headers/UIMenu.h"
#include "../GameLib/Math/Math.h"





//********************************************************
//プレイヤー処理
//********************************************************


//**************************************************
//
//　　　　　　　　　プレイヤー初期化
//
//**************************************************
void Player::Init(int pad)
{


	//シェーダー作成
	plShader = new Shader();
	plShader->Create(L"Shaders/Player.fx", "VSChara", "PSChara");
	plMesh = new FBXMesh();


	plShaderOL = new Shader();
	plShaderOL->Create(L"Shaders/Player.fx", "VSOutLine", "PSOutLine");





	//モーション追加一括
	AddMotionAll();

	//パラメータ全初期化
	InitParameter(pad);

	//行列更新
	plMesh->Update(true);
	plMesh->Play(PLAYER_MOTION::NEUTRAL);
	comboCnt = 0;

	//コマンド判定用実体の初期化
	commandRun.Init(COM_DATA::comDataRun);
	commandBackStep.Init(COM_DATA::comDataBackStep);

	commandRunUP.Init(COM_DATA::comDataRunUP);
	commandBackStepUP.Init(COM_DATA::comDataBackStepUP);


	command623C.Init(COM_DATA::comData623C);
	command623C2.Init(COM_DATA::comData623C2);

	command214A.Init(COM_DATA::comData214A);
	command214B.Init(COM_DATA::comData214B);
	command214C.Init(COM_DATA::comData214C);

	command236A.Init(COM_DATA::comData236A);
	command236B.Init(COM_DATA::comData236B);
	command236C.Init(COM_DATA::comData236C);


}

//**************************************************
//
//　　　　　　　　　プレイヤー更新
//
//**************************************************
void Player::Update()
{

	using namespace Input;

	//-----------------------------------------------------
	//2D処理
	//-----------------------------------------------------

	//キー入力取得
	keyState = inputManager->GetPadAddress()[pad].state;//キーステート
	keyTrg = inputManager->GetPadAddress()[pad].trigger;//キートリガー

	if (inputManager->oldRTrigger[pad]<0.8 && inputManager->GetPadAddress()[pad].right > 0.8)//キートリガー
		keyTrg = PAD_R2;


	

	//向きによって左右キー情報のビット反転
	if ((dir == DIR_LEFT) && (keyState & PAD_WALK) != 0)
		keyState ^= PAD_WALK;
	if ((dir == DIR_LEFT) && (keyTrg & PAD_WALK) != 0)
		keyTrg ^= PAD_WALK;


	//ラウンドコール中か否か
	if (sceneBattle->timer < ROUND_CALL_TIME) {
		//操作不能
		sceneBattle->isInput = false;
	}
	else if (!sceneBattle->isDead)
		sceneBattle->isInput = true;



	//CPUフラグが立っていれば
	if (isCPU)
		CPUInput(keyTrg, keyState);


	//キー入力可能であれば
	if (sceneBattle->isInput) {

		//コマンド入力
		PlyerCommand(keyTrg, keyState);

		//攻撃キー入力
		PlyerInput(keyTrg, keyState);



	}
	else {
		//キー入力を受け付けない
		keyState = 0;
		keyTrg = 0;
	}

	CancelMove(keyTrg);



	if (!sceneBattle->isStop && !isStop) {

		CancelStateMove();

		//重力加算
		speed.y += this->GRAVITY;

		//ダメージ、ガード処理実行
		if (isDamage)
			PlayDamage();
		else if (isGuard)
			PlayGuard();



		//行動分岐
		PlyerMove(keyTrg, keyState);
	}
	//押し合い
	PushMove();
	if (isDamage)isStop = false;


}

//**************************************************
//
//　　　　　　　　　プレイヤー描画
//
//**************************************************
void Player::Render(ID3D11Buffer* constantBuffer)
{





	//--------------------------------------------
	//3D描画処理
	//--------------------------------------------


	//定数バッファ用
	SceneBattle::ConstantBufferParam cb;


	//定数バッファにセットOrtho
	cb.view = gameCamera->GetViewMATRIX();
	cb.proj = gameCamera->GetProjectionMATRIXBlend();


	//プレイヤー描画(平行投影)
	cb.world = plMesh->transform;
	cb.wvp = plMesh->transform * cb.view * cb.proj;
	cb.color = this->color;


	//頂点情報セット
	DxSystem::deviceContext->VSSetConstantBuffers(0, 1, &constantBuffer);
	//定数バッファ更新
	DxSystem::deviceContext->UpdateSubresource(constantBuffer, 0, NULL, &cb, 0, 0);
	DxSystem::deviceContext->OMSetDepthStencilState(DxSystem::depthStencilState, 0);





	//メッシュ描画(輪郭線あり

	if (dir == DIR_RIGHT) {

		////プレイヤー輪郭線用のシェーダー有効化
		plShaderOL->Activate();
		//メッシュ描画(輪郭線描画
		plMesh->Render();
		DxSystem::CullFront();
		//プレイヤー用のシェーダー有効化
		plShader->Activate();
		//メッシュ描画
		plMesh->Render();
		DxSystem::CullBack();
	}
	else {
		DxSystem::CullFront();
		//輪郭線用のシェーダー有効化
		plShaderOL->Activate();
		//メッシュ描画(輪郭線描画
		plMesh->Render();
		DxSystem::CullBack();
		//プレイヤー用のシェーダー有効化
		plShader->Activate();
		//メッシュ描画(輪郭線描画
		plMesh->Render();
		
	}

//--------------------------------------------
//当たり判定矩形描画( D key で切り替え )
//--------------------------------------------
	RectRender();

}
//--------------------------------------------
//当たり判定矩形描画( D key で切り替え )
//--------------------------------------------
void Player::RectRender()
{
	//Dキーで切り替え
	if (inputManager->GetPadAddress()[0].trigger&Input::KEY_D)isRectRender = !isRectRender;


	if (!isRectRender)return;


	//すべての当たり判定を描画する
	for (int i = 0; i < RECT_MAX; i++)
	{
		if (rectData[i].motionName != plMesh->motionName)continue;//モーションが違えば無視

		if (plMesh->nowFrame<rectData[i].start ||
			plMesh->nowFrame>rectData[i].start + rectData[i].keep)continue;//持続フレーム間でなければ無視

																		   //矩形描画
		rectRender->Render(
			VECTOR2(position.x + rectData[i].position.x*dir - gameCamera->scroll.x,
				position.y + rectData[i].position.y - gameCamera->scroll.y),
			rectData[i].size,
			rectData[i].color);

	}
	//矩形描画(押し合い判定
	rectRender->Render(
		VECTOR2(position.x + pushRectData.position.x - gameCamera->scroll.x,
			position.y + pushRectData.position.y - gameCamera->scroll.y),
		pushRectData.size,
		pushRectData.color);
}

