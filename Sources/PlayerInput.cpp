#include "../Headers/Player.h"
#include "../Headers/PlayerConstant.h"
#include "../Headers/Effect.h"
#include "../Headers/SceneBattle.h"
#include "../Headers/UIBattle.h"
#include "../Headers/UIMenu.h"

//*********************************************************
//
//				入力関連
//
//*********************************************************
using namespace Input;

void Player::PlyerInput(int keyTrg, int keyState)
{

	//---------------------------------------------
	//先行入力処理
	//---------------------------------------------

	//何かが押されたらそのキーをバッファに保存
	if (keyTrg) {

		CreateKeyLogger();
		for (int i = 0; i < KEEP_MAX; i++) {

			if (keyData[i].keyBit == 0) {
				keyData[i].keyBit = keyTrg;

				break;
			}
		}
	}
	//キーをバッファ更新
	for (int i = 0; i < KEEP_MAX; i++) {

		//保持フレーム加算
		if (keyData[i].keyBit != 0) {
			keyData[i].frame++;
		}
		//保持フレームが一定以上になると解放
		if (keyData[i].frame > KEEP_FRAME) {
			keyData[i].frame = 0;
			keyData[i].keyBit = 0;
		}
	}





	//ロマンキャンセル
	if (isCancel &&
		IsTRG(PAD_L1, keyTrg) &&
		artsPoint >= ARTS_MAX*0.25f) {

		isCancelStandBy = true;
		isActive = false;
		isCommand = false;



	}

	if ((isActive || moveState == PLAYER_MOVE::GUARD) &&
		IsTRG(PAD_R1, keyTrg) &&
		artsPoint >= ARTS_MAX*0.25f&&
		!isAir)
	{
		moveState = PLAYER_MOVE::ATEMI_READY;
		frameCnt = 0;
		Obj2D* obj = effectManager->Create(EffectManager::Move::Cancel, position);
		obj->color = VECTOR4(0, 1, 0, 1);
		isCancel = false;
		ArtsAdd(-ARTS_MAX*0.25f);
		speed.x = 0;
		knockbackSpeed.x = 0;
	}


	//行動不能時は判定しない
	if (!isActive)return;

	if (IsTRG(PAD_R2, keyTrg)&& !isAir)
	{

		isRollingSB = true;

	}



	if (IsTRGSame(PAD_TRG1, PAD_TRG2, keyTrg) && (!isAir))
	{
		isCancel = false;
		isHit = false;
		plMesh->Play(PLAYER_MOTION::THROW);
		subState = 0;
		moveCnt = 0;
		moveState = PLAYER_MOVE::ATTACK_THROW;

	}
	else if (IsTRG(PAD_TRG1, keyTrg))
	{

		isHit = false;

		if (isAir) {


			plMesh->Play(PLAYER_MOTION::JUMP_ATTACK_A);
			moveState = PLAYER_MOVE::ATTACK_JA;

		}
		else if (keyState&PAD_DOWN) {
			plMesh->Play(PLAYER_MOTION::LOW_ATTACK_A);
			moveCnt = 0;
			moveState = PLAYER_MOVE::ATTACK_LA;
			subState = 0;
		}

		else {
			plMesh->Play(PLAYER_MOTION::STAND_ATTACK_A);
			subState = 0;
			moveCnt = 0;
			moveState = PLAYER_MOVE::ATTACK_NA;
		}
	}


	else if (IsTRG(PAD_TRG2, keyTrg))
	{
		isHit = false;

		if (isAir) {
			if (subState == 1) {
				plMesh->Play(PLAYER_MOTION::JUMP_ATTACK_B);
				moveState = PLAYER_MOVE::ATTACK_JB;
			}
		}
		else if (keyState&PAD_DOWN) {
			plMesh->Play(PLAYER_MOTION::LOW_ATTACK_B);
			subState = 0;
			moveCnt = 0;
			moveState = PLAYER_MOVE::ATTACK_LB;

			speed.x = 50;
		}

		else {
			plMesh->Play(PLAYER_MOTION::STAND_ATTACK_B);
			subState = 0;
			moveCnt = 0;
			moveState = PLAYER_MOVE::ATTACK_NB;
		}
	}
	else if (IsTRG(PAD_TRG3, keyTrg)) {

		isHit = false;

		if (isAir) {
			if (subState == 1) {
				plMesh->Play(PLAYER_MOTION::JUMP_ATTACK_C);
				moveState = PLAYER_MOVE::ATTACK_JC;
			}
		}
		else if (keyState&PAD_DOWN) {
			plMesh->Play(PLAYER_MOTION::LOW_ATTACK_C);
			subState = 0;
			moveCnt = 0;
			moveState = PLAYER_MOVE::ATTACK_LC;
		}
		else {
			plMesh->Play(PLAYER_MOTION::STAND_ATTACK_C);
			subState = 0;
			moveCnt = 0;
			moveState = PLAYER_MOVE::ATTACK_NC;
		}


	}

	else if (IsTRG(PAD_TRG4, keyTrg)) {

		isHit = false;

		if (isAir) {

		}

		else {
			plMesh->Play(PLAYER_MOTION::STAND_ATTACK_EX);
			subState = 0;
			moveCnt = 0;
			moveState = PLAYER_MOVE::STAND_ATTACK_EX;
		}


	}

}

void Player::PlyerCommand(int keyTrg, int keyState)
{


	//
	//地上にいる場合に出せる技
	//
	if (isActive) {

		if (commandRun.Parse(keyTrg)|| commandRunUP.Parse(keyTrg)) {

			if (isAir&& position.y < GROUND - 200) {

				if (airMoveCnt <= 0)
					return;

				--airMoveCnt;
				plMesh->Play(PLAYER_MOTION::AIR_RUN);
				moveState = PLAYER_MOVE::RUNAIR;
				speed = VECTOR2(38, 0);
			}
			else {

				plMesh->Play(PLAYER_MOTION::RUN);
				moveState = PLAYER_MOVE::RUN;
			}
			return;
		}
		else if (commandBackStep.Parse(keyTrg)|| commandBackStepUP.Parse(keyTrg)) {

			if (isAir&&position.y < GROUND - 200) {

				if (airMoveCnt <= 0)
					return;

				--airMoveCnt;
				plMesh->Play(PLAYER_MOTION::AIR_RUN_BACK);
				moveState = PLAYER_MOVE::BACK_DASH;
				speed = VECTOR2(-38, 0);
			}
			else {
				speed = VECTOR2(commonParam[PLAYER_PARAM::BUCK_STEP_SPEEDX],
					commonParam[PLAYER_PARAM::BUCK_STEP_SPEEDY]);
				plMesh->Play(PLAYER_MOTION::BACK_STEP);
				isActive = false;

				moveState = PLAYER_MOVE::BACKSTEP;
			}
		}

	}




	if (command236A.Parse(keyState)) {

		if (!isCommand)return;
		if (isAir)return;


		plMesh->Play(PLAYER_MOTION::ARTS01);
		isHit = false;

		moveState = PLAYER_MOVE::ARTS01A;
		isCommand = false;
		isActive = false;

	}

	if (command236B.Parse(keyState)) {

		if (!isCommand)return;
		if (isAir)return;


		plMesh->Play(PLAYER_MOTION::ARTS01);
		isHit = false;

		moveState = PLAYER_MOVE::ARTS01B;
		isCommand = false;
		isActive = false;

	}
	if (command236C.Parse(keyState)) {

		if (!isCommand)return;
		if (isAir)return;


		plMesh->Play(PLAYER_MOTION::ARTS01);
		isHit = false;

		moveState = PLAYER_MOVE::ARTS01C;
		isCommand = false;
		isActive = false;

	}

	if (command214A.Parse(keyState)) {

		if (!isCommand)return;
		if (isAir)return;



		plMesh->Play(PLAYER_MOTION::ARTS02);
		isHit = false;

		moveState = PLAYER_MOVE::ARTS02A;
		isCommand = false;
		isActive = false;

	}

	if (command214B.Parse(keyState)) {

		if (!isCommand)return;
		if (isAir)return;



		plMesh->Play(PLAYER_MOTION::ARTS02);
		isHit = false;

		moveState = PLAYER_MOVE::ARTS02B;
		isCommand = false;
		isActive = false;

	}

	if (command214C.Parse(keyState)) {

		if (!isCommand)return;
		if (isAir)return;



		plMesh->Play(PLAYER_MOTION::ARTS02);
		isHit = false;

		moveState = PLAYER_MOVE::ARTS02C;
		isCommand = false;
		isActive = false;

	}



}

bool Player::IsTRG(int keyBit, int keyTrg)
{
	//バッファに保存したキー入力も判定する

	for (int i = 0; i < KEEP_MAX; i++) {

		//もし入力されたキーが配列の中にあれば
		if (keyData[i].keyBit&keyBit) {

			//一括初期化
			for (int j = 0; j < KEEP_MAX; j++) {
				keyData[j].keyBit = 0;
				keyData[j].frame = 0;
			}

			return true;

		}
	}
	return false;
}
bool Player::IsTRGSame(int keyBit1, int keyBit2, int keyTrg)
{
	//バッファに保存したキー入力も判定する
	Input::KeyStackData key1;
	Input::KeyStackData key2;
	key1.Init();
	key2.Init();
	for (int i = 0; i < KEEP_MAX; i++) {

		//もし入力されたキーが配列の中にあればフレーム情報保存
		if (keyData[i].keyBit&keyBit1) {

			key1 = keyData[i];

		}
		if (keyData[i].keyBit&keyBit2) {

			key2 = keyData[i];

		}


		//5フレーム以内に押されたら同時押し
		if (key1.keyBit != 0 && key2.keyBit != 0 && (fabs(key1.frame - key2.frame) <= 5)) {

			for (int j = 0; j < KEEP_MAX; j++) {
				keyData[j].frame = 0;
				keyData[j].keyBit = 0;
			}
			key1.Init();
			key2.Init();

			return true;

		}
	}
	return false;
}



//キャンセル入力受付
void Player::CancelMove(int keyTrg)
{
	using namespace Input;
	//ヒットしていない場合リターン

	if (!isHit)return;
	if (cancelState)return;
	int nowFrame = plMesh->GetMotionFrame(GetMotionName());
	if (nowFrame < 10)return;

	switch (moveState)
	{
	case PLAYER_MOVE::ATTACK_NA:

		isCommand = true;

		if (IsTRG(PAD_TRG1, keyTrg))
		{
			cancelState = PLAYER_CANCEL::ATTACK_NA;

			break;
		}
		else if (IsTRG(PAD_TRG2, keyTrg))
		{

			cancelState = PLAYER_CANCEL::ATTACK_NB;
			knockbackSpeed = {};
			enemy->knockbackSpeed = {};			break;
		}
		else if (IsTRG(PAD_TRG3, keyTrg))
		{

			cancelState = PLAYER_CANCEL::ATTACK_NC;
			knockbackSpeed = {};
			enemy->knockbackSpeed = {};
			break;
		}

		break;

	case PLAYER_MOVE::ATTACK_NB:

		isCommand = true;

		if (IsTRG(PAD_TRG3, keyTrg))
		{

			cancelState = PLAYER_CANCEL::ATTACK_NC;
			knockbackSpeed = {};
			enemy->knockbackSpeed = {};
			break;
		}
		break;
	case PLAYER_MOVE::ATTACK_NC:

		isCommand = true;


		if (keyState&PAD_DOWN&&IsTRG(PAD_TRG2, keyTrg))
		{

			cancelState = PLAYER_CANCEL::ATTACK_LB;
			knockbackSpeed = {};
			enemy->knockbackSpeed = {};
			break;
		}

		break;

	case PLAYER_MOVE::ATTACK_JA:

		isCommand = true;

		if (IsTRG(PAD_TRG2, keyTrg))
		{

			cancelState = PLAYER_CANCEL::ATTACK_JB;
			knockbackSpeed = {};
			enemy->knockbackSpeed = {};			
			break;
		}
		else if (IsTRG(PAD_TRG3, keyTrg))
		{

			cancelState = PLAYER_CANCEL::ATTACK_JC;
			knockbackSpeed = {};
			enemy->knockbackSpeed = {};
			break;
		}

		break;

	case PLAYER_MOVE::ATTACK_JB:

		isCommand = true;

		if (IsTRG(PAD_TRG3, keyTrg))
		{

			cancelState = PLAYER_CANCEL::ATTACK_JC;
			knockbackSpeed = {};
			enemy->knockbackSpeed = {};
			break;
		}
		break;



	}


}


void Player::CancelStateMove()
{
	using namespace Input;
	//ヒットしていない場合リターン

	if (!cancelState)return;

	isHit = false;
	isActive = true;
	isPowerUp = false;


	switch (cancelState)
	{
	case PLAYER_CANCEL::ATTACK_NA:


		plMesh->Play(PLAYER_MOTION::STAND_ATTACK_A);
		subState = 0;
		cancelState = 0;
		moveState = PLAYER_MOVE::ATTACK_NA;

		break;

	case PLAYER_CANCEL::ATTACK_NB:

		plMesh->Play(PLAYER_MOTION::STAND_ATTACK_B);
		subState = 0;
		cancelState = 0;
		moveState = PLAYER_MOVE::ATTACK_NB;
		break;
	case PLAYER_CANCEL::ATTACK_NC:

		plMesh->Play(PLAYER_MOTION::STAND_ATTACK_C);
		subState = 0;
		cancelState = 0;
		moveState = PLAYER_MOVE::ATTACK_NC;
		break;

	case PLAYER_CANCEL::ATTACK_LB:

		plMesh->Play(PLAYER_MOTION::LOW_ATTACK_B);
		subState = 0;
		cancelState = 0;
		speed.x = 50;

		moveState = PLAYER_MOVE::ATTACK_LB;
		break;


	case PLAYER_CANCEL::ATTACK_JB:

		plMesh->Play(PLAYER_MOTION::JUMP_ATTACK_B);
		subState = 0;
		cancelState = 0;
		moveState = PLAYER_MOVE::ATTACK_JB;
		break;

	case PLAYER_CANCEL::ATTACK_JC:

		plMesh->Play(PLAYER_MOTION::JUMP_ATTACK_C);
		subState = 0;
		cancelState = 0;
		moveState = PLAYER_MOVE::ATTACK_JC;
		break;
	}


}

void Player::CreateKeyLogger()
{

	if (menuUI->mode != GAME_MODE::TRANING)
		return;

	if (pad == 0) {


		if (IsTRGSame(Input::PAD_UP, Input::PAD_RIGHT, keyTrg)) {

			Obj2D* child = battleUI->Create(BattleUI::Move::KeyLogger, VECTOR2(100, 200));
			child->parent3D = this;
			child->type = PAD_UP + PAD_RIGHT;

		}
		else if (IsTRGSame(Input::PAD_UP, Input::PAD_LEFT, keyTrg)) {

			Obj2D* child = battleUI->Create(BattleUI::Move::KeyLogger, VECTOR2(100, 200));
			child->parent3D = this;
			child->type = PAD_UP + PAD_LEFT;

		}
		else if (IsTRGSame(Input::PAD_DOWN, Input::PAD_RIGHT, keyTrg)) {

			Obj2D* child = battleUI->Create(BattleUI::Move::KeyLogger, VECTOR2(100, 200));
			child->parent3D = this;
			child->type = PAD_DOWN + PAD_RIGHT;

		}
		else if (IsTRGSame(Input::PAD_DOWN, Input::PAD_LEFT, keyTrg)) {

			Obj2D* child = battleUI->Create(BattleUI::Move::KeyLogger, VECTOR2(100, 200));
			child->parent3D = this;
			child->type = PAD_UP + PAD_RIGHT;

		}
		else {
			Obj2D* child = battleUI->Create(BattleUI::Move::KeyLogger, VECTOR2(100, 200));
			child->parent3D = this;
			child->type = keyTrg;
		}
	}
	else {

		if (IsTRGSame(Input::PAD_UP, Input::PAD_RIGHT, keyTrg)) {

			Obj2D* child = battleUI->Create(BattleUI::Move::KeyLogger, VECTOR2(100, 200));
			child->parent3D = this;
			child->type = PAD_UP + PAD_RIGHT;

		}
		else if (IsTRGSame(Input::PAD_UP, Input::PAD_LEFT, keyTrg)) {

			Obj2D* child = battleUI->Create(BattleUI::Move::KeyLogger, VECTOR2(1820, 200));
			child->parent3D = this;
			child->type = PAD_UP + PAD_LEFT;

		}
		else if (IsTRGSame(Input::PAD_DOWN, Input::PAD_RIGHT, keyTrg)) {

			Obj2D* child = battleUI->Create(BattleUI::Move::KeyLogger, VECTOR2(1820, 200));
			child->parent3D = this;
			child->type = PAD_DOWN + PAD_RIGHT;

		}
		else if (IsTRGSame(Input::PAD_DOWN, Input::PAD_LEFT, keyTrg)) {

			Obj2D* child = battleUI->Create(BattleUI::Move::KeyLogger, VECTOR2(1820, 200));
			child->parent3D = this;
			child->type = PAD_UP + PAD_RIGHT;

		}
		else {
			Obj2D* child = battleUI->Create(BattleUI::Move::KeyLogger, VECTOR2(1820, 200));
			child->parent3D = this;
			child->type = keyTrg;
		}

	}
}

//ジャンプキャンセル入力受付
void Player::JumpCancel(int keyState)
{
	using namespace Input;



	if (keyState&PAD_UP) {

		isPowerUp = false;
		isAir = true;
		isActive = true;
		moveState = PLAYER_MOVE::JUMP;
		subState = 0;
		motionMoveCnt = 0;
		plMesh->Play(PLAYER_MOTION::JUMP_INIT);
		//左右キーが押されていれば
		//その方向へジャンプ
		if (keyState&PAD_RIGHT)
			speed.x = commonParam[PLAYER_PARAM::WALK_SPEED];
		else if (keyState&PAD_LEFT)
			speed.x = -commonParam[PLAYER_PARAM::WALK_SPEED];

	}
}


