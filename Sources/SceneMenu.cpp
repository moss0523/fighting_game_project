#include"../Headers/SceneMenu.h"
#include"../Headers/SceneBattle.h"

#include "../Headers/UIMenu.h"
#include "../Headers/SpriteData.h"

//***************************************************
//	
//		モード選択シーン
//
//***************************************************


//初期化
void SceneMenu::Init()
{
	nextScene = nullptr;

	//レンダーターゲット用の実体初期化
	screen = new RenderTarget();
	screen->Initialize();
	//ブレンド初期化
	blend.Initialize();

	//sprite読み込み
	SpriteLoadMenu();


	menuUI->Init();


}

//更新
void SceneMenu::Update()
{

	int keyTrg = inputManager->GetPadAddress()[0].trigger;

	//Enter押すとシーンチェンジ
	if ((keyTrg&Input::PAD_START)|| (keyTrg&Input::PAD_TRG1)) {
		SceneChange(sceneBattle);
	}
	menuUI->Update();


	

}

//描画
void SceneMenu::Render()
{
	

	//最初にテクスチャにレンダリングする
	screen->Activate();

	//画面クリア
	DxSystem::Clear();

	//画面にレンダリングを戻す
	screen->Deactivate();
	//最後にポストエフェクトをかけて描画
	screen->Render();

	//ブレンドモード設定
	blend.Set(blend.BS_ALPHA);

	//UI描画
	menuUI->Render();

	//画面フリップ
	DxSystem::Flip();
}


void SceneMenu::UnInit()
{
	delete screen;
	SpriteUnLoadMenu();
}
