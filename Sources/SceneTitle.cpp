#include"../Headers/SceneTitle.h"
#include "../Headers/SpriteData.h"
#include "../GameLib/Headers/InputManager.h"
#include"../Headers/SceneMenu.h"

#include "../GameLib/Headers/GameCamera.h"



void SceneTitle::Init()
{
	nextScene = nullptr;

	timer = 0;
	//レンダーターゲット用の実体初期化
	screen = new RenderTarget();
	screen->Initialize();
	//ブレンド初期化
	blend.Initialize();


	//システムフォント
	systemFont = new Sprite("Fonts/font4.png");






}
void SceneTitle::UnInit()
{
	delete screen;
	SpriteUnLoadTitle();
	//レンダーターゲット用の実体初期化
	if (systemFont)delete systemFont;

}

void SceneTitle::Update()
{

	int keyTrg = inputManager->GetPadAddress()[0].trigger;

	//Enter押すとシーンチェンジ
	if ((keyTrg&Input::PAD_START) || (keyTrg&Input::PAD_TRG1))
		SceneChange(sceneMenu);


}

void SceneTitle::Render()
{


	//最初にテクスチャにレンダリングする
	screen->Activate();

	//画面クリア
	DxSystem::Clear();


	//ブレンドモード設定
	blend.Set(blend.BS_ALPHA);


	systemFont->Textout(VECTOR2((float)(DxSystem::SCREEN_WIDTH/2-20*5*4), (float)(DxSystem::SCREEN_HEIGHT / 4)), VECTOR2(5, 5),VECTOR4(1,1,1,1), "FIGHTING");

	//点滅
	if ((++timer / 40) % 2) {
		systemFont->Textout(VECTOR2((float)(DxSystem::SCREEN_WIDTH / 2 - 20 * 3 * 5), (float)(DxSystem::SCREEN_HEIGHT / 2)), VECTOR2(3, 3), VECTOR4(1, 1, 1, 1), "Push_Enter");
	}



	//ブレンドモード設定
	blend.Set(blend.BS_ALPHA);

	//画面にレンダリングを戻す
	screen->Deactivate();
	//最後にポストエフェクトをかけて描画
	screen->Render();


	//画面フリップ
	DxSystem::Flip();
}
