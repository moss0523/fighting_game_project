#include "../Headers/UIMenu.h"
#include "../Headers/SpriteData.h"
#include "../Headers/Common.h"

#include <string>
#include <bitset>


enum
{
	MENU_TRANING,
	MENU_VERSUS,
	MENU_ARCADE,
	MENU_CHARANGE,
	MENU_OPTION,

};


#define MENU_MAX 3

using namespace Input;

static const VECTOR4 COL_BLACK = VECTOR4(0.3f, 0.3f, 0.3f, 1);


void SetModeSprite(Obj2D*obj) {

	switch (obj->index)
	{
	case MENU_TRANING:
		obj->data = &sprModeTraning;
		break;
	case MENU_VERSUS:
		obj->data = &sprModeVersus;
		break;
	case MENU_ARCADE:
		obj->data = &sprModeArcade;
		break;
	case MENU_CHARANGE:
		obj->data = &sprModeCharange;
		break;
	case MENU_OPTION:
		obj->data = &sprModeOption;
		break;
	}

}
//モード選択アイコン
void MenuUI::Move::Rotation(Obj2D * obj)
{


	switch (obj->state)
	{
	case MOVE_STATE::INIT:

		SetModeSprite(obj);
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		obj->scale =ORIGINAL_SCALE;
		obj->position = VECTOR2((float)(DxSystem::SCREEN_WIDTH / 2), (float)(300 + 140 * obj->index));
		obj->state++;

		//break;

	case  MOVE_STATE::MAIN:


		//選択中なら色変更
		if (obj->index == menuUI->mode)
			obj->color = ORIGINAL_COLOR;
		else
			obj->color = COL_BLACK;



		break;

	}
}
void MenuUI::Init()
{
	Obj2DManager::Init();   // Obj2DManagerの初期化


	//モードセレクトUI生成(画像変更のためインデックス設定
	for (int i = 0; i < MENU_MAX; i++)
	{
		Obj2D* obj = menuUI->Create(Move::Rotation, VECTOR2(0, 0));
		obj->index = i;
	}
	mode = 0;
}

void MenuUI::Update()
{
	int keyTrg = inputManager->GetPadAddress()[0].trigger;

	
	
	//モード選択処理
	//上下キーでモード切替
	if (keyTrg&Input::PAD_UP)
		mode = (mode + (MENU_MAX - 1)) % MENU_MAX;
	if (keyTrg&Input::PAD_DOWN)
		mode = (mode + 1) % MENU_MAX;


	Obj2DManager::Update();   // Obj2DManagerの初期化
}

void MenuUI::Render()
{
	Obj2DManager::Render();   // Obj2DManagerの初期化
}



