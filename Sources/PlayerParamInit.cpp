#include "../Headers/Player.h"
#include "../Headers/SceneBattle.h"
#include "../Headers/UIMenu.h"
#include "../GameLib/Math/Math.h"
#include "../Headers/Common.h"

//*********************************************************
//
//				初期化、データ読み込み
//
//*********************************************************

//モーション一括追加
void Player::AddMotionAll()
{

	//キャラクターモデル読み込み
	
	switch (this->chNo) {

	case CH_FEMALE:

		plMesh->Create("Assets/chara00/ruria_karimodel.FBX", VECTOR4(1, 1, 1.f, 1.f));

		//ENDが見つかるまで検索してモーション追加
		for (int i = 0; motionDataChara00[i].motionName != PLAYER_MOTION::END; i++)
		{
			plMesh->AddMotion(motionDataChara00[i].motionName, motionDataChara00[i].fileName);
		}
		break;

	case CH_MALE:
		
		//plMesh->Create("Assets/chara01/spica_June_8th.FBX", VECTOR4(1, 1, 1.f, 1.f));

		////ENDが見つかるまで検索してモーション追加
		//for (int i = 0; motionDataChara01[i].motionName != PLAYER_MOTION::END; i++)
		//{
		//	plMesh->AddMotion(motionDataChara01[i].motionName, motionDataChara01[i].fileName);
		//}

		//break;
		plMesh->Create("Assets/chara00/ruria_karimodel.FBX", VECTOR4(1, 1, 1.f, 1.f));

		//ENDが見つかるまで検索してモーション追加
		for (int i = 0; motionDataChara00[i].motionName != PLAYER_MOTION::END; i++)
		{
			plMesh->AddMotion(motionDataChara00[i].motionName, motionDataChara00[i].fileName);
		}
		break;
	}
}

//パラメータ初期化
void Player::InitParameter(int pad)
{
	isAir = false;
	isActive = true;
	isHit = false;
	isDamage = false;
	isPush = false;
	isPushed = false;
	isRectRender = false;
	isWall = false;
	isNotReturn = false;
	isThrowed = false;
	isThrow = false;
	isInvisible = false;
	isDown = false;
	isNotPush = false;
	isCancelStandBy = false;
	isDark = false;
	isLow = false;
	isPowerUp = false;
	airMoveCnt = 1;
	moveState = 0;
	subState = 0;
	speed.x = 0;
	speed.y = 0;
	moveCnt = 0;
	stopFrame = 0;
	this->pad = pad;
	rigidFrame = 0;
	frameCnt = 0;
	hp = HP_MAX;
	artsPoint =  ARTS_MAX*0.25f;
	attackLast = 0;
	knockbackSpeed = {};

	//1Pか2Pかで分岐
	switch (pad)
	{
	case PLAYER_ONE:
		
		position = VECTOR2(INITPOS, GROUND);
		dir = DIR_RIGHT;
		plMesh->rotation = INIT_ROTATION_1P;
		plMesh->scale = INIT_SCALE_1P;
		color = plMesh->vertexColor;
		isPrioritize = true;
		break;

	case PLAYER_TWO:

		position = VECTOR2((float)DxSystem::SCREEN_WIDTH - INITPOS, GROUND);
		dir = DIR_LEFT;
		plMesh->rotation = INIT_ROTATION_2P;
		plMesh->scale = INIT_SCALE_2P;
		color = plMesh->vertexColor;
		isPrioritize = false;
		break;
	}



	FILE*fp = nullptr;


	//キャラクターモデル読み込み
	switch (this->chNo) {

	case CH_FEMALE:
		fopen_s(&fp, "Assets/chara00/ruria_rect.csv", "r");//ファイルオープン
		break;
	case CH_MALE:
		fopen_s(&fp, "Assets/chara00/ruria_rect.csv", "r");//ファイルオープン
		break;
	
	}


	if (fp == NULL)return;


	//矩形データをCSVから読み込んで配列に保持
	for (int i = 0; !feof(fp); i++)
	{
		fscanf_s(fp, "%d,%f,%f,%f,%f,%d,%d,%d\n", &rectData[i].motionName,
			&rectData[i].position.x, &rectData[i].position.y,
			&rectData[i].size.x, &rectData[i].size.y,
			&rectData[i].type, &rectData[i].start, &rectData[i].keep);

		//矩形の色設定
		switch (rectData[i].type)
		{
		case TYPE_HURT:
			rectData[i].color = HURT_RECT_COLOR;
			break;
		case TYPE_HIT:
			rectData[i].color = HIT_RECT_COLOR;
			break;
		}
	}

	fclose(fp);//ファイル閉じる




	//キャラクターモデル読み込み
	switch (this->chNo) {

	case CH_FEMALE:
		fopen_s(&fp, "Assets/chara00/ruria_param.csv", "r");//ファイルオープン
		break;
	case CH_MALE:
		fopen_s(&fp, "Assets/chara00/ruria_param.csv", "r");//ファイルオープン
		break;

	}


	if (fp == NULL)return;


	//矩形データをCSVから読み込んで配列に保持
	for (int i = 0; !feof(fp); i++)
	{
		fscanf_s(fp,"%f\n",&commonParam[i]);
	}

	fclose(fp);//ファイル閉じる



	//押し合い判定
	pushRectData.position = VECTOR2(0, 0);
	pushRectData.size = VECTOR2(300, 300);
	pushRectData.color = VECTOR4(1, 1, 0, 0.4f);
	//行列更新
	plMesh->Update(true);
	plMesh->Play(PLAYER_MOTION::NEUTRAL);
	comboCnt = 0;


}

