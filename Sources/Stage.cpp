#include "../Headers/Stage.h"
#include "../Headers/SceneBattle.h"
#include "../GameLib/Headers/GameCamera.h"
#include "../GameLib/Math/Math.h"







void Stage::Init()
{

	tex = new Texture();
	tex->Load("Assets/stage/Grid_04.png");
	mesh = new FBXMesh();
	mesh->Create("Assets/stage/TrainingStage.fbx", VECTOR4(1, 1, 1, 1));
	mesh->position = VECTOR3((float)DxSystem::SCREEN_WIDTH / 2.f, (float)DxSystem::SCREEN_HEIGHT / 2.f, -0.4f);
	stgShader = new Shader();
	stgShader->Create(L"Shaders/Stage.fx", "VSStage", "PSStage");
	mesh->scale = VECTOR3(0.05, 0.05, 0.05);
	mesh->rotation = VECTOR3(0, 0, 0);
	mesh->Update(true);

	isShake = false;
}

void Stage::Reset()
{
	mesh->position = VECTOR3((float)DxSystem::SCREEN_WIDTH / 2.f, (float)DxSystem::SCREEN_HEIGHT / 2.f, -0.4f);
	mesh->scale = VECTOR3(0.05, 0.05, 0.05);
	mesh->rotation = VECTOR3(0, 0, 0);
	mesh->Update(true);
}


void Stage::SetShake(float rate)
{
	isShake = true;
	shakeRate = rate;
}

void Stage::Update()
{

	/*if (GetAsyncKeyState('W'))
		mesh->position.y -= 10.f;


	if (GetAsyncKeyState('S'))
		mesh->position.y += 10.f;

	if (GetAsyncKeyState('A'))
		mesh->position.z -= 0.1f;


	if (GetAsyncKeyState('D'))
		mesh->position.z += 0.1f;*/



	mesh->Update(true);
}

void Stage::Render(ID3D11Buffer * constantBuffer)
{
	tex->Set(0);
	stgShader->Activate();


	SceneBattle::ConstantBufferParam cb;
	//定数バッファにセット
	cb.view = gameCamera->GetViewMATRIX();
	cb.proj = gameCamera->GetProjectionMATRIXBlend();


	//WVP変換->描画
	cb.world = mesh->transform;
	cb.wvp = mesh->transform * cb.view * cb.proj;
	//頂点情報セット
	DxSystem::deviceContext->VSSetConstantBuffers(0, 1, &constantBuffer);
	//定数バッファ更新
	DxSystem::deviceContext->UpdateSubresource(constantBuffer, 0, NULL, &cb, 0, 0);

	mesh->Render();
}

Stage::~Stage()
{
	delete tex;
	delete mesh;
	delete stgShader;
}
