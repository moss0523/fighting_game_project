#include "../Headers/Player.h"
#include "../Headers/SceneBattle.h"

//*********************************************************
//
//				CPU関連
//
//*********************************************************

//*********************************************************
//				反応確率(数字=確率
//*********************************************************
namespace PERCENT {
	//何もしてないとき
	static const int NEAR_NOT_MOVE00 = 20;
	static const int NEAR_NOT_MOVE01 = 40 + NEAR_NOT_MOVE00;

	static const int MID_NOT_MOVE00 = 60;
	static const int MID_NOT_MOVE01 = 30 + MID_NOT_MOVE00;

	static const int FAR_NOT_MOVE00 = 40;
	static const int FAR_NOT_MOVE01 = 30 +FAR_NOT_MOVE00;

	//ジャンプしてるとき
	static const int NEAR_JUMP00 = 40;
	static const int NEAR_JUMP01 = 40 + NEAR_JUMP00;

	static const int MID_JUMP00 = 20;
	static const int MID_JUMP01 = 50 + MID_JUMP00;

	static const int FAR_JUMP00 = 20;
	static const int FAR_JUMP01 = 50 + FAR_JUMP00;

	//攻撃してるとき
	static const int NEAR_ATTACK00 = 50;

	static const int MID_ATTACK00 = 30;
	static const int MID_ATTACK01 = 40 + MID_ATTACK00;

	static const int FAR_ATTACK00 = 30;
	static const int FAR_ATTACK01 = 40 + FAR_ATTACK00;

	//ガードしてるとき
	static const int GUARD00 = 30;
	static const int GUARD01 = 60 + GUARD00;
	//ダメージ喰らってるとき
	static const int DAMAGE00 = 40;
};



//どの行動をとるか判断する
//percePercentと設定した確率値で行動
void Player::CPUPerce(int& keyTrg, int& keyState)
{

	//プレイヤーとの距離
	float distance = (enemy->position - position).Length();

	using namespace Input;



	//乱数
	int percePercent = rand() % 100;


	//プレイヤーが今何してるか、
	//プレイヤーとの距離によって変化

	switch (enemy->playerState)
	{

		//プレイヤーが何もしてない
	case PLAYER_STATE::NOT_MOVE:


		//近いとき
		if (distance <= NEAR_DISTANCE)
		{

			if (percePercent < PERCENT::NEAR_NOT_MOVE00)
				keyTrg = PAD_TRG1;
			else if (percePercent < PERCENT::NEAR_NOT_MOVE01)
				keyTrg = PAD_TRG2;
			else
				keyTrg = PAD_TRG3;

		}
		//中距離
		else if (distance <= FAR_DISTANCE)
		{
			if (percePercent < PERCENT::MID_NOT_MOVE00)
				keyState = PAD_RIGHT;
			else if (percePercent < PERCENT::MID_NOT_MOVE01)
				keyTrg = PAD_TRG2;
			else
				keyTrg = PAD_UP | PAD_RIGHT;
		}
		//離れている
		else {
			if (percePercent < PERCENT::FAR_NOT_MOVE00)
				keyState = PAD_RIGHT;
			
			else
				keyTrg = PAD_UP | PAD_RIGHT;
		}

		break;
		//プレイヤーがジャンプ
	case PLAYER_STATE::JUMP:

		//近いとき
		if (distance <= NEAR_DISTANCE)
		{
			if (percePercent < PERCENT::NEAR_JUMP00)
				keyState = PAD_LEFT;
			
			else
				keyTrg = PAD_TRG2;


		}
		//中距離
		else if (distance <= FAR_DISTANCE)
		{

			if (percePercent < PERCENT::MID_JUMP00)
				keyTrg = PAD_TRG2;
			else if (percePercent < PERCENT::MID_JUMP01)
				keyState = PAD_RIGHT;
			else {
				keyTrg = PAD_TRG3;
				keyState = PAD_DOWN;

			}
		}
		//離れている
		else {
			if (percePercent < PERCENT::FAR_JUMP00)
				keyTrg = PAD_TRG2;
			else if (percePercent < PERCENT::FAR_JUMP01)
				keyState = PAD_RIGHT;

		}
		break;

		//プレイヤーが攻撃している
	case PLAYER_STATE::ATTACK:

		//近いとき
		if (distance <= NEAR_DISTANCE)
		{
			if (percePercent < PERCENT::NEAR_ATTACK00)
				keyState = PAD_LEFT;
			else
				keyTrg = PAD_TRG1;

		}
		//中距離
		else if (distance <= FAR_DISTANCE)
		{
			if (percePercent < PERCENT::MID_ATTACK00)
				keyTrg = PAD_TRG2;
			else if (percePercent < PERCENT::MID_ATTACK01)
				keyTrg = PAD_LEFT;
			else {
				keyTrg = PAD_TRG2;
				keyState = PAD_DOWN;
			}
		}
		//離れている
		else {

			if (percePercent < PERCENT::FAR_ATTACK00)
				keyTrg = PAD_TRG2;
			
			else {
				keyTrg = PAD_TRG2;
				keyState = PAD_DOWN;
			}
		}

		keyState = PAD_LEFT;

		break;

		//プレイヤーがダメージを受けている
	case PLAYER_STATE::DAMAGE:

		if (percePercent < PERCENT::DAMAGE00)
			keyTrg = PAD_TRG2;
		else {
			keyTrg = PAD_TRG1;

		}
		break;

		//プレイヤーがガード中
	case PLAYER_STATE::GUARD:

		if (percePercent < PERCENT::GUARD00)
			keyTrg = PAD_TRG1;
		else 
			keyTrg = PAD_TRG3;
		
		break;


	}
}

void Player::CPUInput(int& keyTrg, int& keyState)
{
	if (!sceneBattle->isInput)return;

	using namespace Input;
	//状態遷移変数
	static int state = 0;
	//次の行動までのインターバル
	static int interval = 0;
	//最後に入力されたキーステート
	static int oldKeyState = 0;


	switch (state)
	{
		//CPUキー入力
	case CPU_INPUT:


		//知覚し、行動を決定
		CPUPerce(keyTrg, keyState);

		oldKeyState = keyState;//キーステート保存
		state++;

		break;


	case CPU_NOW_MOVE:

		//キーステート固定
		keyState = oldKeyState;

		//インターバルが一定値を超えると
		//ステート移動(また行動を選び直す
		if (++interval > INTERVAL_TIME)
		{
			state--;
			interval = 0;
		}

		break;

	}


}
