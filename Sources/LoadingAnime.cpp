#include "../Headers/LoadingAnime.h"
#include "../Headers/SpriteData.h"
#include "../Headers/Common.h"
#include <string>
#include <bitset>


static const VECTOR2 LOADING_SCALE = VECTOR2(2.f, 2.f);


void LoadUI::Move::LoadingMove(Obj2D*obj)
{
	switch (obj->state) {

	case MOVE_STATE::INIT:
		
		obj->scale = LOADING_SCALE;
	
		obj->state++;
		//break; 意図的
	case MOVE_STATE::MAIN:
		
		//アニメーション設定
		AnimeData*anime = nullptr;

		anime = AnimeUILoading;

		obj->AnimeUpdate(anime);
		break;
	}

}



void LoadUI::Init()
{
	Obj2DManager::Init();   // Obj2DManagerの初期化
	this->Create(LoadUI::Move::LoadingMove, SCREEN_CENTER);
}

