

#include "../Headers/Edit.h"
#include "../Headers/SceneBattle.h"
#include "../GameLib/Headers/RectRender.h"
#include "../Headers/SpriteData.h"
#include "../GameLib/Headers/ImGui.h"
#include "../Headers/Common.h"

//******************************************************************
//
//			定数
//
//*****************************************************************
static const VECTOR2 HIT_BUTTON_POS = //攻撃判定生成ボタンの位置
VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 - 256, 230.f);

static const VECTOR2 HURT_BUTTON_POS =//ダメージ判定生成ボタンの位置
VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 + 256, 230.f);

//当たり判定の大きさ
static const float MOUSE_SIZE = 5.f;//マウス
static const VECTOR2 BUTTON_SIZE = VECTOR2(128.f, 64.f);//ボタン
static const VECTOR2 RECT_SIZE = VECTOR2(25.f, 25.f);//矩形(初期サイズ
static const VECTOR2 POS_CONTROLER_SIZE = VECTOR2(16.f, 16.f);//コントローラー中心
static const VECTOR2 CONTROLER_X_SIZE = VECTOR2(48.f, 16.f);//コントローラー横軸
static const VECTOR2 CONTROLER_Y_SIZE = VECTOR2(16.f, 48.f);//コントローラー縦軸

//パラメータ管理ウインドウの位置と大きさ
static const VECTOR2 IMGUI_WINDOW_SIZE_PARAM = VECTOR2(400.f, 300.f);
static const VECTOR2 IMGUI_WINDOW_POS_PARAM = VECTOR2(100.f, 100.f);

//パラメータ管理ウインドウの位置と大きさ
static const VECTOR2 IMGUI_WINDOW_SIZE_RECT = VECTOR2(300, 900);
static const VECTOR2 IMGUI_WINDOW_POS_RECT = VECTOR2(1500.f, 30.f);

static const float IMGUI_FONT_SCALE = 2.5f;//ImGuiフォントサイズ

static const float CONTROLER_OFFSET = 64.f;//コントローラー位置調整値

//初期化
void Edit::Init()
{
	isMove = false;
	Obj2DManager::Init();   //Obj2DManagerの初期化
	//UI生成(ボタン)----------
	Create(Edit::Move::HitButton, HIT_BUTTON_POS);
	Create(Edit::Move::HurtButton, HURT_BUTTON_POS);

}

//IMGUIを用いたパラメータ調整ツールの生成と描画
void Edit::ImGuiToolUpdate() {

	//当たり判定調整、セーブ
	RectToolMove();

	//パラメータ調整、セーブ
	ParamToolMove();

	//IMGUI終了
	ImGui::End();


}

//パラメータ管理
void Edit::ParamToolMove()
{
	ImGui::SetNextWindowSize(ImVec2(IMGUI_WINDOW_SIZE_PARAM.x, IMGUI_WINDOW_SIZE_PARAM.y), ImGuiSetCond_FirstUseEver);
	ImGui::SetNextWindowPos(ImVec2(IMGUI_WINDOW_POS_PARAM.x, IMGUI_WINDOW_POS_PARAM.y));
	ImGui::Begin("CommonParam", false);
	ImGui::SetWindowFontScale(IMGUI_FONT_SCALE);

	//パラメータ調整用ウインドウ
	ParamToolWindow();
	//セーブボタンでパラメータ保存
	if (ImGui::Button("save")) {

		FILE*fp = nullptr;


		//キャラクター矩形データ読み込み
		switch (sceneBattle->player1->GetChNo()) {

		case CH_FEMALE:
			fopen_s(&fp, "Assets/chara00/ruria_param.csv", "w");//ファイルオープン
			break;
		case CH_MALE:
			fopen_s(&fp, "Assets/chara00/ruria_param.csv", "w");//ファイルオープン
			break;

		}
		if (fp == NULL)return;

		//パラメータセーブ
		for (int i = 0; i<PLAYER_PARAM::NUM; i++) {
			fprintf_s(fp, "%f\n", sceneBattle->player1->commonParam[i]);
		}

		fclose(fp);
	}



	ImGui::End();
}
//パラメータ調整用ウインドウ
void Edit::ParamToolWindow()
{
	ImGui::DragFloat("WALK_SPEED		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::WALK_SPEED]);
	ImGui::DragFloat("RUN_SPEED			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::RUN_SPEED]);
	ImGui::DragFloat("JUMP_SPEED		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::JUMP_SPEED]);
	ImGui::DragFloat("BUCK_STEP_SPEEDX	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::BUCK_STEP_SPEEDX]);
	ImGui::DragFloat("BUCK_STEP_SPEEDY	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::BUCK_STEP_SPEEDY]);
	ImGui::DragFloat("HITSTOP_FRAME_NA	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::HITSTOP_FRAME_NA]);
	ImGui::DragFloat("HITSTOP_FRAME_NB	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::HITSTOP_FRAME_NB]);
	ImGui::DragFloat("HITSTOP_FRAME_NC	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::HITSTOP_FRAME_NC]);
	ImGui::DragFloat("HITSTOP_FRAME_JA	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::HITSTOP_FRAME_JA]);
	ImGui::DragFloat("HITSTOP_FRAME_JB	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::HITSTOP_FRAME_JB]);
	ImGui::DragFloat("HITSTOP_FRAME_JC	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::HITSTOP_FRAME_JC]);
	ImGui::DragFloat("HITSTOP_FRAME_LA	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::HITSTOP_FRAME_LA]);
	ImGui::DragFloat("HITSTOP_FRAME_LB	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::HITSTOP_FRAME_LB]);
	ImGui::DragFloat("HITSTOP_FRAME_LC	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::HITSTOP_FRAME_LC]);
	ImGui::DragFloat("HITSTOP_FRAME_THROW  ", &sceneBattle->player1->commonParam[PLAYER_PARAM::HITSTOP_FRAME_THROW]);
	ImGui::DragFloat("HITSTOP_FRAME_ARTS00 ", &sceneBattle->player1->commonParam[PLAYER_PARAM::HITSTOP_FRAME_ARTS00]);
	ImGui::DragFloat("HITSTOP_FRAME_ARTS01 ", &sceneBattle->player1->commonParam[PLAYER_PARAM::HITSTOP_FRAME_ARTS01]);
	ImGui::DragFloat("HITSTOP_FRAME_ARTS02 ", &sceneBattle->player1->commonParam[PLAYER_PARAM::HITSTOP_FRAME_ARTS02]);
	ImGui::DragFloat("DMG_FRAME_MAX		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_MAX]);
	ImGui::DragFloat("DMG_FRAME_NA		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_NA]);
	ImGui::DragFloat("DMG_FRAME_NB		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_NB]);
	ImGui::DragFloat("DMG_FRAME_NC		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_NC]);
	ImGui::DragFloat("DMG_FRAME_JA		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_JA]);
	ImGui::DragFloat("DMG_FRAME_JB		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_JB]);
	ImGui::DragFloat("DMG_FRAME_JC		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_JC]);
	ImGui::DragFloat("DMG_FRAME_LA		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_LA]);
	ImGui::DragFloat("DMG_FRAME_LB		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_LB]);
	ImGui::DragFloat("DMG_FRAME_LC		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_LC]);
	ImGui::DragFloat("DMG_FRAME_ARTS00	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_ARTS00]);
	ImGui::DragFloat("DMG_FRAME_ARTS01	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_ARTS01]);
	ImGui::DragFloat("DMG_FRAME_ARTS02	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_ARTS02]);
	ImGui::DragFloat("DMG_FRAME_AIR_MAX	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_AIR_MAX]);
	ImGui::DragFloat("DMG_FRAME_AIR_NA	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_AIR_NA]);
	ImGui::DragFloat("DMG_FRAME_AIR_NB	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_AIR_NB]);
	ImGui::DragFloat("DMG_FRAME_AIR_NC	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_AIR_NC]);
	ImGui::DragFloat("DMG_FRAME_AIR_JA	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_AIR_JA]);
	ImGui::DragFloat("DMG_FRAME_AIR_JB	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_AIR_JB]);
	ImGui::DragFloat("DMG_FRAME_AIR_JC	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_AIR_JC]);
	ImGui::DragFloat("DMG_FRAME_AIR_LA	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_AIR_LA]);
	ImGui::DragFloat("DMG_FRAME_AIR_LB	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_AIR_LB]);
	ImGui::DragFloat("DMG_FRAME_AIR_LC	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_AIR_LC]);
	ImGui::DragFloat("DMG_FRAME_SHOT	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::DMG_FRAME_SHOT]);
	ImGui::DragFloat("RECOVER_SPEEDX	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::RECOVER_SPEEDX]);
	ImGui::DragFloat("GRD_FRAME_NA		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::GRD_FRAME_NA]);
	ImGui::DragFloat("GRD_FRAME_NB		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::GRD_FRAME_NB]);
	ImGui::DragFloat("GRD_FRAME_NC		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::GRD_FRAME_NC]);
	ImGui::DragFloat("GRD_FRAME_JA		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::GRD_FRAME_JA]);
	ImGui::DragFloat("GRD_FRAME_JB		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::GRD_FRAME_JB]);
	ImGui::DragFloat("GRD_FRAME_JC		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::GRD_FRAME_JC]);
	ImGui::DragFloat("GRD_FRAME_LA		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::GRD_FRAME_LA]);
	ImGui::DragFloat("GRD_FRAME_LB		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::GRD_FRAME_LB]);
	ImGui::DragFloat("GRD_FRAME_LC		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::GRD_FRAME_LC]);
	ImGui::DragFloat("GRD_FRAME_ARTS00	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::GRD_FRAME_ARTS00]);
	ImGui::DragFloat("GRD_FRAME_ARTS01	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::GRD_FRAME_ARTS01]);
	ImGui::DragFloat("GRD_FRAME_ARTS02	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::GRD_FRAME_ARTS02]);
	ImGui::DragFloat("GRD_FRAME_SHOT	   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::GRD_FRAME_SHOT]);
	ImGui::DragFloat("ATTACK_NA			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::ATTACK_NA]);
	ImGui::DragFloat("ATTACK_NB			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::ATTACK_NB]);
	ImGui::DragFloat("ATTACK_NC			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::ATTACK_NC]);
	ImGui::DragFloat("ATTACK_JA			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::ATTACK_JA]);
	ImGui::DragFloat("ATTACK_JB			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::ATTACK_JB]);
	ImGui::DragFloat("ATTACK_JC			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::ATTACK_JC]);
	ImGui::DragFloat("ATTACK_LA			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::ATTACK_LA]);
	ImGui::DragFloat("ATTACK_LB			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::ATTACK_LB]);
	ImGui::DragFloat("ATTACK_LC			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::ATTACK_LC]);
	ImGui::DragFloat("ATTACK_ARTS00		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::ATTACK_ARTS00]);
	ImGui::DragFloat("ATTACK_ARTS01		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::ATTACK_ARTS01]);
	ImGui::DragFloat("ATTACK_ARTS02		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::ATTACK_ARTS02]);
	ImGui::DragFloat("ATTACK_SHOT		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::ATTACK_SHOT]);
	ImGui::DragFloat("FORCE_NA			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::FORCE_NA]);
	ImGui::DragFloat("FORCE_NB			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::FORCE_NB]);
	ImGui::DragFloat("FORCE_NC			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::FORCE_NC]);
	ImGui::DragFloat("FORCE_LA			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::FORCE_LA]);
	ImGui::DragFloat("FORCE_LB			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::FORCE_LB]);
	ImGui::DragFloat("FORCE_LC			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::FORCE_LC]);
	ImGui::DragFloat("FORCE_JA			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::FORCE_JA]);
	ImGui::DragFloat("FORCE_JB			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::FORCE_JB]);
	ImGui::DragFloat("FORCE_JC			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::FORCE_JC]);
	ImGui::DragFloat("FORCE_ARTS00X		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::FORCE_ARTS00X]);
	ImGui::DragFloat("FORCE_ARTS01X		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::FORCE_ARTS01X]);
	ImGui::DragFloat("FORCE_ARTS00Y		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::FORCE_ARTS00Y]);
	ImGui::DragFloat("FORCE_ARTS01Y		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::FORCE_ARTS01Y]);
	ImGui::DragFloat("KOCKBACK_NA		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::KOCKBACK_NA]);
	ImGui::DragFloat("KOCKBACK_NB		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::KOCKBACK_NB]);
	ImGui::DragFloat("KOCKBACK_NC		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::KOCKBACK_NC]);
	ImGui::DragFloat("KOCKBACK_LA		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::KOCKBACK_LA]);
	ImGui::DragFloat("KOCKBACK_LB		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::KOCKBACK_LB]);
	ImGui::DragFloat("KOCKBACK_LC		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::KOCKBACK_LC]);
	ImGui::DragFloat("KOCKBACK_JA		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::KOCKBACK_JA]);
	ImGui::DragFloat("KOCKBACK_JB		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::KOCKBACK_JB]);
	ImGui::DragFloat("KOCKBACK_JC		   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::KOCKBACK_JC]);
	ImGui::DragFloat("RIGID_NA			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::RIGID_NA]);
	ImGui::DragFloat("RIGID_NB			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::RIGID_NB]);
	ImGui::DragFloat("RIGID_NC			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::RIGID_NC]);
	ImGui::DragFloat("RIGID_JA			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::RIGID_JA]);
	ImGui::DragFloat("RIGID_JB			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::RIGID_JB]);
	ImGui::DragFloat("RIGID_JC			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::RIGID_JC]);
	ImGui::DragFloat("RIGID_LA			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::RIGID_LA]);
	ImGui::DragFloat("RIGID_LB			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::RIGID_LB]);
	ImGui::DragFloat("RIGID_LC			   ", &sceneBattle->player1->commonParam[PLAYER_PARAM::RIGID_LC]);


}

//当たり判定調整用ウインドウ
void Edit::RectToolMove()
{

	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
	ImGui::SetNextWindowSize(ImVec2(IMGUI_WINDOW_SIZE_RECT.x, IMGUI_WINDOW_SIZE_RECT.y));
	ImGui::SetNextWindowPos(ImVec2(IMGUI_WINDOW_POS_RECT.x, IMGUI_WINDOW_POS_RECT.y));
	ImGui::Begin("RectParam", false);
	ImGui::SetWindowFontScale(IMGUI_FONT_SCALE);



	for (auto &p : *edit) {

		//何も設定されていないものと矩形以外は無視
		if (p.size.x <= 0)continue;
		if (p.type != EDIT_TYPE_RECT)continue;


		//矩形データ表示
		ImGui::Text("rect[%d]", p.index);
		ImGui::DragFloat("posX", &(p.position.x));
		ImGui::DragFloat("posY", &(p.position.y));
		ImGui::DragFloat("sizeX", &(p.size.x));
		ImGui::DragFloat("sizeY", &(p.size.y));
		ImGui::InputInt("start", &(p.startFrame));
		ImGui::InputInt("keep", &(p.keepFrame));

	}


	ImGui::SetWindowFontScale(IMGUI_FONT_SCALE);

	{
		//モーションの全体フレーム表示
		int f = (int)sceneBattle->player1->GetMesh()->GetMotionFrame(sceneBattle->player1->GetMesh()->motionName);
		ImGui::InputInt("AllFrame", &f);
	}
	{
		//モーションの現在フレーム表示
		int f = (int)sceneBattle->player1->GetFrameCnt();
		ImGui::InputInt("NowFrame", &f);
	}


	if (ImGui::Button("save"))
		SaveAdd();
}


void Edit::Update()
{

	Obj2DManager::Update();//矩形、UIの更新

	//IMGUI関連の更新
	ImGuiToolUpdate();

}

void Edit::Render()
{
	Obj2DManager::Render();
	//スプライトあり
	//無し
	for (auto &item : *edit)
	{
		if (item.type == EDIT_TYPE_RECT)
		{
			rectRender->Render(item.position, item.size * 2, item.color);
		}
	}

	ImGui::Render();
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}

void Edit::SaveAdd()
{

	VECTOR2 plPos = sceneBattle->player1->GetPosition();

	FILE*fp = nullptr;


	//キャラクター矩形データ読み込み
	switch (sceneBattle->player1->GetChNo()) {

	case CH_FEMALE:
		fopen_s(&fp, "Assets/chara00/ruria_rect.csv", "a");//ファイルオープン
		break;
	case CH_MALE:
		fopen_s(&fp, "Assets/chara00/ruria_rect.csv", "a");//ファイルオープン
		break;

	}
	if (fp == NULL)return;




	//全部セーブ TODO:開始フレームなどの情報を追加する
	for (auto &item : *edit)
	{
		//矩形じゃなければ無視
		if (item.type != EDIT_TYPE_RECT)continue;

		//矩形の種類によって分ける
		if (item.mover == Edit::Move::HurtRectNew)
			fprintf_s(fp, "%d,%.0f,%.0f,%.0f,%.0f,%d,%d,%d\n",
				item.index,
				item.position.x - plPos.x,
				item.position.y - plPos.y,
				item.size.x*2.f,
				item.size.y*2.f,
				0,
				item.startFrame,
				item.keepFrame
			);//書き込み

		else
			fprintf_s(fp, "%d,%.0f,%.0f,%.0f,%.0f,%d,%d,%d\n",
				item.index,
				item.position.x - plPos.x,
				item.position.y - plPos.y,
				item.size.x * 2.f,
				item.size.y * 2.f,
				1,
				item.startFrame,
				item.keepFrame
			);//書き込み



		item.Clear();
	}
	//再初期化処理
	fclose(fp);//ファイル閉じる

	sceneBattle->ReSet();
	sceneBattle->stage->Reset();
	gameCamera->Update();
}





//マウスとの当たり判定
bool MouseHitCheckEdit(Obj2D* other)
{
	//マウスの座標取得
	VECTOR2 MousePos = VECTOR2((float)InputManager::GetInstance()->GetCursorPosX(),
		(float)InputManager::GetInstance()->GetCursorPosY());

	float MouseSize = MOUSE_SIZE;


	//上下左右を設定(マウス
	float thisLeft = MousePos.x - MouseSize;
	float thisRight = MousePos.x + MouseSize;
	float thisTop = MousePos.y - MouseSize;
	float thisBottom = MousePos.y + MouseSize;

	//上下左右を設定(他のオブジェクト
	float otherLeft = other->position.x - other->size.x;
	float otherRight = other->position.x + other->size.x;
	float otherTop = other->position.y - other->size.y;
	float otherBottom = other->position.y + other->size.y;

	//当たってなければ早期リターン
	if (thisRight < otherLeft)return false;
	if (otherRight < thisLeft)return false;
	if (thisBottom < otherTop)return false;
	if (otherBottom < thisTop)return false;

	return true;

}







//*******************************************************
//移動関数
//*******************************************************

//攻撃判定生成ボタン
void Edit::Move::HitButton(Obj2D*obj) {


	//キー入力取得
	int keyState = inputManager->GetPadAddress()[0].state;//キーステート
	int keyTrg = inputManager->GetPadAddress()[0].trigger;//キートリガー

	switch (obj->state)
	{
		//初期設定
	case MOVE_STATE::INIT:
		obj->data = &sprBottunHit;
		obj->scale = ORIGINAL_SCALE;
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		obj->size = BUTTON_SIZE;
		obj->type = EDIT_TYPE_UI;

		obj->state++;
		//break;

		//本処理
	case MOVE_STATE::MAIN:

		if (MouseHitCheckEdit(obj)) {
			obj->color = RED_COLOR;


			if (keyTrg&Input::LEFT_CLICK)
			{
				//矩形生成
				Obj2D *child = edit->Create(Edit::Move::HitRectNew, SCREEN_CENTER);
				child->parent2D = obj;
				child->index = sceneBattle->player1->GetMotionName();
			}

		}
		else
			obj->color = ORIGINAL_COLOR;

		break;
	}


}

//ダメージ判定生成ボタン
void Edit::Move::HurtButton(Obj2D*obj) {

	//キー入力取得
	int keyState = inputManager->GetPadAddress()[0].state;//キーステート
	int keyTrg = inputManager->GetPadAddress()[0].trigger;//キートリガー



	switch (obj->state)
	{
		//初期設定
	case MOVE_STATE::INIT:

		obj->data = &sprBottunHurt;
		obj->scale = ORIGINAL_SCALE;
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		obj->size = BUTTON_SIZE;
		obj->type = EDIT_TYPE_UI;




		obj->state++;
		//break;　意図的

		//本処理
	case MOVE_STATE::MAIN:


		if (MouseHitCheckEdit(obj)) {
			obj->color = GREEN_COLOR;
			if (keyTrg&Input::LEFT_CLICK)
			{
				Obj2D *child = edit->Create(Edit::Move::HurtRectNew, SCREEN_CENTER);
				child->parent2D = obj;
				child->index = sceneBattle->player1->GetMotionName();

			}
		}
		else
			obj->color = ORIGINAL_COLOR;


		break;
	}

}


//コントローラーをすべて削除
void Edit::ControlerAllClear() {

	//すべて検索(参照型
	for (auto &p : *edit)
	{
		p.isSelect = false;

		if (p.type == EDIT_TYPE_CONTROL) {
			p.Clear();
		}

	}


}

//位置調整コントローラーをセットする
void Edit::SetPosControler(Obj2D*obj) {

	//新しいコントローラーが生成されたら他は削除します
	ControlerAllClear();

	Obj2D *child = edit->Create(Edit::Move::PosControler, obj->position);
	child->parent2D = obj;

	Obj2D *child2 = edit->Create(Edit::Move::PosControlerX, VECTOR2(obj->position.x + 64, obj->position.y));
	child2->parent2D = obj;

	Obj2D *child3 = edit->Create(Edit::Move::PosControlerY, VECTOR2(obj->position.x, obj->position.y - 64));
	child3->parent2D = obj;

	//選択済みフラグオン
	obj->isSelect = true;

}

//大きさ調整コントローラーをセットする
void Edit::SetSizeControler(Obj2D*obj) {

	//新しいコントローラーが生成されたら他は削除します
	ControlerAllClear();

	Obj2D *child = edit->Create(Edit::Move::SizeControler, obj->position);
	child->parent2D = obj;

	Obj2D *child2 = edit->Create(Edit::Move::SizeControlerW, VECTOR2(obj->position.x + 64, obj->position.y));
	child2->parent2D = obj;

	Obj2D *child3 = edit->Create(Edit::Move::SizeControlerH, VECTOR2(obj->position.x, obj->position.y - 64));
	child3->parent2D = obj;

	//選択済みフラグオン
	obj->isSelect = true;

}


//攻撃判定矩形処理
void  Edit::Move::HitRectNew(Obj2D*obj) {


	int keyTrg = inputManager->GetPadAddress()[0].trigger;//キートリガー

	switch (obj->state)
	{
		//初期設定
	case MOVE_STATE::INIT:

		obj->color = HIT_RECT_COLOR;
		obj->type = EDIT_TYPE_RECT;
		obj->size = RECT_SIZE;

		obj->state++;
		//break;　意図的

	//本処理
	case MOVE_STATE::MAIN:

		if (obj->isSelect)
		{
			//Wキーで位置調整モード
			//Rキーで大きさ調整モード
			//Deleteキーで削除
			if (keyTrg&Input::KEY_W)
				edit->SetPosControler(obj);
			if (keyTrg&Input::KEY_R)
				edit->SetSizeControler(obj);
			if (keyTrg&Input::KEY_DELETE) {
				edit->ControlerAllClear();
				obj->Clear();
			}

			break;
		}
		else {
			//矩形クリックされるとその位置にコントローラー生成
			if (MouseHitCheckEdit(obj) && keyTrg&Input::LEFT_CLICK)
				edit->SetPosControler(obj);
		}
		break;
	}
}

//ダメージ判定矩形処理
void  Edit::Move::HurtRectNew(Obj2D*obj) {


	int keyTrg = inputManager->GetPadAddress()[0].trigger;//キートリガー

	switch (obj->state)
	{

		//初期設定
	case MOVE_STATE::INIT:
		obj->color = HURT_RECT_COLOR;
		obj->size = RECT_SIZE;

		obj->type = EDIT_TYPE_RECT;
		obj->state++;
		//break;　意図的

		//本処理
	case MOVE_STATE::MAIN:

		if (obj->isSelect)
		{
			//Wキーで位置調整モード
			//Rキーで大きさ調整モード
			//Deleteキーで削除
			if (keyTrg&Input::KEY_W)
				edit->SetPosControler(obj);
			if (keyTrg&Input::KEY_R)
				edit->SetSizeControler(obj);
			if (keyTrg&Input::KEY_DELETE) {
				edit->ControlerAllClear();
				obj->Clear();
			}
			break;
		}
		else {
			//矩形クリックされるとその位置にコントローラー生成
			if (MouseHitCheckEdit(obj) && keyTrg&Input::LEFT_CLICK)
				edit->SetPosControler(obj);
		}

		break;
	}

}




//--------------------------------------------------
//
//				コントローラー関連
//
//-----------------------------------------------

struct CONROL_STATE
{
	enum type {
		INIT,
		SELECT,
		MOVE,
	};
};

//座標コントローラー（全体サイズ変更
void Edit::Move::PosControler(Obj2D*obj) {

	int keyState = inputManager->GetPadAddress()[0].state;//キーステート

	//マウスと当たっていたらちょっと大きく
	if (MouseHitCheckEdit(obj))
		obj->scale = HALF_UP_SCALE;
	else
		obj->scale = ORIGINAL_SCALE;

	switch (obj->state)
	{
		//初期設定
	case CONROL_STATE::INIT:

		obj->data = &sprAllControler;
		obj->size = POS_CONTROLER_SIZE;
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		obj->scale = ORIGINAL_SCALE;
		obj->timer = 0;
		obj->type = EDIT_TYPE_CONTROL;

		obj->state++;
		//break;　意図的

		//選択されたら移動ステートへ
	case CONROL_STATE::SELECT:

		if (MouseHitCheckEdit(obj) && !edit->isMove)
			obj->state++;


		break;

		//移動処理
	case CONROL_STATE::MOVE:


		//移動中フラグオン
		edit->isMove = true;
		//ドラッグで座標変更
		if (keyState&Input::LEFT_CLICK)
			obj->parent2D->position += inputManager->GetDeltaPos();
		//ドラッグ外れたら選択ステートに戻る
		else {
			edit->isMove = false;
			obj->state--;
		}

		break;

	}

	//座標更新
	obj->position = obj->parent2D->position;

}
//大きさコントローラー（全体サイズ変更
void Edit::Move::SizeControler(Obj2D*obj) {


	int keyState = inputManager->GetPadAddress()[0].state;//キーステート

	//マウスと当たっていたらちょっと大きく
	if (MouseHitCheckEdit(obj))
		obj->scale = HALF_UP_SCALE;
	else
		obj->scale = ORIGINAL_SCALE;

	switch (obj->state)
	{
		//初期設定
	case CONROL_STATE::INIT:

		obj->data = &sprAllControler;
		obj->size = POS_CONTROLER_SIZE;
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		obj->scale = ORIGINAL_SCALE;
		obj->timer = 0;
		obj->type = EDIT_TYPE_CONTROL;

		obj->state++;
		//break;

		//選択されたら移動ステートへ
	case CONROL_STATE::SELECT:


		if (MouseHitCheckEdit(obj) && !edit->isMove)
			obj->state++;

		break;

		//移動処理
	case CONROL_STATE::MOVE:

		//移動中フラグオン
		edit->isMove = true;

		//ドラッグでサイズ変更
		if (keyState&Input::LEFT_CLICK)
			obj->parent2D->size += inputManager->GetDeltaPos();

		//ドラッグ外れたら選択ステートに戻る
		else {
			edit->isMove = false;

			obj->state--;
		}

		break;

	}
	//座標更新
	obj->position = obj->parent2D->position;

}

//縦軸横軸のみ編集したい時
void Edit::Move::PosControlerX(Obj2D*obj) {

	int keyState = inputManager->GetPadAddress()[0].state;//キーステート

	if (MouseHitCheckEdit(obj))
		obj->scale = HALF_UP_SCALE;
	else
		obj->scale = ORIGINAL_SCALE;

	switch (obj->state)
	{
	case CONROL_STATE::INIT:

		obj->data = &sprXControler;
		obj->size = CONTROLER_X_SIZE;
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		obj->scale = ORIGINAL_SCALE;

		obj->type = EDIT_TYPE_CONTROL;

		obj->state++;
		//break;
	case  CONROL_STATE::SELECT:
		if (MouseHitCheckEdit(obj) && !edit->isMove)
			obj->state++;


		break;

	case CONROL_STATE::MOVE:
		edit->isMove = true;

		if (keyState&Input::LEFT_CLICK)
			obj->parent2D->position.x += inputManager->GetDeltaPos().x;
		else {
			edit->isMove = false;

			obj->state--;
		}

		break;
	}

	obj->position = VECTOR2(obj->parent2D->position.x + CONTROLER_OFFSET, obj->parent2D->position.y);

}
void Edit::Move::PosControlerY(Obj2D*obj) {


	int keyState = inputManager->GetPadAddress()[0].state;//キーステート

	if (MouseHitCheckEdit(obj))
		obj->scale = HALF_UP_SCALE;
	else
		obj->scale = ORIGINAL_SCALE;

	switch (obj->state)
	{
	case CONROL_STATE::INIT:

		obj->data = &sprYControler;
		obj->size = CONTROLER_Y_SIZE;
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		obj->scale = ORIGINAL_SCALE;

		obj->type = EDIT_TYPE_CONTROL;

		obj->state++;
		//break;

	case  CONROL_STATE::SELECT:
		if (MouseHitCheckEdit(obj) && !edit->isMove)
			obj->state++;



		break;

	case CONROL_STATE::MOVE:
		edit->isMove = true;

		if (keyState&Input::LEFT_CLICK)
			obj->parent2D->position.y += inputManager->GetDeltaPos().y;
		else {
			edit->isMove = false;

			obj->state--;
		}

		break;
	}

	obj->position = VECTOR2(obj->parent2D->position.x, obj->parent2D->position.y - CONTROLER_OFFSET);

}
void Edit::Move::SizeControlerW(Obj2D*obj) {

	int keyState = inputManager->GetPadAddress()[0].state;//キーステート

	if (MouseHitCheckEdit(obj))
		obj->scale = HALF_UP_SCALE;
	else
		obj->scale = ORIGINAL_SCALE;

	switch (obj->state)
	{
	case CONROL_STATE::INIT:

		obj->data = &sprWControler;
		obj->size = CONTROLER_Y_SIZE;
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		obj->scale = ORIGINAL_SCALE;

		obj->type = EDIT_TYPE_CONTROL;

		obj->state++;
		//break;

	case  CONROL_STATE::SELECT:
		if (MouseHitCheckEdit(obj) && !edit->isMove)
			obj->state++;


		break;
	case CONROL_STATE::MOVE:
		edit->isMove = true;

		if (keyState&Input::LEFT_CLICK)
			obj->parent2D->size.x += inputManager->GetDeltaPos().x;
		else {
			edit->isMove = false;

			obj->state--;
		}
		break;
	}
	obj->position = VECTOR2(obj->parent2D->position.x + CONTROLER_OFFSET, obj->parent2D->position.y);

}
void Edit::Move::SizeControlerH(Obj2D*obj) {

	int keyState = inputManager->GetPadAddress()[0].state;//キーステート

	if (MouseHitCheckEdit(obj))
		obj->scale = HALF_UP_SCALE;
	else
		obj->scale = ORIGINAL_SCALE;

	switch (obj->state)
	{
	case CONROL_STATE::INIT:

		obj->data = &sprHControler;
		obj->size = CONTROLER_X_SIZE;
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		obj->scale = ORIGINAL_SCALE;

		obj->type = EDIT_TYPE_CONTROL;

		obj->state++;
		//break;
	case  CONROL_STATE::SELECT:

		if (MouseHitCheckEdit(obj) && !edit->isMove)
			obj->state++;

		break;


	case  CONROL_STATE::MOVE:

		edit->isMove = true;

		if (keyState&Input::LEFT_CLICK)
			obj->parent2D->size.y -= inputManager->GetDeltaPos().y;
		else {
			edit->isMove = false;

			obj->state--;
		}
		break;
	}
	obj->position = VECTOR2(obj->parent2D->position.x, obj->parent2D->position.y - CONTROLER_OFFSET);

}




