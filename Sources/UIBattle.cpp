#include "../Headers/UIBattle.h"
#include "../Headers/SpriteData.h"
#include "../Headers/SceneBattle.h"
#include "../Headers/Common.h"

#include <string>
#include <bitset>
#include <math.h>

//*******************************************************
//
//	定数
//
//*******************************************************

static const VECTOR2 LIFEBAR_POS_1P = VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 - 160.f, (float)DxSystem::SCREEN_HEIGHT / 8.f);
static const VECTOR2 LIFEBAR_POS_2P = VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 + 160.f, (float)DxSystem::SCREEN_HEIGHT / 8.f);
static const VECTOR2 LIFE_FRAME_POS_1P = VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 - 160.f - 770.f, (float)DxSystem::SCREEN_HEIGHT / 8.f);
static const VECTOR2 LIFE_FRAME_POS_2P = VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 + 160.f, (float)DxSystem::SCREEN_HEIGHT / 8.f);

static const VECTOR2 ARTSBAR_POS_1P = VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 - 899, (float)DxSystem::SCREEN_HEIGHT - 50);
static const VECTOR2 ARTSBAR_POS_2P = VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 + 899, (float)DxSystem::SCREEN_HEIGHT - 50);

static const VECTOR2 COMBO_NUM_POS_1P = VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 - 390, (float)DxSystem::SCREEN_HEIGHT / 2.f);
static const VECTOR2 COMBO_NUM_POS_2P = VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 + 910, (float)DxSystem::SCREEN_HEIGHT / 2.f);

static const VECTOR2 COMBO_NUM2_POS_1P = VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 - 450, (float)DxSystem::SCREEN_HEIGHT / 2.f);
static const VECTOR2 COMBO_NUM2_POS_2P = VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 + 850, (float)DxSystem::SCREEN_HEIGHT / 2.f);

static const VECTOR2 COMBO_FONT_POS_1P = VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 - 650, (float)DxSystem::SCREEN_HEIGHT / 2.f);
static const VECTOR2 COMBO_FONT_POS_2P = VECTOR2((float)DxSystem::SCREEN_WIDTH / 2 + 650, (float)DxSystem::SCREEN_HEIGHT / 2.f);



//初期化
void BattleUI::Init()
{
	Obj2DManager::Init();   // Obj2DManagerの初期化

	//ライフゲージ生成-----------------------------------------------------------------------------------------------------------------------------------------
	{
		Obj2D* child = Create(BattleUI::Move::Life, LIFEBAR_POS_1P);
		child->parent3D = sceneBattle->player1;
		Create(BattleUI::Move::Frame, LIFE_FRAME_POS_1P);
	}

	{
		Obj2D* child = Create(BattleUI::Move::Life, LIFEBAR_POS_2P);
		child->parent3D = sceneBattle->player2;
		Create(BattleUI::Move::Frame, LIFE_FRAME_POS_2P);

	}


	//アーツゲージ生成-----------------------------------------------------------------------------------------------------------------------------------------
	{

		Obj2D* child = Create(BattleUI::Move::Arts, ARTSBAR_POS_1P);
		child->parent3D = sceneBattle->player1;
		child->color = VECTOR4(1, 0, 0, 1);

		Obj2D* child2 = Create(BattleUI::Move::ArtsFrame, ARTSBAR_POS_1P);
		child2->parent3D = sceneBattle->player1;
	}

	{
		
		Obj2D* child = Create(BattleUI::Move::Arts, ARTSBAR_POS_2P);
		child->parent3D = sceneBattle->player2;
		child->color = VECTOR4(0, 0, 1, 1);
		Obj2D* child2 = Create(BattleUI::Move::ArtsFrame, ARTSBAR_POS_2P);
		child2->parent3D = sceneBattle->player2;
	}





	//----------------------------------------------------------------------------------------------------------------------------------------------------------

	//コンボカウンター生成----------------------------------------------------------------------------------------------------------------------------------

	//1P側生成
	{
		Obj2D* child = Create(BattleUI::Move::ComboCounterFont, COMBO_FONT_POS_1P);
		child->parent3D = sceneBattle->player1;
	}
	{
		Obj2D* child = Create(BattleUI::Move::ComboCounter, COMBO_NUM_POS_1P);
		child->parent3D = sceneBattle->player1;
	}
	{
		Obj2D* child = Create(BattleUI::Move::ComboCounter2, COMBO_NUM2_POS_1P);
		child->parent3D = sceneBattle->player1;
	}

	//2P側生成
	{
		Obj2D* child = Create(BattleUI::Move::ComboCounterFont, COMBO_FONT_POS_2P);
		child->parent3D = sceneBattle->player2;
	}

	{
		Obj2D* child = Create(BattleUI::Move::ComboCounter, COMBO_NUM_POS_2P);
		child->parent3D = sceneBattle->player2;
	}
	{
		Obj2D* child = Create(BattleUI::Move::ComboCounter2, COMBO_NUM2_POS_2P);
		child->parent3D = sceneBattle->player2;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------




	{
		Obj2D* child = Create(BattleUI::Move::KeyLogger, VECTOR2(100, 200));
		child->parent3D = sceneBattle->player1;
	}
	{
		Obj2D* child = Create(BattleUI::Move::KeyLogger, VECTOR2(1820, 200));
		child->parent3D = sceneBattle->player2;
	}



	//ラウンドコール生成----------------------------------------------------------------------------------------------------------------------------------
	Create(BattleUI::Move::RoundCall, SCREEN_CENTER);
}

//ライフフレーム描画
void BattleUI::Move::Frame(Obj2D*obj)
{
	//描画だけ(プレイヤーの裏側
	obj->data = &sprLifeFrame;
	obj->color = ORIGINAL_COLOR;
	obj->scale = ORIGINAL_SCALE;
	obj->isFront = false;
}


void BattleUI::Move::Life(Obj2D*obj)
{

	obj->data = &sprLife;

	obj->color = ORIGINAL_COLOR;

	obj->isFront = false;

	//親プレイヤーによって中心座標変更
	switch (obj->parent3D->GetPad())
	{
	case PLAYER_ONE:
		obj->center = VECTOR2(obj->data->texWidth, obj->data->centerY);
		break;
	case PLAYER_TWO:
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		break;
	}

	//残りHPでスケール増減
	obj->scale = VECTOR2((obj->parent3D->GetHp() / HP_MAX), 1.f);

}


void BattleUI::Move::ArtsFrame(Obj2D*obj)
{
	//描画だけ(プレイヤーの表側
	obj->data = &sprArtsFrame;
	obj->color = ORIGINAL_COLOR;
	switch (obj->parent3D->GetPad())
	{
	case PLAYER_ONE:
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		break;
	case PLAYER_TWO:
		obj->center = VECTOR2(obj->data->texWidth, obj->data->centerY);
		break;
	}

	obj->scale = VECTOR2((ARTS_MAX / 75.f), 1.8f);


	obj->isFront = true;
}

void BattleUI::Move::Arts(Obj2D*obj)
{

	//プレイヤーの表側

	obj->data = &sprArts;

	//親プレイヤーによって中心座標変更

	switch (obj->parent3D->GetPad())
	{
	case PLAYER_ONE:
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		break;
	case PLAYER_TWO:
		obj->center = VECTOR2(obj->data->texWidth, obj->data->centerY);
		break;
	}

	obj->scale = VECTOR2((obj->parent3D->GetArtsPoint()/75.f), 1.8f);




	obj->isFront = true;
}



struct MOVE_STATE_CALL
{
	enum type {
		INIT,
		SIZE_UP_1,
		SIZE_DOWN_1,
		SIZE_UP_2,
		SIZE_DOWN_2,
	};
};

static const VECTOR2 UP_SCALE = VECTOR2(0.3f, 0.3f);
static const float SCALE_LIMIT = 3.f;
static const int TIME_LIMIT = 60;


void BattleUI::Move::RoundCall(Obj2D * obj)
{
	switch (obj->state)
	{
	case MOVE_STATE_CALL::INIT:

		if (sceneBattle->round == 0)
			obj->data = &sprRoundCallRound1;
		else if (sceneBattle->round == 1)
			obj->data = &sprRoundCallRound2;
		else
			obj->data = &sprRoundCallRound3;

		obj->scale = {};
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		obj->color = ORIGINAL_COLOR;

		obj->timer = 0;
		obj->isFront = true;

		obj->state++;

		//break; 意図的

		//大きく
	case MOVE_STATE_CALL::SIZE_UP_1:

		if (obj->scale.x < SCALE_LIMIT)
			obj->scale += UP_SCALE;


		if (++obj->timer > TIME_LIMIT) {
			obj->timer = 0;
			obj->state++;
		}
		break;

		//小さく
	case MOVE_STATE_CALL::SIZE_DOWN_1:

		if (obj->scale.x > 0)
			obj->scale -= UP_SCALE;
		else {
			obj->data = &sprRoundCallFight;
			obj->state++;

		}
		break;

		//また大きく
	case MOVE_STATE_CALL::SIZE_UP_2:

		if (obj->scale.x < SCALE_LIMIT)
			obj->scale += UP_SCALE;


		if (++obj->timer > TIME_LIMIT) {
			obj->timer = 0;
			obj->state++;
		}
		break;

		//小さくして消す
	case MOVE_STATE_CALL::SIZE_DOWN_2:

		if (obj->scale.x > 0)
			obj->scale -= UP_SCALE;
		else {
			obj->data = &sprRoundCallFight;
			obj->Clear();

		}
		break;
	}
}

void BattleUI::Move::WinnerCall(Obj2D * obj)
{
	switch (obj->state)
	{
	case MOVE_STATE_CALL::INIT:

		if (sceneBattle->player1->GetHp() > sceneBattle->player2->GetHp())
			obj->data = &sprRoundCallPlayer1;
		else
			obj->data = &sprRoundCallPlayer2;

		obj->scale = VECTOR2(0, 0);
		obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
		obj->color = ORIGINAL_COLOR;

		obj->timer = 0;
		obj->isFront = true;

		obj->state++;
		//break; 意図的

	case MOVE_STATE_CALL::SIZE_UP_1:

		if (obj->scale.x < SCALE_LIMIT)
			obj->scale += UP_SCALE;


		if (++obj->timer > TIME_LIMIT) {
			obj->timer = 0;
			obj->state++;
		}
		break;

	case MOVE_STATE_CALL::SIZE_DOWN_1:

		if (obj->scale.x > 0)
			obj->scale -= UP_SCALE;
		else {
			obj->data = &sprRoundCallWin;
			obj->state++;

		}
		break;


	case MOVE_STATE_CALL::SIZE_UP_2:

		if (obj->scale.x < SCALE_LIMIT)
			obj->scale += UP_SCALE;


		if (++obj->timer > TIME_LIMIT) {
			obj->timer = 0;
			obj->state++;
		}
		break;

	case MOVE_STATE_CALL::SIZE_DOWN_2:

		if (obj->scale.x > 0)
			obj->scale -= UP_SCALE;
		else {
			obj->data = &sprRoundCallWin;
			obj->Clear();

		}
		break;
	}
}

static const int DELETE_TIME = 60;
static const float ADJUST_POS = 50.f;

//コンボカウンター表示(数字一の位
void BattleUI::Move::ComboCounter(Obj2D * obj)
{

	switch (obj->state)
	{
	case MOVE_STATE::INIT:
		obj->scale = TWICE_SCALE;
		obj->color = ORIGINAL_COLOR;
		obj->timer = 0;
		obj->initPos = obj->position;
		obj->state++;
		//break; 意図的

		//２コンボ以上なら表示
	case MOVE_STATE::MAIN:


		if (obj->parent3D->GetComboCnt() > 1) {
			obj->data = sprGameFontNum[obj->parent3D->GetComboCnt() % 10];
			obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
			obj->timer = DELETE_TIME;
		}
		//一定フレーム表示後画像を空にする
		else if (--obj->timer <= 0)
		{
			obj->data = nullptr;
		}



		break;
	}
}
//コンボカウンター表示(数字十の位
void BattleUI::Move::ComboCounter2(Obj2D * obj)
{

	switch (obj->state)
	{
	case  MOVE_STATE::INIT:
		obj->scale = TWICE_SCALE;
		obj->color = ORIGINAL_COLOR;
		obj->timer = 0;
		obj->initPos = obj->position;
		obj->state++;
		//break; 意図的

	case  MOVE_STATE::MAIN:

		//２コンボ以上なら表示

		if (obj->parent3D->GetComboCnt() > 1) {

			if (obj->parent3D->GetComboCnt() >= 99)
				obj->data = sprGameFontNum[9];
			else
				obj->data = sprGameFontNum[obj->parent3D->GetComboCnt() / 10];

			obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
			obj->timer = DELETE_TIME;
		}
		//一定フレーム表示後画像を空にする
		else if (--obj->timer <= 0)
		{
			obj->data = nullptr;
		}


		break;
	}
}

//コンボカウンター表示
void BattleUI::Move::ComboCounterFont(Obj2D * obj)
{

	switch (obj->state)
	{
	case  MOVE_STATE::INIT:
		obj->scale = TWICE_SCALE;
		obj->color = ORIGINAL_COLOR;
		obj->timer = 0;
		obj->state++;
		//break; 意図的

	case  MOVE_STATE::MAIN:



		//２コンボ以上なら表示
		if (obj->parent3D->GetComboCnt() > 1) {

			obj->data = &sprGameFontCombo;
			obj->center = VECTOR2(obj->data->centerX, obj->data->centerY);
			obj->timer = DELETE_TIME;

		}
		//一定フレーム表示後画像を空にする
		else if (--obj->timer <= 0) {
			obj->data = nullptr;
		}


		break;

	}
}

void BattleUI::Move::KeyLogger(Obj2D * obj)
{
	obj->scale = ORIGINAL_SCALE;
	obj->color = ORIGINAL_COLOR;
	obj->isAdd = true;
	if(obj->parent3D->GetKeyTrg())
	obj->position.y +=  64;


	if (obj->position.y > 1200)obj->Clear();

		switch (obj->type)
		{
		case Input::PAD_TRG1:

			obj->data = &sprKeyX;

			break;

		case Input::PAD_TRG2:

			obj->data = &sprKeyY;

			break;

		case Input::PAD_TRG3:

			obj->data = &sprKeyB;

			break;

		case Input::PAD_TRG4:

			obj->data = &sprKeyA;
			break;
		case Input::PAD_R1:

			obj->data = &sprKeyR;

			break;

		case Input::PAD_L1:

			obj->data = &sprKeyL;

			break;

		case Input::PAD_UP:

			obj->data = &sprArrowKeyU;
			obj->scale = VECTOR2(1, 1);
			obj->color = VECTOR4(1, 0.5f, 0.5f, 1);

			break;


		case Input::PAD_DOWN:

			obj->data = &sprArrowKeyD;
			obj->scale = VECTOR2(1, 1);
			obj->color = VECTOR4(1, 0.5f, 0.5f, 1);

			break;


		case Input::PAD_LEFT:

			obj->data = &sprArrowKeyL;
			obj->scale = VECTOR2(1, 1);
			obj->color = VECTOR4(1, 0.5f, 0.5f, 1);

			break;

		case Input::PAD_RIGHT:

			obj->data = &sprArrowKeyR;
			obj->scale = VECTOR2(1, 1);
			obj->color = VECTOR4(1, 0.5f, 0.5f, 1);


			break;


		case Input::PAD_UP+ Input::PAD_RIGHT:

			obj->data = &sprArrowKeyRU;
			obj->scale = VECTOR2(1, 1);
			obj->color = VECTOR4(1, 0.5f, 0.5f, 1);

			break;


		case Input::PAD_UP + Input::PAD_LEFT:

			obj->data = &sprArrowKeyLU;
			obj->scale = VECTOR2(1, 1);
			obj->color = VECTOR4(1, 0.5f, 0.5f, 1);

			break;


		case Input::PAD_DOWN + Input::PAD_RIGHT:

			obj->data = &sprArrowKeyRD;
			obj->scale = VECTOR2(1, 1);
			obj->color = VECTOR4(1, 0.5f, 0.5f, 1);

			break;

		case Input::PAD_DOWN + Input::PAD_LEFT:

			obj->data = &sprArrowKeyLD;
			obj->scale = VECTOR2(1, 1);
			obj->color = VECTOR4(1, 0.5f, 0.5f, 1);


			break;



		default:

			break;

		}

		






}
