
#include"../Headers/SceneBattle.h"

#include"../Headers/Judge.h"
#include "../Headers/UIBattle.h"
#include "../Headers/UIMenu.h"
#include "../Headers/LoadingAnime.h"
#include "../../Headers/Edit.h"
#include "../Headers/Effect.h"
#include "../GameLib/Headers/GameCamera.h"
#include"../Headers/SceneTitle.h"
#include"../Headers/SceneMenu.h"

#include <thread>



//************************************************************
//
//					対戦部分処理
//
//************************************************************

//----------------------------------------
//初期化処理
//----------------------------------------
void SceneBattle::Init()
{

	sceneBattle->player1 = new Player();
	sceneBattle->player1->SetChNo(0);

	sceneBattle->player2 = new Player();
	sceneBattle->player2->SetChNo(0);


	if (menuUI->mode == GAME_MODE::ARCADE)
		player2->SetIsCPU(true);




	shadowMap.Initialize();
	nextScene = nullptr;

	isDead = false;
	isInput = true;

	state = 0;
	round = 0;
	//レンダーターゲット用の実体初期化

	screen = new RenderTarget();
	screen->Initialize();

	//ブレンド初期化
	blend.Initialize();

	//タイマー
	timer = 0;
	changeTimer = 0;
	battleTimer = TIME_MAX;

	//ロード用画像読み込み
	SpriteLoadLoading();

	//システムフォント
	systemFont = new Sprite("Fonts/font4.png");

	//エフェクト初期化
	effectManager->Init();

	//カメラ初期化
	gameCamera->Init();

	//定数バッファ生成
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.ByteWidth = sizeof(ConstantBufferParam);
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	HRESULT hr = DxSystem::device->CreateBuffer(&bd, NULL, &constantBuffer);

	//エラー処理
	if (FAILED(hr))
	{
		assert(false && "定数バッファの生成に失敗(シーンバトル)");
		return;
	}

	gameCamera->isScroll = true;


}

void SceneBattle::Load()
{
	//読み込み終わりやとリターン
	if (sceneBattle->isLoad)return;


	//ステージ生成
	stage = new Stage();
	stage->Init();

	//sprite読み込み
	SpriteLoadBattle();


	//1P初期化
	player1->Init(PLAYER_ONE);
	//2P初期化
	player2->Init(PLAYER_TWO);
	//敵情報セット
	player1->SetEnemy(player2);
	player2->SetEnemy(player1);

	//もし選択したキャラが同じならばカラー変更
	if (player1->GetChNo() == player2->GetChNo())
	{
		player2->Set2PColor();
	}


	//UI初期化
	battleUI->Init();




	//読み込み終わりのフラグ
	sceneBattle->isLoad = true;
}
//----------------------------------------
//更新処理
//----------------------------------------
void SceneBattle::Update()
{
	//キー入力保存
	int keyTrg1 = inputManager->GetPadAddress()[PLAYER_ONE].trigger;
	int keyTrg2 = inputManager->GetPadAddress()[PLAYER_TWO].trigger;

	//エンターキーでタイトルへ(ロード中は不可
	if ((keyTrg1&Input::PAD_START) && isLoad && !isEdit)
		SceneChange(sceneMenu);


	// 更新・描画
	switch (state)
	{
	case BATTLE_STATE::INIT:


		if (!sceneBattle->isLoad) {
			//プレイヤー読み込み処理スレッドスタート
			std::thread th(&SceneBattle::Load, sceneBattle);
			th.detach();
		}

		//UI更新
		loadUI->Init();

		state++;
		//break;


	case BATTLE_STATE::LOADING:

		//ロード中UI更新
		loadUI->Update();

		break;


	case BATTLE_STATE::MAIN:

		////編集モード切り替え
		if (keyTrg1&Input::PAD_SELECT)
			EditerInit();

		////通常
		if (!isEdit) {

			//ヒットストップ中
			if (isStop) {

				player1->SetIsActive(false);
				player2->SetIsActive(false);

				if (--this->stopFrame <= 0)
					isStop = false;

			}

			//プレイヤー更新1P
			player1->Update();
			//プレイヤー更新2P
			player2->Update();

			//必殺技ゲージ入れ替え(位置入れ替えで反転
			if (player1->GetOldPosition().x > player2->GetOldPosition().x&&
				player1->GetPosition().x < player2->GetPosition().x)
			{
				float tmp = 0;
				tmp = player1->GetArtsPoint();

				player1->SetArtsPoint(player2->GetArtsPoint());
				player2->SetArtsPoint(tmp);

				sceneBattle->isCross = true;
			}
			else if (player1->GetOldPosition().x < player2->GetOldPosition().x&&
				player1->GetPosition().x > player2->GetPosition().x)
			{

				float tmp = 0;
				tmp = player1->GetArtsPoint();

				player1->SetArtsPoint(player2->GetArtsPoint());
				player2->SetArtsPoint(tmp);

				sceneBattle->isCross = true;

			}
			//ステージ更新
			stage->Update();

			//バトル残り時間更新
			TimeUpdate();

			++timer;

		}
		//編集モードなら
		else
		{
			//キー入力があればコマ送り
			if (keyTrg1&KEY_MOVE || keyTrg2&KEY_MOVE||inputManager->GetPadAddress()[0].right) {

				//ヒットストップしない
				isStop = false;

				//プレイヤー更新1P
				player1->Update();

				//プレイヤー更新2P
				player2->Update();
			}
			//編集モード更新
			edit->Update();
		}

		//UI更新
		battleUI->Update();



		//エフェクト更新
		effectManager->Update();

		//カメラ更新
		gameCamera->Update();

		//当たり判定
		judge->Execute();


		//死亡処理
		DeadMove();
		break;

	}
}

//----------------------------------------
//描画処理
//----------------------------------------
void SceneBattle::Render()
{
	//シャドウマップ描画
	ShadowMapRender();

	//最初にテクスチャにレンダリングする
	screen->Activate();

	//画面クリア
	DxSystem::Clear();

	//ブレンドモード設定
	blend.Set(blend.BS_ALPHA);

	//クリアされてるのでシャドウマップ再起動
	shadowMap.Deactivate();

	switch (state)
	{
		//ロード中
	case BATTLE_STATE::LOADING:
		loadUI->Render();

		if (isLoad)state++;
		break;

		//バトル中
	case BATTLE_STATE::MAIN:

		//ステージ描画
		stage->Render(constantBuffer);



		//画面暗転
		if (player1->GetIsDark() || player2->GetIsDark())
		{
			sprBlackOut.Render(VECTOR2(DxSystem::SCREEN_WIDTH/2.f,DxSystem::SCREEN_HEIGHT/2.f)
							  ,VECTOR2(1,1)
							  ,VECTOR4(1,1,1,0.5f)
							  ,0,VECTOR2(sprBlackOut.centerX, sprBlackOut.centerY));
		}

		//UI描画(プレイヤーの裏側に描画するもの
		battleUI->Render(false);

		//残り時間表示
		TimeRender();

		TraningFontRender();

		//プレイヤー描画
		PlayerRender();

		//UI描画(プレイヤーの表側に描画するもの
		battleUI->Render(true);

		// 全エフェクト描画
		effectManager->Render(blend);

		break;

	}
	//画面にレンダリングを戻す
	screen->Deactivate();
	//最後にポストエフェクトをかけて描画
	screen->Render();

	//編集ツールの表示
	if (isEdit)//編集モードなら
		edit->Render();

	//画面フリップ
	DxSystem::Flip();
}

//ヒットストップセット
void SceneBattle::SetHitStop(float stopFrame)
{
	if (isStop)return;

	//ストップ中フラグオン
	isStop = true;
	this->stopFrame = stopFrame;
}

//ヒットストップ初期化
void SceneBattle::ReleaseHitStop()
{
	isStop = false;
	this->stopFrame = 0;
}

//バトル再初期化
void SceneBattle::ReSet()
{

	nextScene = nullptr;
	isDead = false;
	timer = 0;
	changeTimer = 0;
	player1->InitParameter(PLAYER_ONE);
	player2->InitParameter(PLAYER_TWO);
	stage->Reset();

	//もし選択したキャラが同じならばカラー変更
	if (player1->GetChNo() == player2->GetChNo())
	{
		player2->Set2PColor();
	}
	//ラウンド進める
	if (!isEdit)
		round++;
}

//終了処理
void SceneBattle::UnInit()
{
	state = 0;

	delete screen;
	if (systemFont)delete systemFont;
	//ステージ生成
	if (stage) delete stage;
	//生成
	if (player1)delete player1;
	if (player2)delete player2;

	SpriteUnLoadLoading();
	SpriteUnLoadBattle();

	if (constantBuffer)
		constantBuffer->Release();
	//読み込み終わりのフラグ
	sceneBattle->isLoad = false;
}

//残り時間更新
void SceneBattle::TimeUpdate()
{
	//トレーニングやとタイム無限
	if (menuUI->mode == GAME_MODE::TRANING)
		battleTimer = TIME_MAX - 1;

	if (!isInput) return;

	--battleTimer;

	//時間切れ
	if (battleTimer <= 0) {
		isDead = true;
		battleTimer = TIME_MAX;
	}
}
//死亡時
void SceneBattle::DeadMove()
{
	if (!isDead)return;

	++changeTimer;

	if (changeTimer == TIME_WIN_CALL)
	{
		//勝利者表示
		battleUI->Create(BattleUI::Move::WinnerCall, VECTOR2(DxSystem::SCREEN_WIDTH / 2.f, DxSystem::SCREEN_HEIGHT / 2.f));

		//HPが多いほうが勝利モーションを取る
		if (player1->GetHp() > player2->GetHp()) {

			player1->AddWinCnt();
			player1->SetMoveState(PLAYER_MOVE::WIN);
			player1->GetMesh()->Play(PLAYER_MOTION::WIN);
			player1->SetSpeed({});

		}
		else if (player1->GetHp() < player2->GetHp()) {

			player2->AddWinCnt();
			player2->SetMoveState(PLAYER_MOVE::WIN);
			player2->GetMesh()->Play(PLAYER_MOTION::WIN);
			player2->SetSpeed({});
		}

	}

	//次のラウンド開始までの時間計測
	if (changeTimer > TIME_SCENE_CHANGE) {

		//二勝している場合はタイトルに戻る
		if (player1->GetWinCnt() >= 2 || player2->GetWinCnt() >= 2) {

			SceneChange(sceneMenu);
		}
		else {
			//それ以外はラウンドを進める
			ReSet();
			battleTimer = TIME_MAX;
			battleUI->Init();
		}
	}

}
//シャドウマップ描画
void SceneBattle::ShadowMapRender()
{
	if (!sceneBattle->isLoad)return;

	// シャドウマップ
	shadowMap.Activate(
		VECTOR3(0, 0, 0),
		VECTOR3(0, -1, 1)
	);
	ConstantBufferParam cb;

	cb.world = stage->GetMesh()->transform;
	//頂点情報セット
	DxSystem::deviceContext->UpdateSubresource(constantBuffer, 0, NULL, &cb, 0, 0);
	stage->GetMesh()->Render();

	cb.world = player1->GetMesh()->transform;
	//頂点情報セット
	DxSystem::deviceContext->UpdateSubresource(constantBuffer, 0, NULL, &cb, 0, 0);
	player1->GetMesh()->Render();

	cb.world = player2->GetMesh()->transform;
	//頂点情報セット
	DxSystem::deviceContext->UpdateSubresource(constantBuffer, 0, NULL, &cb, 0, 0);
	player2->GetMesh()->Render();
}
//残り時間表示
void SceneBattle::TimeRender()
{
	if (battleTimer >= 0) {
		sprGameFontNum[(battleTimer / 60) / 10]->Render(VECTOR2((float)(DxSystem::SCREEN_WIDTH / 2 - 45), 150.f), VECTOR2(2, 2));
		sprGameFontNum[(battleTimer / 60) % 10]->Render(VECTOR2((float)(DxSystem::SCREEN_WIDTH / 2 + 45), 150.f), VECTOR2(2, 2));
	}
}
//トレーニングモード限定のデバッグフォント
void SceneBattle::TraningFontRender()
{
	/*if (menuUI->mode != GAME_MODE::TRANING) return;
	systemFont->Textout(VECTOR2(100,100) ,VECTOR2(1,1),VECTOR4(1,1,1,1),"Y:%f Z:%f",stage->GetMesh()->position.y, stage->GetMesh()->position.z);
	*/
}
//プレイヤー描画
void SceneBattle::PlayerRender()
{
	//プレイヤー描画(ガードしたら入れ替え)
	if (player1->GetIsPrioritize()) {
		player2->Render(constantBuffer);
		screen->ClearZBuffer();
		player1->Render(constantBuffer);
	}
	else {
		player1->Render(constantBuffer);
		screen->ClearZBuffer();
		player2->Render(constantBuffer);
	}
}
//エディタ初期化
void SceneBattle::EditerInit()
{
	//エディタ初期化
	edit->Init();

	//パラメータ初期化
	player1->InitParameter(PLAYER_ONE);
	player2->InitParameter(PLAYER_TWO);

	//ステージ初期化
	stage->Reset();
	//プレイヤー更新1P
	player1->Update();
	//プレイヤー更新2P
	player2->Update();

	//カメラ更新
	gameCamera->Update();

	//プレイヤー更新1P
	player1->Update();
	//プレイヤー更新2P
	player2->Update();

	//エディット切り替え
	isEdit = !isEdit;

}





