//ShadowMap.fx
cbuffer CBPerFrame : register(b0)
{
	matrix  World;
	matrix  View;
	matrix  Proj;
	matrix  WVP;
};
cbuffer CBShadowMap :register(b2){
	matrix  ShadowVP;
}

struct VSInput
{
	float3 Position : POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
	float4 Color : COLOR;
};
struct PSInput
{
	float4 Position : SV_POSITION;  //!< 位置座標です.
	float4 ScreenPos : TEXCOORD1;
};

PSInput VSMain(VSInput input)
{
	PSInput output = (PSInput)0;
	// 光から見た位置
	float4 P = float4(input.Position, 1.0);
	P = mul(World, P);
	float4 projPos = mul(ShadowVP, P);
	// 出力値設定.
	output.Position = projPos;
	output.ScreenPos = projPos;

	return output;
}

float4 PSMain(PSInput input) : SV_TARGET0
{
	float4 col= input.ScreenPos.z;

	col.a = 1.0f;

	return col;
}
