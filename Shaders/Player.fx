// Assets/base.fx
cbuffer CBPerFrame : register(b0)
{
	matrix  World;
	matrix  View;
	matrix  Proj;
	matrix  WVP;
	float4  Color;
	float3  ViewPos;
	float   DirPlayer;

};

Texture2D Diffuse : register(t0);
SamplerState Decale : register(s0);

//光からの距離が入ってる
cbuffer CBShadowMap :register(b2) {
	matrix  ShadowVP;
}
Texture2D ShadowMap : register(t8);

//ステージ用
Texture2D Stage : register(t1);
//法線マップ用テクスチャ
Texture2D StageNormal : register(t3);

struct VSInput
{
	float3 Position : POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
	float4 Color : COLOR;

};

struct PSInput
{
	float4 Position : SV_POSITION;  //!< 位置座標です.
	float2 Tex : TEXCOORD;
	float4 Color : COLOR;
	float4 ScreenPos :TEXCOORD1;
	float3 Normal:NORMAL;
	float3 WorldPos : TEXCOORD2;

};




//****************************************************************
//キャラクターシェーダ
//****************************************************************
PSInput VSChara(VSInput input)
{
	PSInput output = (PSInput)0;

	float3 pos = input.Position;



	float4 P = float4(pos, 1.0);
	float4 projPos = mul(WVP, P);

	// 出力値設定.
	output.Position = projPos;
	output.Color = Color;
	output.Tex = input.Tex;
	output.ScreenPos = projPos;// / projPos.w;
	output.Normal = mul((float3x3)World,input.Normal);
	output.WorldPos = mul((float3x3)World, P.xyz);

	



	return output;

}

// ピクセルシェーダー
// 画面にピクセルを打つときのプログラム
float4 PSChara(PSInput input) : SV_TARGET0
{
	float4 color;
color = input.Color;

//ライト計算(COSが適切、内積により算出)
float3 L = float3(1, 0, 0);
L = normalize(L);

//法線
float3 N = normalize(input.Normal);

//明るさ
float I = -dot(L, N);

//0とIで大きい方を選択(-を消す)

//I = max(0,I);//ランバート(90度で黒くなる)

//0-1に収める(-を消す2,幅合わせ)

I = I*0.5 + 0.5;//ハーフランバート(90度で黒くならない)

//Toonその1(if)
if (I < 0.4)I = 0.6;
else I = 1.0;



//シャドウマップ
float4 P = float4(input.WorldPos, 1);
//ライト空間からのXYZ
float4 LightSpace = mul(ShadowVP, P);


//自分がどの距離にいるか
float MyDepth = LightSpace.z;
//スクリーン座標→テクスチャ座標
float2 uv = LightSpace.xy*0.5 + 0.5;
//Y反転
uv.y = 1.0 - uv.y;



//自分と同じ光の直線上にある最も光に近い座標
float Depth = ShadowMap.Sample(Decale, uv).r;

//自分が最も近い距離にいない場合
//誤差の範囲分近づける
if (Depth < MyDepth - 0.001)I = 0.6;

//Toonその2(RampTex)
//float4 ramp = Ramp.Sample(Decale, float2(I,0));


color.rgb *= I;

//明るさに応じてコントラスト
//I=0.0<-->1.0
//c=2.0<-->1.0
float c = 2.0 - I;
color.rgb = (color.rgb - 0.5)*c + 0.5;




return color;
}


//****************************************************************
//キャラクターシェーダ
//****************************************************************
PSInput VSOutLine(VSInput input)
{
	PSInput output = (PSInput)0;

	float3 pos = input.Position;
	pos += normalize(input.Normal)*1.0f;




	float4 P = float4(pos, 1.0);



	float4 projPos = mul(WVP, P);

	// 出力値設定.
	output.Position = projPos;
	output.Color = input.Color;
	output.Tex = input.Tex;
	output.ScreenPos = projPos / projPos.w;

	//法線を渡す
	output.Normal = mul((float3x3)World, input.Normal);

	
	return output;
}

// ピクセルシェーダー
// 画面にピクセルを打つときのプログラム
float4 PSOutLine(PSInput input) : SV_TARGET0
{
	return float4(0,0,0,1.0f);
}







