// プログラム側から受け取る情報
cbuffer CBPerMesh : register(b0)
{
	matrix  World;
	matrix	View;
	matrix	Proj;
	matrix	matWVP;
};
//	テクスチャ
Texture2D DiffuseTexture : register(t0);
SamplerState DecalSampler : register(s0);

//	入力頂点
struct APPtoVS {
	float3 Position : POSITION;
	float3 Normal   : NORMAL;
	float2 Tex      : TEXCOORD;
	float4 Color    : COLOR;
};

struct VStoPS
{
	float4 Position : SV_POSITION;
	float2 Tex : TEXCOORD0;
};

//--------------------------------------------
//	頂点シェーダー
//--------------------------------------------
VStoPS VSMain(APPtoVS input)
{
	VStoPS output = (VStoPS)0;
	output.Position = float4(input.Position, 1);
	output.Tex = input.Tex;
	return output;
}


#define CONTLAST_AVERAGE 0.5 //光の平均値

//--------------------------------------------
//	ピクセルシェーダー
//--------------------------------------------
float4 PSMain(VStoPS input) : SV_TARGET0
{
	float4 tex =
	DiffuseTexture.Sample(
		DecalSampler, input.Tex);// *input.Color;


	float4 color;


	//----------------輝度調整---------------------------
	color = tex + 0.1;
	//color = tex * 1.4;//基本的には使わない

	float gray = dot(color.rgb,
		float3(0.299, 0.587, 0.144));
	color = (color - gray)*0.55 + gray;//彩度0.6



	
	//----------------コントラスト調整-------------------
	color = (color - CONTLAST_AVERAGE)*1.2 + CONTLAST_AVERAGE;//コントラスト1.7倍

	//暗転
	//color.rgb -= float3(0.3, 0.3, 0.3);

	return tex;
}