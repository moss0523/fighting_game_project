#pragma once

#include "../GameLib/Headers/Obj2D.h"


//********************************************************
//
//			読み込み中アニメーション描画
//
//********************************************************


#define LOAD_MAX 1



class LoadUI : public Obj2DManager, public Singleton<LoadUI>
{

public:

	void Init(); // 初期化
	

	//移動関数インナークラス
	class Move{
	public:
		static void LoadingMove(Obj2D*obj);
	};


private:
	//vector resize用
	int GetSize() { return LOAD_MAX; }




};
//インスタンス取得マクロ
#define loadUI	(LoadUI::GetInstance())

