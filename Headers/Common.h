#pragma once

//******************************************
//
//			共通パラメータ
//
//******************************************

struct MOVE_STATE
{
	enum type {
		INIT,
		MAIN,
		SUB,
	};
};

static const VECTOR2 SCREEN_CENTER = VECTOR2((float)DxSystem::SCREEN_WIDTH/2.f, (float)DxSystem::SCREEN_HEIGHT / 2.f);

static const VECTOR2 ORIGINAL_SCALE = VECTOR2(1, 1);//スケール等倍
static const VECTOR2 HALF_UP_SCALE = VECTOR2(1.5f, 1.5f);//スケール1.5倍
static const VECTOR2 TWICE_SCALE = VECTOR2(2, 2);//スケール二倍
static const VECTOR2 THREE_TIMES_SCALE = VECTOR2(3, 3);//スケール三倍

static const VECTOR4 ORIGINAL_COLOR = VECTOR4(1,1,1,1);//白色、カラー変更無し
static const VECTOR4 RED_COLOR = VECTOR4(1, 0, 0, 1);//赤色
static const VECTOR4 GREEN_COLOR = VECTOR4(0, 1, 0, 1);//緑色
static const VECTOR4 BLUE_COLOR = VECTOR4(0, 0, 1, 1);//青色

static const VECTOR4 HIT_RECT_COLOR = VECTOR4(1, 0, 0, 0.4f);//攻撃判定の色
static const VECTOR4 HURT_RECT_COLOR = VECTOR4(0, 1, 0, 0.4f);//ダメージ判定の色
static const VECTOR4 PUSH_RECT_COLOR = VECTOR4(1, 1, 0, 0.4f);//押し合い判定の色
