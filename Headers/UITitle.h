#pragma once

#include "../GameLib/Headers/Obj2D.h"

#define UI_MAX 10


class TitleUI : public Obj2DManager, public Singleton<TitleUI>
{
public:
	

private:
	int GetSize() { return UI_MAX; }
};

#define titleUI	(TitleUI::GetInstance())

