#pragma once

#include "../GameLib/Headers/Obj2D.h"

#define MENU_UI_MAX 20

struct GAME_MODE {

	enum Type {

		TRANING,
		VERSUS,
		ARCADE,


	};
};


class MenuUI : public Obj2DManager, public Singleton<MenuUI>
{
public:
	void Init();    // 初期化
	void Update();  // 更新
	void Render();    // 描画


	//移動関数インナークラス
	class Move {
	public:
		static void Rotation(Obj2D*obj);

	};
	int mode;
private:
	int GetSize() { return MENU_UI_MAX; }


};

#define menuUI	(MenuUI::GetInstance())

