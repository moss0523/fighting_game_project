#pragma once

#include "../Headers/Player.h"
#include "../GameLib/Headers/ShadowMap.h"
#include "../GameLib/Headers/RenderTarget.h"
#include "../GameLib/Headers/BlendState.h"
#include "../Headers/Stage.h"
#include "../Headers/Scene.h"
#include "../Headers/SpriteData.h"

//***************************************************
//	
//			定数
//
//***************************************************
static const int TIME_MAX = 100 * 60;
static const int TIME_WIN_CALL =  60;
static const int TIME_SCENE_CHANGE = 300;

struct BATTLE_STATE
{
	enum type {

		INIT,
		LOADING,
		MAIN,

	};
};

//***************************************************
//	
//			バトルシーンクラス
//
//***************************************************
class SceneBattle :public Scene, public Singleton<SceneBattle>
{
private:

	int battleTimer = TIME_MAX;

	RenderTarget* screen;//スクリーン用実体(ポストエフェクト時の処理系に使用する)
	Sprite* systemFont;//フォント用
	BlendState blend;//ブレンドモード用実体
	ShadowMap shadowMap;//シャドウマップ用実体


	void TimeUpdate();		 //バトル残り時間更新
	void DeadMove();		 //死亡処理
	void ShadowMapRender();  //シャドウマップ描画
	void TimeRender();		 //タイマー描画
	void TraningFontRender();//トレーニングモードでの表示(ダメージなど)
	void PlayerRender();     //プレイヤー描画、描画順入れ替え
	void EditerInit();       //エディタ初期化
public:


	Player*player1;//1Pキャラクター処理用
	Player*player2;//2Pキャラクター処理用

	Stage * stage;

	//定数バッファに渡す値達
	ID3D11Buffer* constantBuffer;
	struct ConstantBufferParam
	{
		MATRIX world;
		MATRIX view;
		MATRIX proj;
		MATRIX wvp;
		VECTOR4 color;
		VECTOR3 ViewPos;
		float param;
	};



	void Load();
	void Init();
	void ReSet();
	void UnInit();
	void Update();
	void Render();

	bool isLoad = false;
	bool isEdit = false;
	bool isStop = false;
	bool isDead = false;
	bool isInput = true;
	bool isCross = false;
	float stopFrame;
	int round;

	//一括削除
	void SetHitStop(float stopFrame);
	void ReleaseHitStop();


};
#define sceneBattle SceneBattle::GetInstance()
