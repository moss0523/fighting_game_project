#pragma once

#include "../GameLib/Headers/RenderTarget.h"
#include "../GameLib/Headers/BlendState.h"
#include "../GameLib/Headers/Template.h"
#include "../GameLib/Headers/Sprite.h"
#include "../GameLib/Math/Matrix.h"
#include "../GameLib/Math/Vector.h"

#include "../Headers/Scene.h"

//***************************************************
//	
//		キャラクター選択シーンクラス
//
//***************************************************


class SceneTitle :public Scene, public Singleton<SceneTitle>
{
private:

	RenderTarget* screen;//スクリーン用実体(ポストエフェクト時の処理系に使用する)
	BlendState blend;//ブレンドモード用実体
	Sprite* systemFont;

	int state;
	int timer;
public:


	void Init();
	void UnInit();

	void Update();
	void Render();

	


};
#define sceneTitle SceneTitle::GetInstance()
