#pragma once
//インクルード
#include "../GameLib/Headers/FbxMesh.h"
#include "../GameLib/Headers/DXShader.h"
#include "../GameLib/Headers/Template.h"


//************************************************
//
//  ステージ
//
//************************************************
class Stage
{

private:
	Shader* stgShader;//ステージ用シェーダー実体
	FBXMesh* mesh;//ステージ用メッシュ実体
	Texture* tex;

	bool isShake;//画面揺れ判定フラグ
	float shakeRate;//画面揺れ判定フラグ
public:
	void Init();	//初期化

	void Reset();   //再初期化

	FBXMesh* GetMesh() { return mesh; }

	void SetShake(float rate=1.f);//画面揺れセット（プレイヤー側で呼ぶ

	//更新
	void Update();		
	//描画
	void Render(ID3D11Buffer* constantBuffer);

	~Stage();

};

