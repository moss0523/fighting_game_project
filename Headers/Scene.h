#pragma once
#include <windows.h>


//******************************************************************************
//
//
//      シーンの基底クラス
//
//
//******************************************************************************

//==============================================================================
//
//      Sceneクラス
//
//==============================================================================

class Scene
{
public:
	
	//シーン切り替えするまで無限ループで処理を回す
	Scene* Execute(HWND hwnd);

	void SceneChange(Scene *scene);

	//次のシーン情報を格納する
	Scene*nextScene;
	int state;          // 状態
	int timer;			//カウント
	int changeTimer;


	virtual void Init() {
		state = 0;
		nextScene = nullptr;
	}
	virtual void Update() {}
	virtual void Render() {}
	virtual void UnInit() {}


};

//******************************************************************************
//
//
//      シーン管理
//
//
//******************************************************************************

//==============================================================================
//
//      SceneManagerクラス
//
//==============================================================================
class SceneManager
{
public:
	//Scene::Executeをメイン関数で実行
	void Execute(HWND hwnd,Scene *scene);  // 実行処理
};

//******************************************************************************
