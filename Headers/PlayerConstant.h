#pragma once
#include "../GameLib/Headers/Command.h"
#include "../GameLib/Headers/FbxMesh.h"
#include "../GameLib/Headers/GameCamera.h"
#include "../GameLib/Headers/InputManager.h"
//*********************************************************
//
//				プレイヤーデータ、定数
//
//*********************************************************



//方向フラグ、パラメータとして仕様
static const int DIR_LEFT = -1;
static const int DIR_RIGHT = 1;

static const int   MOVE_TIME_RECOVER = 15;
static const float HP_MAX = 500.f;
static const float  DOWN_SPEED = -2.f;	//減衰する値(主に慣性の調整に使用する


static const VECTOR3 INIT_ROTATION_1P(0, 0, 0);//モデル回転値(1P
static const VECTOR3 INIT_ROTATION_2P(0, 0, 0);//モデル回転値(2P

static const VECTOR3 INIT_SCALE_1P(-0.005f, 0.005f, 0.005f);//モデルスケール1P
static const VECTOR3 INIT_SCALE_2P(0.005f, 0.005f, 0.005f);//モデルスケール2P

static const float INITPOS = 500.f;	//初期位置X

//方向決定
static const int DIR_ADDIST = 20;



//CPU
static const int CPU_INPUT = 0;//入力ステート
static const int CPU_NOW_MOVE = 1;//今動いてる
//距離判定の閾値
static const int NEAR_DISTANCE = 600;
static const int FAR_DISTANCE = 1200;
//思考更新時間
static const int INTERVAL_TIME = 30;


//スクロール制限
static const float STAGE_MAX = (float)(SCROLL_MAX_X + DxSystem::SCREEN_WIDTH / 2);
static const float STAGE_MIN = (float)(SCROLL_MIN_X - DxSystem::SCREEN_WIDTH / 2);

//共通パラメータ---------------------------------------------------
static const int HITSTOP_FRAME_THROWCOMP = 5;//投げ相殺ヒットストップ
static const float THROWCOMP_SPEED = -50.f;//投げ相殺ノックバック

static const int ROUND_CALL_TIME = 155;//ラウンド開始まで
static const int KNOCK_BACK_FRAME = 5;//ノックバックフレーム
static const int RECT_MAX = 2048;//矩形の数
static const int KEEP_FRAME = 5;//先行入力受付
static const int KEEP_MAX = 15;	//先行入力保持フレーム
static const int PLAYER_ONE = 0;//PLAYER番号
static const int PLAYER_TWO = 1;//PLAYER番号
static const float GROUND = 800.f;//地面の位置
static const float AIR_ATTACK_LIMIT = GROUND - 20.f;//これより下では攻撃を行わない
static const int JUMP_INIT_FRAME = 4;//ジャンプ移行フレーム(共通)
static const int BACK_STEP_AIR_FRAME = 20;//バクステ空中判定
static const float THROW_COMP_FRAME = 10.f;//投げ相殺時のヒットストップ
static const int DMG_FRAME_DOWN = 9999;//ダウン硬直
static const float COMBO_CORRECT = 1.5f;//コンボ補正(ヒット数で減少
static const float GUARD_CORRECT = 10.f;//ガード補正(ガード中のダメージを何分の一にするか
static const VECTOR2 RECOVER_SPEED = VECTOR2(-10.f, -25.f);//受け身移動値
static const float PUSH_LERP_RATE = 0.4f;//押し合い補間比率


static const int THROW_ATTACK_FRAME = 40;//投げてから攻撃までのフレーム
static const float THROW_ADJUST = 500.f;//投げ時の補正値

static const int JC_FALL_FRAME = 10;//JCの落下までのフレーム
static const float JC_FALL_SPEED = 13.f;//JCの落下スピード

//FEMALE固有フレーム
static const int ARTS01_MOVE_FRAME_FEMALE = 9;
static const int ARTS01_END_FRAME_FEMALE = 30;
static const float ARTS01_VELOCITY_FEMALE = 5.f;
//MALE固有フレーム
static const int ARTS01_MOVE_FRAME_MALE = 9;
static const int ARTS01_END_FRAME_MALE = 33;
static const float ARTS01_VELOCITY_MALE = 6.f;


static const int SHOT_INIT_FRAME = 15;//弾の発生フレーム

static const int JUMP_FALL_FRAME = 10;//落下中モーションに飛ばす





//KEY入力関連マクロ
#define KEY_MOVE (Input::PAD_RIGHT|Input::PAD_LEFT|Input::PAD_UP|Input::PAD_DOWN|Input::PAD_TRG1|Input::PAD_TRG2|Input::PAD_TRG3|Input::PAD_TRG4|Input::ARTS_1|Input::ARTS_2|Input::ARTS_3|Input::PAD_R1|Input::PAD_L1|Input::PAD_R2|Input::ARTS_1|Input::ARTS_2|Input::ARTS_3)
#define KEY_WALK (Input::PAD_RIGHT|Input::PAD_LEFT|Input::PAD_UP|Input::PAD_DOWN)
//歩き
#define PAD_WALK (PAD_LEFT|PAD_RIGHT)

//共通パラメータ配列のインデックスとして使用
struct PLAYER_PARAM
{
	enum type {
		WALK_SPEED,
		RUN_SPEED,
		JUMP_SPEED,
		BUCK_STEP_SPEEDX,
		BUCK_STEP_SPEEDY,
		HITSTOP_FRAME_NA,
		HITSTOP_FRAME_NB,
		HITSTOP_FRAME_NC,
		HITSTOP_FRAME_JA,
		HITSTOP_FRAME_JB,
		HITSTOP_FRAME_JC,
		HITSTOP_FRAME_LA,
		HITSTOP_FRAME_LB,
		HITSTOP_FRAME_LC,
		HITSTOP_FRAME_THROW,
		HITSTOP_FRAME_ARTS00,
		HITSTOP_FRAME_ARTS01,
		HITSTOP_FRAME_ARTS02,
		DMG_FRAME_MAX,
		DMG_FRAME_NA,
		DMG_FRAME_NB,
		DMG_FRAME_NC,
		DMG_FRAME_JA,
		DMG_FRAME_JB,
		DMG_FRAME_JC,
		DMG_FRAME_LA,
		DMG_FRAME_LB,
		DMG_FRAME_LC,
		DMG_FRAME_ARTS00,
		DMG_FRAME_ARTS01,
		DMG_FRAME_ARTS02,
		DMG_FRAME_AIR_MAX,
		DMG_FRAME_AIR_NA,
		DMG_FRAME_AIR_NB,
		DMG_FRAME_AIR_NC,
		DMG_FRAME_AIR_JA,
		DMG_FRAME_AIR_JB,
		DMG_FRAME_AIR_JC,
		DMG_FRAME_AIR_LA,
		DMG_FRAME_AIR_LB,
		DMG_FRAME_AIR_LC,
		DMG_FRAME_SHOT,
		RECOVER_SPEEDX,
		GRD_FRAME_NA,
		GRD_FRAME_NB,
		GRD_FRAME_NC,
		GRD_FRAME_JA,
		GRD_FRAME_JB,
		GRD_FRAME_JC,
		GRD_FRAME_LA,
		GRD_FRAME_LB,
		GRD_FRAME_LC,
		GRD_FRAME_ARTS00,
		GRD_FRAME_ARTS01,
		GRD_FRAME_ARTS02,
		GRD_FRAME_SHOT,
		ATTACK_NA,
		ATTACK_NB,
		ATTACK_NC,
		ATTACK_JA,
		ATTACK_JB,
		ATTACK_JC,
		ATTACK_LA,
		ATTACK_LB,
		ATTACK_LC,
		ATTACK_ARTS00,
		ATTACK_ARTS01,
		ATTACK_ARTS02,
		ATTACK_SHOT,
		FORCE_NA,
		FORCE_NB,
		FORCE_NC,
		FORCE_LA,
		FORCE_LB,
		FORCE_LC,
		FORCE_JA,
		FORCE_JB,
		FORCE_JC,
		FORCE_ARTS00X,
		FORCE_ARTS01X,
		FORCE_ARTS00Y,
		FORCE_ARTS01Y,
		KOCKBACK_NA,
		KOCKBACK_NB,
		KOCKBACK_NC,
		KOCKBACK_LA,
		KOCKBACK_LB,
		KOCKBACK_LC,
		KOCKBACK_JA,
		KOCKBACK_JB,
		KOCKBACK_JC,
		RIGID_NA,
		RIGID_NB,
		RIGID_NC,
		RIGID_JA,
		RIGID_JB,
		RIGID_JC,
		RIGID_LA,
		RIGID_LB,
		RIGID_LC,
		NUM,
	};

};




//コマンドデータ
namespace COM_DATA {
	//コマンドデータ
	static  COMMAND_DATA comDataRun[] =
	{
		{ Input::PAD_RIGHT,20 },
		{ Input::PAD_RIGHT,0 },
	};
	static  COMMAND_DATA comDataBackStep[] =
	{
		{ Input::PAD_LEFT,20 },
		{ Input::PAD_LEFT,0 },
	};
	static  COMMAND_DATA comDataRunUP[] =
	{
		{ Input::PAD_RIGHT|Input::PAD_UP,20 },
		{ Input::PAD_RIGHT,0 },
	};
	static  COMMAND_DATA comDataBackStepUP[] =
	{
		{ Input::PAD_LEFT|Input::PAD_UP,20 },
		{ Input::PAD_LEFT,0 },
	};


	static  COMMAND_DATA comData236A[] =
	{
		{ Input::PAD_DOWN,20 },
		{ Input::PAD_RIGHT | Input::PAD_DOWN,20 },
		{ Input::PAD_RIGHT,20 },
		{ Input::PAD_TRG1,0 },
	};
	static  COMMAND_DATA comData236B[] =
	{
		{ Input::PAD_DOWN,20 },
		{ Input::PAD_RIGHT | Input::PAD_DOWN,20 },
		{ Input::PAD_RIGHT,20 },
		{ Input::PAD_TRG2,0 },
	};
	static  COMMAND_DATA comData236C[] =
	{
		{ Input::PAD_DOWN,20 },
		{ Input::PAD_RIGHT | Input::PAD_DOWN,20 },
		{ Input::PAD_RIGHT,20 },
		{ Input::PAD_TRG3,0 },
	};
	static  COMMAND_DATA comData214A[] =
	{
		{ Input::PAD_DOWN,20 },
		{ Input::PAD_LEFT | Input::PAD_DOWN,20 },
		{ Input::PAD_LEFT,20 },
		{ Input::PAD_TRG1,0 },
	};
	static  COMMAND_DATA comData214B[] =
	{
		{ Input::PAD_DOWN,20 },
		{ Input::PAD_LEFT | Input::PAD_DOWN,20 },
		{ Input::PAD_LEFT,20 },
		{ Input::PAD_TRG2,0 },
	};

	static  COMMAND_DATA comData214C[] =
	{
		{ Input::PAD_DOWN,20 },
		{ Input::PAD_LEFT | Input::PAD_DOWN,20 },
		{ Input::PAD_LEFT,20 },
		{ Input::PAD_TRG3,0 },
	};


	static  COMMAND_DATA comData623C[] =
	{
		{ Input::PAD_RIGHT,20 },
		{ Input::PAD_DOWN,20 },
		{ Input::PAD_RIGHT | Input::PAD_DOWN,20 },
		{ Input::PAD_TRG3,0 },
	};
	static  COMMAND_DATA comData623C2[] =
	{
		{ Input::PAD_RIGHT | Input::PAD_DOWN,20 },
		{ Input::PAD_DOWN,20 },
		{ Input::PAD_RIGHT | Input::PAD_DOWN,20 },
		{ Input::PAD_TRG3,0 },
	};

	
}




//当たり判定用構造体
struct JudgeRect
{
	int motionName;

	VECTOR2 position;

	VECTOR2 size;

	VECTOR4 color;

	int type;

	int start;	//発生フレーム

	int keep;	//持続フレーム
};

//キャラクター番号
enum ChNum
{
	CH_FEMALE,
	CH_MALE,
	CH_HEAVY,
	CH_RANDOM,

};
//当たり判定の種類
enum RectType
{
	TYPE_HURT,
	TYPE_HIT,
	TYPE_PUSH,

};
//プレイヤーの現在のモーション番号
struct  PLAYER_MOTION {

	enum Type {
		NEUTRAL,
		WALKFRONT,
		WALKBACK,
		JUMP,
		LOW_NEUTRAL,
		STAND_ATTACK_A,
		STAND_ATTACK_B,
		STAND_ATTACK_C,
		JUMP_ATTACK_A,
		JUMP_ATTACK_B,
		JUMP_ATTACK_C,
		LOW_ATTACK_A,
		LOW_ATTACK_B,
		LOW_ATTACK_C,
		RUN,
		AIR_RUN,
		BACK_STEP,
		DAMAGE,
		DAMAGE_AIR,
		DOWN,
		DEATH,
		THROW,
		THROWED,
		GUARD,
		ARTS00,
		ARTS01,
		ARTS02,
		ARTS03,
		WIN,
		COMBO_AIR,
		RECOVER_BACK,
		RECOVER_FORWARD,
		RECOVER_AIR,
		APPEAL,
		AIR_RUN_BACK,
		AIR_DOWN,
		JUMP_INIT,
		JUMP_END,
		LOW_GUARD,
		LOW_INIT,
		DAMAGE_LOW,
		ATEMI,
		STAND_ATTACK_EX,
		ROLLING,




		NUM,
		END = -1
	};
};
//プレイヤー行動分岐ステート
struct PLAYER_MOVE
{
	enum Type
	{
		//移動関係
		NEUTRAL = 0,
		WALKFRONT,
		WALKBACK,
		JUMP,
		RUN,
		RUNAIR,
		BACKSTEP,
		//攻撃関係
		ATTACK_NA,
		ATTACK_LA,
		ATTACK_JA,
		ATTACK_NB,
		ATTACK_LB,
		ATTACK_JB,
		ATTACK_NC,
		ATTACK_LC,
		ATTACK_JC,
		ATTACK_THROW,
		THROWED,
		//状態
		SIT,
		DAMAGE,
		DAMAGE_AIR,

		DEATH,
		GUARD,
		//コマンド必殺技
		ARTS00A,
		ARTS01A,
		ARTS02A,
		ARTS00B,
		ARTS01B,
		ARTS02B,
		ARTS00C,
		ARTS01C,
		ARTS02C,
		WIN,
		COMBO_AIR,
		RECOVER_BACK,
		RECOVER_FORWARD,
		RECOVER_AIR,
		BACK_DASH,
		CANCEL,
		ATEMI_READY,
		ATEMI,
		STAND_ATTACK_EX,
		ROLLING,
		RIGID,
		DOWN,
		NUM,


	};

};


struct PLAYER_CANCEL
{
	enum Type
	{
		NONE = 0,
		ATTACK_NA,
		ATTACK_LA,
		ATTACK_JA,
		ATTACK_NB,
		ATTACK_LB,
		ATTACK_JB,
		ATTACK_NC,
		ATTACK_LC,
		ATTACK_JC,

	};

};

//攻撃の種類
struct PLAYER_ATTACK
{
	enum Type
	{

		NA,
		LA,
		JA,
		NB,
		LB,
		JB,
		NC,
		LC,
		JC,
		THROW,

		//コマンド必殺技
		ARTS00,
		ARTS01,
		ARTS02,

	};

};

//CPUが判断するための情報
//プレイヤーが何してるか
struct PLAYER_STATE
{
	enum Type
	{
		ATTACK,
		GUARD,
		DAMAGE,
		JUMP,
		NOT_MOVE,
	};
};
struct PLAYER_ELEMENT
{
	enum Type
	{
		NONE,
		FIRE,
		WATER,
		NATURE,
	};
};

//ジャンプ遷移
struct PLAYER_JUMP
{
	enum Type
	{
		INIT,
		FALL,
	};
};

//攻撃属性(上段、下段、中断、投げ
struct ATTACK_TYPE
{
	enum Type
	{
		ALL,
		LOW,
		UP,
		THROW
	};
};

//ガード属性(上段、下段、中断、投げ
struct GRD_TYPE
{
	enum Type
	{
		ALL,
		LOW,
		UP,
		THROW
	};
};



//**************************************
//　モーションデータ
//**************************************


//モーション用構造体
struct MotionData
{
	int motionName;
	const char* fileName;
};

//モーションデータ
static MotionData motionDataChara00[] =
{
	{ PLAYER_MOTION::NEUTRAL,			"Assets/chara00/ruria_taiki.fbx" },
	{ PLAYER_MOTION::WALKFRONT,			"Assets/chara00/ruria_aruki.fbx" },
	{ PLAYER_MOTION::WALKBACK,			"Assets/chara00/ruria_usiroaruki.fbx" },
	{ PLAYER_MOTION::JUMP,				"Assets/chara00/ruria_kuutyuutaiki.fbx" },
	{ PLAYER_MOTION::LOW_NEUTRAL,		"Assets/chara00/ruria_syagami_taiki.fbx" },
	{ PLAYER_MOTION::STAND_ATTACK_A,	"Assets/chara00/ruria_X.fbx" },
	{ PLAYER_MOTION::STAND_ATTACK_B,	"Assets/chara00/ruria_Y.fbx" },
	{ PLAYER_MOTION::STAND_ATTACK_C,	"Assets/chara00/ruria_k_B.fbx" },
	{ PLAYER_MOTION::JUMP_ATTACK_A,		"Assets/chara00/ruria_k_X.fbx" },
	{ PLAYER_MOTION::JUMP_ATTACK_B,		"Assets/chara00/ruria_k_Y.fbx" },
	{ PLAYER_MOTION::JUMP_ATTACK_C,		"Assets/chara00/ruria_k_B.fbx" },
	{ PLAYER_MOTION::LOW_ATTACK_A,		"Assets/chara00/ruria_s_X.fbx" },
	{ PLAYER_MOTION::LOW_ATTACK_B,		"Assets/chara00/ruria_s_Y.fbx" },
	{ PLAYER_MOTION::LOW_ATTACK_C,		"Assets/chara00/ruria_s_B.fbx" },
	{ PLAYER_MOTION::RUN,				"Assets/chara00/ruria_dash.fbx" },
	{ PLAYER_MOTION::BACK_STEP,			"Assets/chara00/ruria_backstep.fbx" },
	{ PLAYER_MOTION::AIR_RUN,			"Assets/chara00/ruria_k_step.fbx" },
	{ PLAYER_MOTION::DAMAGE,			"Assets/chara00/ruria_yarare.fbx" },
	{ PLAYER_MOTION::DOWN,				"Assets/chara00/ruria_down.fbx" },
	{ PLAYER_MOTION::DAMAGE_AIR,		"Assets/chara00/ruria_k_yarare.fbx" },
	{ PLAYER_MOTION::GUARD,				"Assets/chara00/ruria_guard.fbx" },

	{ PLAYER_MOTION::ARTS00,			"Assets/chara00/ruria_h_2.fbx" },
	{ PLAYER_MOTION::ARTS01,			"Assets/chara00/ruria_h_2.fbx" },
	{ PLAYER_MOTION::ARTS02,			"Assets/chara00/ruria_h_1.fbx" },
	{ PLAYER_MOTION::DEATH,				"Assets/chara00/ruria_taiki.fbx" },
	{ PLAYER_MOTION::THROW,				"Assets/chara00/ruria_taiki.fbx" },
	{ PLAYER_MOTION::THROWED,			"Assets/chara00/ruria_taiki.fbx" },
	{ PLAYER_MOTION::WIN,				"Assets/chara00/ruria_taiki.fbx" },
	{ PLAYER_MOTION::COMBO_AIR,			"Assets/chara00/ruria_k_yarare.fbx" },
	{ PLAYER_MOTION::RECOVER_BACK,		"Assets/chara00/ruria_k_hukki.fbx" },
	{ PLAYER_MOTION::RECOVER_FORWARD,	"Assets/chara00/ruria_k_hukki.fbx" },
	{ PLAYER_MOTION::RECOVER_AIR,		"Assets/chara00/ruria_k_hukki.fbx" },
	{ PLAYER_MOTION::APPEAL,			"Assets/chara00/ruria_appeal.fbx" },
	{ PLAYER_MOTION::AIR_RUN_BACK,		"Assets/chara00/ruria_k_backstep.fbx" },
	{ PLAYER_MOTION::AIR_DOWN,			"Assets/chara00/ruria_k_down.fbx" },
	{ PLAYER_MOTION::JUMP_INIT,			"Assets/chara00/ruria_jump.fbx" },
	{ PLAYER_MOTION::JUMP_END,			"Assets/chara00/ruria_kuutyuutaiki.fbx" },
	{ PLAYER_MOTION::LOW_GUARD,			"Assets/chara00/ruria_s_guard.fbx" },
	{ PLAYER_MOTION::LOW_INIT,			"Assets/chara00/ruria_syagami.fbx" },
	{ PLAYER_MOTION::DAMAGE_LOW,		"Assets/chara00/ruria_s_yarare.fbx" },
	{ PLAYER_MOTION::ATEMI,				"Assets/chara00/ruria_Y.fbx" },
	{ PLAYER_MOTION::STAND_ATTACK_EX,	"Assets/chara00/ruria_B.fbx" },
	{ PLAYER_MOTION::ROLLING,			"Assets/chara00/ruria_taiki.fbx" },
	{ PLAYER_MOTION::END,				"" },
};



//モーションデータ
static MotionData motionDataChara01[] =
{
	{ PLAYER_MOTION::NEUTRAL,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::WALKFRONT,			"Assets/chara01/aruki.fbx"},
	{ PLAYER_MOTION::WALKBACK,			"Assets/chara01/ushiroaruki.fbx"},
	{ PLAYER_MOTION::JUMP,				"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::LOW_NEUTRAL,		"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::STAND_ATTACK_A,	"Assets/chara01/X_5LP.fbx"},
	{ PLAYER_MOTION::STAND_ATTACK_B,	"Assets/chara01/Y_5MP.fbx"},
	{ PLAYER_MOTION::STAND_ATTACK_C,	"Assets/chara01/B_5HK.fbx"},
	{ PLAYER_MOTION::JUMP_ATTACK_A,		"Assets/chara01/k_X_JLP.fbx"},
	{ PLAYER_MOTION::JUMP_ATTACK_B,		"Assets/chara01/k_Y_JMK.fbx"},
	{ PLAYER_MOTION::JUMP_ATTACK_C,		"Assets/chara01/k_B_JHK.fbx"},
	{ PLAYER_MOTION::LOW_ATTACK_A,		"Assets/chara01/s_X_2LP.fbx"},
	{ PLAYER_MOTION::LOW_ATTACK_B,		"Assets/chara01/s_Y_2MK.fbx"},
	{ PLAYER_MOTION::LOW_ATTACK_C,		"Assets/chara01/s_B_2HP.fbx"},
	{ PLAYER_MOTION::RUN,				"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::BACK_STEP,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::AIR_RUN,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::DAMAGE,			"Assets/chara01/yarare.fbx"},
	{ PLAYER_MOTION::DOWN,				"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::DAMAGE_AIR,		"Assets/chara01/k_yarare.fbx"},
	{ PLAYER_MOTION::GUARD,				"Assets/chara01/guard.fbx"},
	{ PLAYER_MOTION::ARTS00,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::ARTS01,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::ARTS02,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::DEATH,				"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::THROW,				"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::THROWED,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::WIN,				"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::COMBO_AIR,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::RECOVER_BACK,		"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::RECOVER_FORWARD,	"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::RECOVER_AIR,		"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::APPEAL,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::AIR_RUN_BACK,		"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::AIR_DOWN,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::JUMP_INIT,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::JUMP_END,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::LOW_GUARD,			"Assets/chara01/s_guard.fbx"},
	{ PLAYER_MOTION::LOW_INIT,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::DAMAGE_LOW,		"Assets/chara01/s_yarare.fbx"},
	{ PLAYER_MOTION::ATEMI,				"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::STAND_ATTACK_EX,	"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::ROLLING,			"Assets/chara01/stand.fbx"},
	{ PLAYER_MOTION::END,				"" },
};
