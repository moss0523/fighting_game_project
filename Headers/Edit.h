#pragma once

#include "../GameLib/Headers/Obj2D.h"

#define PARAM_MAX 1024





//***************************************
//
//		パラメータ編集ツールクラス
//
//***************************************

//オブジェクトの種類
enum
{
	EDIT_TYPE_NONE = 0,
	EDIT_TYPE_RECT,
	EDIT_TYPE_UI,
	EDIT_TYPE_CONTROL,

};

struct EDIT_MODE {
	enum type
	{
		ADD = 0,
		DEL,	
	};
};

class Edit : public Obj2DManager, public Singleton<Edit>
{
public:
	void Init();    // 初期化
	void Update();  // 更新
	void Render();  // 描画

	bool isMove;
	bool isAdd;


	int state;

	//移動関数用インナークラス
	class Move {

	public:

		static void HitButton(Obj2D*);		//攻撃判定生成ボタン
		static void HurtButton(Obj2D*);		//ダメージ判定生成ボタン
		static void HitRectNew(Obj2D*);		//攻撃判定
		static void HurtRectNew(Obj2D*);	//ダメージ判定
		static void SizeControler(Obj2D*);	//大きさコントローラー(全体
		static void SizeControlerW(Obj2D*);	//大きさコントローラー(横軸
		static void SizeControlerH(Obj2D*);	//大きさコントローラー(縦軸
		static void PosControler(Obj2D*);	//座標コントローラー(全体
		static void PosControlerX(Obj2D*);	//座標コントローラー(横軸
		static void PosControlerY(Obj2D*);	//座標コントローラー(縦軸


	};


private:

	void ImGuiToolUpdate();//ImGui関係更新
	void ParamToolMove();//パラメータ管理ツール
	void ParamToolWindow();//パラメータ管理ウインドウ
	void RectToolMove();//矩形管理ツール

	void SaveAdd();						//追加した当たり判定保存
	void ControlerAllClear();			//コントローラー全削除
	void SetPosControler(Obj2D * obj);	//位置調整コントローラーをセットする
	void SetSizeControler(Obj2D * obj); //大きさ調整コントローラーをセットする



	//矩形保存(追加)
	int GetSize() { return PARAM_MAX; }


};

#define edit	(Edit::GetInstance())

