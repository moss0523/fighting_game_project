#pragma once
#include "../GameLib/Headers/Obj2D.h"
#include "../GameLib/Headers/BlendState.h"

#define EFC_MAX 256




//***************************************
//
//	スプライトエフェクトクラス
//
//***************************************

//種類
struct EFFECT_TYPE {
	enum type
	{
		HIT = 0,
		SHOT,
	};
};
class EffectManager :public Obj2DManager, public Singleton<EffectManager>
{

public:


	class Move
	{
	public:
		static void Hit(Obj2D*obj);//ヒットエフェクト生成
		static void HitPow(Obj2D*obj);//ヒットエフェクト生成
		static void Storm(Obj2D*obj);//竜巻(飛び道具
		static void Cancel(Obj2D*obj);//竜巻(飛び道具
		static void PowerUp(Obj2D*obj);//竜巻(飛び道具


	};
	int GetSize() { return EFC_MAX; }
	void Render(BlendState blend);

};

#define effectManager (EffectManager::GetInstance())
