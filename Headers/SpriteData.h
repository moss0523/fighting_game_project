#include "../GameLib/Headers/Sprite.h"

//********************************************************
//
//  画像データ、2Dアニメデータ置き場
//
//********************************************************



extern SpriteData sprLife;
extern SpriteData sprLifeFrame;
extern SpriteData sprArts;
extern SpriteData sprArtsFrame;

extern SpriteData sprBottunHit;
extern SpriteData sprBottunHurt;
extern SpriteData sprAllControler;
extern SpriteData sprWControler;
extern SpriteData sprHControler;
extern SpriteData sprXControler;
extern SpriteData sprYControler;
extern SpriteData sprRoundCallRound1;
extern SpriteData sprRoundCallRound2;
extern SpriteData sprRoundCallRound3;
extern SpriteData sprRoundCallFight;
extern SpriteData sprRoundCallWin;
extern SpriteData sprRoundCallPlayer1;
extern SpriteData sprRoundCallPlayer2;
extern SpriteData sprWinTag;
extern SpriteData sprCounter;
extern SpriteData sprModeTraning;
extern SpriteData sprModeVersus;
extern SpriteData sprModeArcade;
extern SpriteData sprModeCharange;
extern SpriteData sprModeOption;
extern SpriteData sprSelectCur;
extern SpriteData sprChSelect;
extern SpriteData sprGameFont00;
extern SpriteData sprGameFont01;
extern SpriteData sprGameFont02;
extern SpriteData sprGameFont03;
extern SpriteData sprGameFont04;
extern SpriteData sprGameFont05;
extern SpriteData sprGameFont06;
extern SpriteData sprGameFont07;
extern SpriteData sprGameFont08;
extern SpriteData sprGameFont09;
extern SpriteData sprGameFontCombo;

extern SpriteData sprKeyX;
extern SpriteData sprKeyY;
extern SpriteData sprKeyB;
extern SpriteData sprKeyA;
extern SpriteData sprKeyR;
extern SpriteData sprKeyL;
extern SpriteData sprKeyJuji;
extern SpriteData sprArrowKeyL;
extern SpriteData sprArrowKeyLU;
extern SpriteData sprArrowKeyU;
extern SpriteData sprArrowKeyRU;
extern SpriteData sprArrowKeyR;
extern SpriteData sprArrowKeyRD;
extern SpriteData sprArrowKeyD;
extern SpriteData sprArrowKeyLD;
extern SpriteData sprBlackOut;



extern SpriteData* sprGameFontNum[];
extern AnimeData AnimeUILoading[];
extern AnimeData AnimeHitEffect[];
extern AnimeData AnimeWind[];
extern AnimeData AnimeCanel[];
extern AnimeData AnimeFrame[];
//バトル中画像ロード
void SpriteLoadBattle();
//バトル中画像解放
void SpriteUnLoadBattle();
//タイトル画像ロード
void SpriteLoadTitle();
//タイトル画像解放
void SpriteUnLoadTitle();
//メニュー画像読み込み
void SpriteLoadMenu();
////メニュー画像解放
void SpriteUnLoadMenu();
//キャラセレクト画像読み込み
void SpriteLoadSelect();
//キャラセレクト画像解放
void SpriteUnLoadSelect();
//ロード中画像読み込み
void SpriteLoadLoading();
//ロード中画像解放
void SpriteUnLoadLoading();
