#pragma once
#include "../GameLib/Headers/Template.h"
#include "../GameLib/Headers/RenderTarget.h"
#include "../GameLib/Headers/BlendState.h"
#include "../Headers/Scene.h"



//***************************************************
//	
//			モード選択シーンクラス
//
//***************************************************



class SceneMenu :public Scene,public Singleton<SceneMenu>
{
private:

	RenderTarget* screen;//スクリーン用実体(ポストエフェクト時の処理系に使用する)
	BlendState blend;//ブレンドモード用実体


	int state;
public:
	
	
	void Init();
	void UnInit();
	void Update();
	void Render();
};
#define sceneMenu SceneMenu::GetInstance()
