#pragma once

#include "../GameLib/Headers/Obj2D.h"

#define UI_MAX 50


class BattleUI : public Obj2DManager, public Singleton<BattleUI>
{
public:
	void Init();    // 初期化

	//移動関数インナークラス
	class Move {
	public:
		static void Frame(Obj2D*obj);
		static void Life(Obj2D*obj);
		static void Arts(Obj2D*obj);
		static void ArtsFrame(Obj2D*obj);
		static void WinnerCall(Obj2D * obj);
		static void RoundCall(Obj2D*obj);
		static void ComboCounter(Obj2D*obj);
		static void ComboCounter2(Obj2D*obj);
		static void ComboCounterFont(Obj2D*obj);
		static void KeyLogger(Obj2D*obj);

	};

private:
	int GetSize() { return UI_MAX; }


};

#define battleUI	(BattleUI::GetInstance())

