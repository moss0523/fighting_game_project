#pragma once
//インクルード
#include "../GameLib/Headers/FbxMesh.h"
#include "../GameLib/Headers/DXShader.h"
#include "../GameLib/Headers/Command.h"
#include "../GameLib/Headers/InputManager.h"
#include "../Headers/PlayerConstant.h"




static const float ARTS_MAX = 100;




//*********************************************************
//
//				プレイヤークラス
//
//*********************************************************
class Player
{

protected:

	//**************************************
	//	変数
	//**************************************
	//行動分岐用サブステート(移行フレームとか)
	int subState;
	//硬直フレーム
	float rigidFrame;
	//パッド情報(何Pか)
	int pad;
	//重力（固定
	const float GRAVITY = 3.f;
	//ステート内で経過フレームをカウントするときに使う
	int moveCnt = 0;
	int motionMoveCnt = 0;

	//空中移動回数
	int airMoveCnt;

	int frameCnt;

	//体力
	float hp;
	float artsPoint;
	//キー入力保存用変数
	int keyState;
	int keyTrg;
	bool isLSUP = false;
	bool isLSDOWN = false;
	bool isLSRIGHT = false;
	bool isLSLEFT = false;


	//ガード属性
	int grdType;
	//ヒットストップ
	float stopFrame;

	//モーション遅延フレーム
	float lateFrame;

	//コンボカウント
	int comboCnt;

	int winCnt = 0;

	//行動分岐用
	int moveState;

	//行動分岐用
	int stateBuffer;


	//方向情報(右向き+1,左向き-1)
	float dir;

	//キャラクタ情報
	int chNo;

	//パラメータ
	VECTOR2 speed;
	VECTOR2 speedTmp;

	VECTOR2 knockbackSpeed;

	VECTOR4 color;

	//プレイヤーの情報を保存しておく
	int playerState;

	//プレイヤーの情報を保存しておく
	int attackLast;

	//パラメータ
	VECTOR2 position;

	//１フレーム前の座標
	VECTOR2 oldPosition;

	//攻撃力
	float attack;

	//*****************************************
	//コマンド入力判定用実体
	//*****************************************


	//ダッシュ
	ComParse commandRun;
	ComParse commandBackStep;
	ComParse commandRunUP;
	ComParse commandBackStepUP;
	//必殺技
	ComParse command236A;
	ComParse command236B;
	ComParse command236C;

	ComParse command214A;
	ComParse command214B;
	ComParse command214C;
	ComParse command623C;
	ComParse command623C2;
	ComParse command623A;
	ComParse command623A2;
	ComParse command623B;
	ComParse command623B2;


	//シェーダー用実体(シェーダーの作成起動に使用)
	Shader* plShader;
	//シェーダー用実体(シェーダーの作成起動に使用)
	//輪郭線用
	Shader* plShaderOL;

	//エネミーへのポインタ
	Player* enemy;


	//********************************************
	//Player用データ
	//********************************************


	//自キャラクターのメッシュデータ
	FBXMesh* plMesh;

	//矩形の実体
	JudgeRect rectData[RECT_MAX];
	JudgeRect pushRectData;

	//キー入力用



	//**************************************
	//	フラグ
	//**************************************

	//空中フラグ
	bool isAir;
	//操作可能かどうかのフラグ
	bool isActive;
	//コマンド入力可能かどうか
	bool isCommand = true;
	//自分の攻撃が当たったかどうかのフラグ
	bool isHit;
	//自分が攻撃を食らっているかどうかのフラグ
	bool isDamage;
	//描画優先フラグ
	bool isPrioritize;
	//なげられフラグ
	bool isThrowed;
	//なげ中フラグ
	bool isThrow;
	//無敵フラグ
	bool isInvisible;
	//ダウンフラグ
	bool isDown;
	//自分がガードしているかどうかのフラグ
	bool isGuard;
	//押してるとき
	bool isPush;
	//押されている
	bool isPushed;
	//デバッグ矩形を描画
	bool isRectRender;
	//壁フラグ
	bool isWall;
	//押し合い判定を無視(初期化忘れ厳禁
	bool isNotPush = false;
	//オート振り向きを無視(初期化忘れ厳禁
	bool isNotReturn = false;
	//CPUかどうか
	bool isCPU = false;
	//投げ相殺中か
	bool isThrowComp = false;

	//当身発生よし!
	bool isAtemi = false;

	//当身取ったフラグ
	bool isGetAttack = false;

	//片方だけ止めるとき
	bool isStop = false;

	//キャンセル可能か
	bool isCancel = false;

	//キャンセル入力受付済み
	bool isCancelStandBy = false;
	//前転入力受付済み
	bool isRollingSB = false;
	//パワーアップ
	bool isPowerUp = false;

	//暗転
	bool isDark = false;

	//しゃがみ
	bool isLow = false;

public:

	float commonParam[256];



	//プレイヤー初期化
	void Init(int pad);
	//パラメータ初期化
	void InitParameter(int pad);
	//プレイヤー更新
	void Update();
	//プレイヤー描画
	void Render(ID3D11Buffer* ConstantBuffer);


	//CPU思考---------------------------------------------
	void CPUPerce(int& keyTrg, int& keyState);
	//CPU入力
	void CPUInput(int& keyTrg, int& keyState);
	//----------------------------------------------------

	//ダメージ行動分岐処理予約(judgeで呼ぶ
	void DamageReserve(bool isShot = false);

	//ガード行動分岐処理予約(judgeで呼ぶ
	void GuardReserve(bool isShot = false);

	//投げ相殺
	void MoveThrowComp();


	//**************************************
	//	変数ゲッター
	//**************************************
	Player*		GetEnemy() { return enemy; }
	int			GetPad() { return pad; }
	int			GetMotionName() { return plMesh->motionName; }
	float		GetHp() { return hp; }
	float		GetArtsPoint() { return artsPoint; }

	int			GetKeyState() { return keyState; }
	int			GetKeyTrg() { return keyTrg; }
	JudgeRect	GetRectData(int index) { return rectData[index]; }
	JudgeRect	GetPushRectData() { return pushRectData; }
	FBXMesh*	GetMesh() { return plMesh; }
	int			GetSubState() { return subState; }
	float		GetRigidFrame() { return rigidFrame; }
	int			GetMoveCnt() { return moveCnt; }
	int			GetMotionMoveCnt() { return motionMoveCnt; }
	int			GetGrdType() { return grdType; }
	float		GetStopFrame() { return stopFrame; }
	int			GetComboCnt() { return comboCnt; }

	float		GetDir() { return dir; }
	int			GetChNo() { return chNo; }
	VECTOR2		GetSpeed() { return speed; }
	VECTOR4		GetColor() { return color; }
	int			GetPlayerState() { return playerState; }
	int			GetAttackLast() { return attackLast; }
	VECTOR2		GetPosition() { return position; }
	VECTOR2		GetOldPosition() { return oldPosition; }

	int			GetMoveState() { return moveState; }
	float		GetFrameCnt() { return plMesh->nowFrame; }
	int			GetWinCnt() { return winCnt; }
	float		GetAttack() { return attack; }
	//**************************************
	//	フラグゲッター
	//**************************************
	bool		GetIsHit() { return isHit; }
	bool		GetIsDamage() { return isDamage; }
	bool		GetIsPrioritize() { return isPrioritize; }
	bool		GetIsThrowed() { return isThrowed; }
	bool		GetIsThrow() { return isThrow; }
	bool		GetIsInvisible() { return isInvisible; }
	bool		GetIsDown() { return isDown; }
	bool		GetIsGuard() { return isGuard; }
	bool		GetIsPush() { return isPush; }
	bool		GetIsPushed() { return isPushed; }
	bool		GetIsRectRender() { return isRectRender; }
	bool		GetIsNotPush() { return isNotPush; }
	bool		GetIsWall() { return isWall; }
	bool		GetIsNotReturn() { return isNotReturn; }
	bool		GetIsCPU() { return isCPU; }
	bool		GetIsAir() { return isAir; }
	bool		GetIsActive() { return isActive; }
	bool		GetIsThrowComp() { return isThrowComp; }
	bool		GetIsPowerUp() { return isPowerUp; }
	bool		GetIsDark() { return isDark; }

	//**************************************
	//	変数セッター
	//**************************************
	void		SetRigidFrame(float rigidFrame) { this->rigidFrame = rigidFrame; }
	void		SetEnemy(Player* enemy) { this->enemy = enemy; }
	void		SetMoveState(int state) { moveState = state; }
	void		Set2PColor() { color.x *= 0.6f; color.y *= 0.6f; color.z *= 0.6f; }
	void		SetSpeed(VECTOR2 spd) { speed = spd; }
	void		SetChNo(int num) { chNo = num; }
	void		SetComboCnt(int num) { comboCnt = num; }
	void		SetArtsPoint(float num) { artsPoint = num; }

	//**************************************
	//	フラグセッター
	//**************************************
	void		SetIsHit(bool tOF) { isHit = tOF; }
	void		SetIsDamage(bool tOF) { isDamage = tOF; }
	void		SetIsPrioritize(bool tOF) { isPrioritize = tOF; }
	void		SetIsThrowed(bool tOF) { isThrowed = tOF; }
	void		SetIsThrow(bool tOF) { isThrow = tOF; }
	void		SetIsInvisible(bool tOF) { isInvisible = tOF; }
	void		SetIsDown(bool tOF) { isDown = tOF; }
	void		SetIsGuard(bool tOF) { isGuard = tOF; }
	void		SetIsPush(bool tOF) { isPush = tOF; }
	void		SetIsPushed(bool tOF) { isPushed = tOF; }
	void		SetIsRectRender(bool tOF) { isRectRender = tOF; }
	void		SetIsNotPush(bool tOF) { isNotPush = tOF; }
	void		SetIsWall(bool tOF) { isWall = tOF; }
	void		SetIsNotReturn(bool tOF) { isNotReturn = tOF; }
	void		SetIsCPU(bool tOF) { isCPU = tOF; }
	void		SetIsAir(bool tOF) { isAir = tOF; }
	void		SetIsActive(bool tOF) { isActive = tOF; }
	void		SetIsCancel(bool tOF) { isCancel = tOF; }

	void		SetIsThrowComp(bool toF) { isThrowComp = toF; }

	//**************************************
	//　加算とか
	//**************************************
	void		AddWinCnt() { ++winCnt; }
	void		AddComboCnt() { ++comboCnt; }

	Input::KeyStackData  keyData[KEEP_MAX] = {};


	//デストラクタ
	~Player()
	{
		if (plShaderOL)delete plShaderOL;
		if (plShader)delete plShader;
		if (plMesh)delete plMesh;
	}

	//トリガー入力判定(先行入力あり
	bool IsTRG(int keyBit, int keyTrg);
	bool IsTRGSame(int keyBit1, int keyBit2, int keyTrg);




	//プライベートメソッド
private:

	//一括モーション追加
	void AddMotionAll();

	//範囲制限
	void ScreenLimit();


	//ジャンプキャンセル
	void JumpCancel(int keyState);
	//行動分岐
	void PlyerMove(int keyTrg, int keyState);

	//入力判定
	void PlyerInput(int keyTrg, int keyState);
	//コマンド受付
	void PlyerCommand(int keyTrg, int keyState);



	//キャンセルステートマシン
	void CancelMove(int keyTrg);
	//ダメージ行動決定
	void PlayDamage();
	//ガード決定
	void PlayGuard();
	//方向決定
	void DecideDirection();
	//ニュートラルに戻る(補間ありかなしか引数で
	void ReturnNeutral(bool isBlend = false);
	//空中に戻る
	void ReturnAir();
	//受け身取る
	void ReturnRecover();

	//硬直する(モーションフレーム以外で管理する用)
	void ReturnRigit(float rigidFrame);



	//当たり判定矩形描画
	void RectRender();

	//慣性減衰
	void SpeedDown();
	//押し合い
	void PushMove();

	//敵に吹っ飛ばされる値を設定する（基本的にYだけ
	void SetForceFromEnemy(float forceY, float forceX = 0);


	//ノックバック速度セット(プレイヤーが壁側の時は相手にセットされる
	void SetNockBackSpeed(float knockSpeed) {
		if (!isWall)
			knockbackSpeed.x = knockSpeed;
		else
			enemy->knockbackSpeed.x = knockSpeed;

	}

	//3Dモデル更新
	void PlMeshUpdate();


	void ArtsAdd(float increaseVal);


	int cancelState = 0;

	void CancelStateMove();


	void CreateKeyLogger();



};


