
#pragma once
#include"../GameLib/Headers/Template.h"
#include"../GameLib/Headers/Obj2D.h"

#include"../GameLib/Math/Vector.h"
#include"Player.h"

//**********************************************
//
// Judgeクラス(当たり判定関係の処理、行動予約を行う
//
//**********************************************
class Judge : public Singleton<Judge>
{


public:

	//一括判定
	void Execute();
	//一括無視
	bool IsIgnore(Player*player, int num, const int RECT_TYPE);

private:

	//それぞれの判定を行う関数
	void JudgeHit(Player*attackPl, Player*damagePl);
	void JudgeShot(Player*attackPl,Player*damagePl);
	void JudgeThrow(Player*attackPl, Player*damagePl);
	void JudgePush(Player*attackPl, Player*damagePl);
	bool JudgeGuard(Player * pl);

	void CreateHitEffect(Player*attackPl, Player*damagePl,int damageNum);


	//当たってるかどうか(プレイヤーvsプレイヤー)
	bool IsHitPlayer(
		JudgeRect rectA, Player* parentA,
		JudgeRect rectD, Player* parentD);
	bool IsHitPlayerPush(
		JudgeRect rectA, Player* parentA,
		JudgeRect rectD, Player* parentD);

	//当たってるかどうか(プレイヤーvs弾とか)
	bool IsHit2D(
		Obj2D*obj,
		JudgeRect rectD, Player* parentD);
	//無敵などフラグ判定
	bool IsNoHit(Player*attackPl, Player*damagePl);
};

// インスタンス取得マクロ
#define judge		(Judge::GetInstance())



