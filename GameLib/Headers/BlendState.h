#pragma once
//--------------------------------------------------------------------
//ヘッダーファイルのインクルード
//--------------------------------------------------------------------

#include <d3d11.h>


class BlendState
{


public:
	enum {
		BS_NONE,
		BS_ALPHA,
		BS_ADD,
		BS_PMA,
		BS_NUM,
	};

	static const int BLEND_TYPE = BS_NUM;
	static ID3D11BlendState* blendStateInst[BLEND_TYPE];//追加



	//ブレンドステートの作成
	static HRESULT Initialize();

	//ブレンドステートの設定
	static void Set(int state);

};