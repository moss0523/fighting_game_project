#pragma once

//*********************************
//　　　コマンド入力
//*********************************


//コマンドデータ
struct COMMAND_DATA
{
	int	key;		// コマンドキービット
	int	interval;	// 猶予フレーム
};

//コマンド解析クラス
class ComParse
{
private:
	COMMAND_DATA* start;	//開始コマンド
	COMMAND_DATA* current;  //現在のコマンド
	int			  interval; //猶予フレーム
public:
	void Init(COMMAND_DATA* data);//コマンド初期化
	bool Parse(int key);		  //コマンド比較
};

