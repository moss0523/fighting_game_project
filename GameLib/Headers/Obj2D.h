#pragma once
#include<vector>
#include"Sprite.h"
#include"DXShader.h"
#include"Template.h"
#include"../Headers/Player.h"


class Obj2D;// 前方宣言

typedef	void(*Mover)(Obj2D* obj);		// 関数ポインタ（更新処理）
typedef bool(*Eraser)(Obj2D* obj);		// 関数ポインタ（消去チェック）

class Obj2D
{

private:

public:
	//************************************************************
	//変数
	//************************************************************

	SpriteData*data;		//画像情報
	Anime  anime;			//アニメーション用実体
	VECTOR2 position;		//座標
	VECTOR2 initPos;		//初期座標保存用
	VECTOR2 scale;			//大きさ
	VECTOR2 speed;			//移動スピード
	VECTOR2 center;			//中心座標	
	VECTOR2 size;			//当たり判定の大きさ
	VECTOR4 color;			//色
	bool isErase;			//消すか否か
	bool isSelect;			//選ばれているか
	bool isScroll;			//スクロールするか
	bool isFront;			//プレイヤーの前後どちらに描画するか
	Obj2D *parent2D;		//親子関係(2Dオブジェクト
	Player*parent3D;		//親子関係(プレイヤー
	int timer;				//汎用タイマー
	int state;				//移動関数用ステート
	int index;				//複数生成した時の番号
	int type;				//属性(当たり判定があるのかないのかとか
	float moveAngle;		//移動するときの角度
	int startFrame;			//当たり判定開始フレーム
	int keepFrame;			//当たり判定持続フレーム
	bool isAdd;				//追加
	bool isRect;			//矩形描画するか否か
	Mover mover;			//更新処理のための関数ポインタ
	Eraser	eraser;         //消去処理のための関数ポインタ
	JudgeRect hitRect;		//当たり判定矩形
	int dir;				//方向
	float angle;

	//************************************************************
	//関数
	//************************************************************
	void Init();	//初期化
	void Update();	//更新
	void Render();	//描画
	void Clear();	//解放


	bool AnimeUpdate(AnimeData* animeData);	// アニメーションのアップデート




};



class Obj2DManager 
{

protected:
	//2Dobj情報を格納する動的配列
	std::vector<Obj2D>	objWork;


public:


	//生成
	void Init();
	//更新
	void Update();
	//描画
	void Render();
	//描画(プレイヤーの前後に出したいとき
	void Render(bool isFront);
	//obj生成
	Obj2D* Create(Mover mover, VECTOR2 pos);	

	//stdVector用begin end
	auto begin() { return objWork.begin(); }
	auto end() { return objWork.end(); }
private:

	virtual int GetSize() = 0;


};