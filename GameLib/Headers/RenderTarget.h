#pragma once
#include <d3d11.h>
#include "DXShader.h"



//*************************************
//
//レンダーターゲット関連クラス
//
//*************************************
class RenderTarget
{
public:
	RenderTarget() {}
	~RenderTarget()
	{
		if (postShader)delete postShader;
		if (depth)depth->Release();
		if (renderTargetView)renderTargetView->Release();
		if (depthStencilView)depthStencilView->Release();
		if (shaderResourceView)shaderResourceView->Release();
		if (vertexBuffer)vertexBuffer->Release();
		if (indexBuffer)indexBuffer->Release();
		if (samplerState)samplerState->Release();

	}
	void Initialize();
	void UnInit() {if (postShader)delete postShader;}
	void Activate();
	void ClearZBuffer();
	void Deactivate();
	void Render();
protected:
	Shader* postShader;//ポストエフェクト用
	ID3D11Texture2D* texture;
	ID3D11Texture2D *depth;
	ID3D11RenderTargetView* renderTargetView;
	ID3D11DepthStencilView* depthStencilView;
	ID3D11ShaderResourceView* shaderResourceView;

	struct Vertex {
		float x, y, z;		// 座標
		float nx, ny, nz;	// 法線
		float tu, tv;		// UV
		DWORD color;
	};
	ID3D11Buffer *vertexBuffer;
	ID3D11Buffer *indexBuffer;
	ID3D11SamplerState *samplerState;
};

