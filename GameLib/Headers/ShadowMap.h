#pragma once
#include <d3d11.h>
#include "DXShader.h"
#include "../Math/Matrix.h"
#include "../Math/Vector.h"

//***************************************
//
//		シャドウマップクラス
//
//***************************************

class ShadowMap
{
public:
	ShadowMap();
	~ShadowMap();


	//	シャドウマップ
protected:
	Shader shaderShadowMap;
	ID3D11Texture2D* shadowMapTex;
	ID3D11Texture2D *shadowMapDepth;
	ID3D11RenderTargetView* rTVShadowMap;
	ID3D11ShaderResourceView *sRVShadowMap;
	ID3D11DepthStencilView* dSVShadowMap;

	ID3D11Buffer* cBShadowMap;
	struct CBShadowMapParam
	{
		MATRIX shadowVP;
	};


public:
	//初期化
	void Initialize();

	//起動
	void Activate(
		const VECTOR3& center,
		const VECTOR3& lightVec
	);
	//再起動
	void Deactivate();
};


