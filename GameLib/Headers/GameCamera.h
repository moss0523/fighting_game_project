
#pragma once
//インクルード
#include"../Math/matrix.h"
#include"Template.h"
#include"DxSystem.h"

static const int SCROLL_MAX_X = DxSystem::SCREEN_WIDTH / 2 + 1000;
static const int SCROLL_MIN_X = DxSystem::SCREEN_WIDTH / 2 - 1000;

//***************************************************************
//
//カメラクラス
//
//***************************************************************


class GameCamera :public Singleton<GameCamera>
{

private:



	MATRIX WorldMATRIX;
	MATRIX ViewMATRIX;

	MATRIX ProjectionMATRIX;
	MATRIX ProjectionMATRIXOrtho;
	MATRIX ProjectionMATRIXBlend;


	int state;

public:

	float moveVal;
	void scroll2D();
	void scroll3D();

	void Init();
	void Update();
	VECTOR2 scroll;
	MATRIX GetWorldMATRIX() { return WorldMATRIX; }
	MATRIX GetViewMATRIX() { return ViewMATRIX; }


	MATRIX GetProjectionMATRIX() { return ProjectionMATRIX; }
	MATRIX GetProjectionMATRIXOrtho() { return ProjectionMATRIXOrtho; }
	MATRIX GetProjectionMATRIXBlend() { return ProjectionMATRIXBlend; }

	bool isScroll = false;
	VECTOR3 pos;
	VECTOR3 target;
	VECTOR3 up;


};

#define gameCamera GameCamera::GetInstance()