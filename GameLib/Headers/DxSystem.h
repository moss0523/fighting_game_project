#pragma once

#include <d3d11.h>



//****************************************************************
//
//DirectX関連処理をまとめたクラス(デバイスとコンテキストもここ)
//
//****************************************************************
class DxSystem {

private:


	static IDXGISwapChain*					swapChain;
	static ID3D11Texture2D*					depthStencilTexture;
	static ID3D11ShaderResourceView*		shaderResourceView;

	static HRESULT Createdevice(HWND hWnd);
	static bool CreateDepthStencil();
	static bool InitializeRenderTarget();

public:

	static ID3D11Device*				device;
	static ID3D11DeviceContext*			deviceContext;

	static int SCREEN_WIDTH;
	static int SCREEN_HEIGHT;
	static ID3D11RenderTargetView*		renderTargetView;
	static ID3D11DepthStencilView*		depthStencilView;
	static ID3D11DepthStencilState*		depthStencilState;

	static float elapsedTime;

	static bool Initialize(HWND hWnd, int width, int height);
	static void CullFront();
	static void CullBack();
	static void CullNone();

	static void Release();
	static void Clear();
	static void Flip();

};


