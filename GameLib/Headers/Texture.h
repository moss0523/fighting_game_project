#pragma once
#include "DxSystem.h"
#include "../../DirectXTex/DirectXTex.h"


//**************************************************
//
//テクスチャクラス
//
//************************************************
class Texture
{
private:
	ID3D11ShaderResourceView* ShaderResourceView;
	ID3D11SamplerState* SamplerState;

	int width;
	int height;

public:
	Texture();
	~Texture();

	bool Load(const char* filename);
	void Set(int slot = 0);

	int GetWidth() { return width; }
	int GetHeight() { return height; }



	
};

