#pragma once

#include <d3d11.h>
#include <d3dcompiler.h>


//****************************************************************
//
//シェーダークラス(fx)
//
//****************************************************************
class Shader
{
protected:
	ID3D11VertexShader*		vertexShader; // 頂点シェーダ
	ID3D11PixelShader*		pixelShader; // ピクセルシェーダ

	ID3D11InputLayout*		vertexLayout;

	HRESULT Shader::Compile(WCHAR* filename, LPCSTR method, LPCSTR shaderModel, ID3DBlob** ppBlobOut);

public:
	Shader() {}
	~Shader();

	bool Create(WCHAR* filename, LPCSTR vertexShaderName, LPCSTR pixelShaderName);

	void Activate();
};