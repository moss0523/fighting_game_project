#pragma once
#include <d3d11.h>
#include "../Headers/DXShader.h"
#include "../Math/Vector.h"
#include "../Headers/Template.h"


class RectRender: public Singleton<RectRender>
{
private:
	ID3D11Buffer* vertexBuffer = nullptr; // 頂点バッファ
	ID3D11DepthStencilState* depthStencilState;

public:

	RectRender();
	~RectRender();


	void Render(VECTOR2 position, VECTOR2 size, VECTOR4 color);


	Shader* shader;


	//----------------------------------------------
	//	頂点データ構造体定義
	//----------------------------------------------
	struct VERTEX
	{
		VECTOR3 position;	//位置
		VECTOR3 normal;//法線
		VECTOR2 texcoord;	//UV座標
		VECTOR4 color;	//頂点色
	};

};

#define rectRender RectRender::GetInstance()