#pragma once
//******************************************************************************
//
//
//      入力マネージャ
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include <Windows.h>
#include "template.h"
#include <Keyboard.h>
#include <GamePad.h>
#include <vector>

#include "../GameLib/Math/Vector.h"



namespace Input
{

	// キーラベル
	const int PAD_UP = (1 << 0);
	const int PAD_DOWN = (1 << 1);
	const int PAD_LEFT = (1 << 2);
	const int PAD_RIGHT = (1 << 3);
	const int PAD_START = (1 << 4);
	const int PAD_TRG1 = (1 << 5);
	const int PAD_TRG2 = (1 << 6);
	const int PAD_TRG3 = (1 << 7);

	const int PAD_SELECT = (1 << 8);
	const int PAD_TRG4 = (1 << 9);
	const int PAD_L1 = (1 << 10);
	const int PAD_R1 = (1 << 11);
	const int PAD_L2 = (1 << 12);
	const int PAD_R2 = (1 << 13);
	const int PAD_L3 = (1 << 14);
	const int PAD_R3 = (1 << 15);
	const int RIGHT_CLICK = (1 << 16);
	const int LEFT_CLICK = (1 << 17);
	const int CENTER_CLICK = (1 << 18);
	const int PAD_EDIT = (1 << 19);
	const int PAD_SAVE = (1 << 20);
	const int SHIFT_KEY = (1 << 21);
	const int PAD_INIT = (1 << 22);
	const int KEY_W = (1 << 23);
	const int KEY_R = (1 << 24);
	const int KEY_D = (1 << 25);
	const int KEY_S = (1 << 26);
	const int KEY_DELETE = (1 << 27);
	const int KEY_TAB = (1 << 28);
	const int ARTS_1 = (1 << 29);
	const int ARTS_2 = (1 << 30);
	const int ARTS_3 = (1 << 31);

	//うしろ
	 int PAD_BACK(int dir);
	

	// パッドの最大数
	const int	GAMEPAD_NUM = 4;

	//先行入力用保持データ

	struct KeyStackData
	{
		void Init()
		{ 
			keyBit = 0;
			frame = 0;

		}
		int keyBit;
		int frame;
	};

	

	// ゲームパッド
	enum GamePad
	{
		UP = 0,// dpad
		DOWN,
		RIGHT,
		LEFT,
		A = 4, // buttons
		B,
		X,
		Y,
		LSTICK,
		RSTICK,
		LSHOULDER,
		RSHOULDER,
		BACK,
		START,
		LSHOULDER2,
		RSHOULDER2,

	};
}

// キー割り当てデータ用構造体
struct PadAssign
{
	int         bit;				// PAD_STATE用ビットデータ
	int         code;				// 対応するキー（ボタン）コード
};

//**********************************
//
//      入力情報クラス
//
//==============================================================================
class PadState
{
public:

	int         state;              // キー入力情報
	int         trigger;            // トリガー入力
	int         triggerUp;            // トリガー入力


	int         repeat;             // リピート入力
	int         old[8];
	float       leftX, leftY;       // 左スティック
	float       rightX, rightY;     // 右スティック
	float       left, right;        // 左右トリガー
	PadAssign*  keyAssign;          // キーボード割り当てデータ
	PadAssign*  joyAssign;          // ジョイスティック割り当てデータ
};

//==============================================================================
//
//      InputManagerクラス
//
//==============================================================================
class InputManager : public Singleton<InputManager>
{
private:
	DirectX::Keyboard*  keyboard;   // https://github.com/Microsoft/DirectXTK/wiki/Keyboard
	DirectX::GamePad*   gamePad;    // https://github.com/Microsoft/DirectXTK/wiki/GamePad

	PadState     pad[Input::GAMEPAD_NUM];	    // ゲームパッド
	POINT        mousePos;			            // マウス座標

												// キー・ボタン割り当て
	void SetKeyAssign(int no, PadAssign* data); // キー割り当ての設定
	void SetJoyAssign(int no, PadAssign* data); // ジョイスティック割り当ての設定

public:

	~InputManager();

	// 初期化
	void Init();                                // 初期化

												// 更新										// 更新
	void Update(HWND);                          // 入力情報の更新

												// パッド											// パッド
	PadState* GetPadAddress() { return pad; }	// ゲームパッドの取得

												// マウス関連											// マウス関連
	int GetCursorPosX();                        // x座標取得
	int GetCursorPosY();                        // y座標取得
	VECTOR2 GetCursorPosXY();                   // 座標取得
	VECTOR2 oldCursorPos;  // 座標取得

	VECTOR2 oldStick[2];  // 座標取得
	float oldRTrigger[2];  // 座標取得
	float oldLTrigger[2];  // 座標取得

	VECTOR2 GetDeltaPos(); 
};
#define inputManager InputManager::GetInstance()

//******************************************************************************

