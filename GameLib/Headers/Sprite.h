#pragma once
#include <d3d11.h>
#include <DirectXMath.h>
#include "Texture.h"

#include "../Math/Vector.h"
#include "DXShader.h"

using namespace DirectX;

//***************************************
//
//		スプライト描画クラス
//
//***************************************

class Sprite
{
private:
	ID3D11Buffer* vertexBuffer = nullptr; // 頂点バッファ
	Texture* texture = nullptr;
	ID3D11DepthStencilState* depthStencilState;
	//スプライト用シェーダー
	Shader *shader;
	Shader *shaderPMA;

public:
	Sprite(const char* filename);//コンストラクタ


	~Sprite();	//デストラクタ



	void Render(VECTOR2 position, VECTOR2 scale, VECTOR2 texPos, VECTOR2 texSize, VECTOR2 center, VECTOR4 color, float angle);
	void Textout(VECTOR2 pos, VECTOR2 scale, VECTOR4 color, const char * s, ...);
	void TextRender(VECTOR2 pos, VECTOR2 scale, VECTOR2 center,VECTOR4 color,VECTOR2 texPos, VECTOR2 texSize, float angle = 0);




	//************************************************
	//
	//	頂点データ構造体定義
	//
	//************************************************
	struct VERTEX
	{
		VECTOR3 position;	//位置
		VECTOR3 normal;//法線
		VECTOR2 texcoord;	//UV座標
		VECTOR4 color;	//頂点色
	};

};


//***************************************
//
//		スプライトデータクラス
//
//***************************************

class SpriteData
{
public:
	void Init(Sprite* sprite,
		float texLeft,
		float texTop,
		float texWidth,
		float texHeight,
		float centerX,
		float centerY);//コンストラクタ
	SpriteData() {}// コンストラクタ
	void Render(VECTOR2 position, VECTOR2 scale, VECTOR4 color = VECTOR4(1, 1, 1, 1), float angle = 0, VECTOR2 center = {});
	void AllInit(Sprite * instance, int sprNum, int row, VECTOR2 size);


	//中心座標
	float centerX;
	float centerY;
	//テクスチャ切り取り位置
	float texLeft;
	float texTop;
	//テクスチャの切り取り幅
	float texWidth;
	float texHeight;
private:

	Sprite* sprite;

};


//==========================================================================
//
//      AnimeDataクラス
//
//==========================================================================
struct AnimeData
{
	SpriteData* data;   // スプライトデータ
	int     frame;  // 表示時間（フレーム数）
};

// アニメーション用構造体
struct Anime
{
	int        frame;    // 表示時間（フレーム数）
	int        patNum;   // パターン番号
	AnimeData* pPrev;    // 前回のアニメーションデータ
};

