#define _CRT_SECURE_NO_WARNINGS

#pragma once

#include <d3d11.h>
#include "Texture.h"
#include "../Math/Matrix.h"
#include <fbxsdk.h>
VECTOR2 ScreenToClip(VECTOR2 pos);

//**************************************************
//
//FBXクラス
//
//************************************************
class FBXMesh
{
protected:
	//	頂点構造体（シェーダーと一致）
	struct PolygonVertex {
		float x, y, z;		// 座標
		float nx, ny, nz;	// 法線
		float tu, tv;		// UV
		VECTOR4 color;		// 頂点カラー
	};

	int numMesh;		//複数メッシュ用、全メッシュ数
	int numVertices;	//全頂点数
	int numFaces;		//全ポリゴン数
	PolygonVertex* vertices;	//頂点
	PolygonVertex* verticesSrc; //頂点元データ
	DWORD* indices;		//三角形（頂点結び方）

	ID3D11Buffer* vertexBuffer;	// 頂点バッファ
	ID3D11Buffer* indexBuffer;		// インデックスバッファ

									//ディレクトリ情報のバッファとして使用
	char fbxDir[128];
	int* meshMaterial;
	int* materialFaces;
	Texture** textures;

	//	ボーン関連
	struct BONE {
		char	name[128];
		MATRIX	offsetMatrix;
		MATRIX	transform;
	};
	int numBone;
	BONE boneArray[256];

	int FindBone(const char* name);
	void LoadBone(FbxMesh* mesh);
	void LoadMeshAnim(FbxMesh* mesh);




	//	ウェイト関連
	struct WEIGHT {
		int count;
		int bone[4];
		float weight[4];
	};
	WEIGHT* weights;//実体

	int StartFrame;
	void Skinning();	// ボーンによる変形処理
	void MotionLerp();	// ボーンによる変形処理

	static const int MOTION_MAX = 256; //モーション最大数(1メッシュあたり)
	static const int BONE_MAX = 256; //ボーン最大数(1メッシュあたり)

									 //	アニメーション
	struct Motion {
		int numFrame;	// フレーム数	
		MATRIX* key[BONE_MAX];	// キーフレーム
	};

	int numMotion;	// モーション数
	Motion motion[MOTION_MAX];	// モーションデータ


	void LoadKeyFrames(int, int bone, FbxNode* bone_node);//キーフレーム読み込み

	void Load(const char* filename);//モデル読み込み
	void LoadMaterial(int index, FbxSurfaceMaterial* material);//マテリアル読み込み

															   //頂点情報関連
	void LoadPosition(FbxMesh* mesh);
	void LoadNormal(FbxMesh* mesh);
	void LoadUV(FbxMesh* mesh);
	void LoadVertexColor(FbxMesh* mesh);
	char textureName[32][128];

	void Save(const char* filename);
	bool LoadMAY(const char* filename);
	void SaveAnim(int name, const char* filename);
	bool LoadMAYAnim(int name, const char* filename);
	void OptimizeVertices();

public:

	~FBXMesh();
	FBXMesh() { isMotionStop = false; }



	void Update(bool isScroll);	//更新

	void Render();	//描画

					//現在再生しているモーション名を切り替え
					//補間のために過去のモーションを保存
	void Play(int motionNum, int frame = 0, bool isBlend = false) {
		motionNameOld = motionName;
		motionName = motionNum;
		this->nowFrame = (float)frame;//フレーム初期化
		this->isBlend = isBlend;
	}
	//アニメーション回す(引数入れるとその分遅延)
	bool Animate(float late=1.f);
	//アニメーション回す(ループせず一回で止まる)
	bool AnimateOnece();
	// モーション再生

	//FBXメッシュ生成
	void Create(const char* filename, VECTOR4 vertexColor);
	//モーション追加
	void AddMotion(int name, const char* filename);

	//	モーション情報
	float nowFrame;		// 現在のフレーム
	int motionName;		// 現在のモーション
	int motionNameOld;
	int blendTime = 0;
	//	姿勢情報
	MATRIX  transform;
	VECTOR3 position;
	VECTOR3 rotation;
	VECTOR3 scale;
	VECTOR3 speed;
	VECTOR4 vertexColor;
	//	ボーン行列取得
	MATRIX GetBoneMATRIX(int bone) {
		return boneArray[bone].transform;
	}

	int GetMotionFrame(int name) {
		return motion[name].numFrame;
	}

	bool isBlend = false;

	void SetTexture(Texture*tex);

	bool inverseFlg;

	bool isMotionStop;



};


