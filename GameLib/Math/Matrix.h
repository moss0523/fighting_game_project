

#pragma once
#include <stdio.h>
#include <string.h>
#include "vector.h"

//*****************************************************************************
//
//行列計算クラス
//
//*****************************************************************************
class MATRIX
{
public:
	union
	{
		struct
		{
			float _11, _12, _13, _14;
			float _21, _22, _23, _24;
			float _31, _32, _33, _34;
			float _41, _42, _43, _44;
		};
		float m[16];
	};

	float* getArray() { return m; }

	void identity();
	void multiply(const MATRIX& mat1, const MATRIX& mat2);
	void multiply(float val);
	void inverse();




	MATRIX() {};
	MATRIX(const float* p) { memcpy(&_11, p, sizeof(float) * 16); }
	MATRIX(float f11, float f12, float f13, float f14,
		float f21, float f22, float f23, float f24,
		float f31, float f32, float f33, float f34,
		float f41, float f42, float f43, float f44)
	{
		_11 = f11; _12 = f12; _13 = f13; _14 = f14;
		_21 = f21; _22 = f22; _23 = f23; _24 = f24;
		_31 = f31; _32 = f32; _33 = f33; _34 = f34;
		_41 = f41; _42 = f42; _43 = f43; _44 = f44;
	}


//*******************************************
//オペレーター
//*******************************************

	MATRIX& operator *= (const MATRIX& mat)
	{
		multiply(*this, mat);
		return *this;
	}

	MATRIX operator * (const MATRIX& mat) const
	{
		MATRIX matT;
		matT.multiply(*this, mat);
		return matT;
	}

	MATRIX operator + (const MATRIX& mat) const
	{
		MATRIX matT;
		for (int i = 0; i < 16; i++) {
			matT.m[i] = this->m[i] + mat.m[i];
		}
		return matT;
	}

	MATRIX operator * (float val) const
	{
		MATRIX matT;
		for (int i = 0; i < 16; i++) {

			matT.m[i] = this->m[i] * val;

		}
		return matT;
	}
	bool  operator == (MATRIX m) const
	{
		for (int i = 0; i < 16; i++) {

			if(m.m[i] != this->m[i])
				return false;

		}
		return true;
	}

	void LookAt(const VECTOR3& position,const VECTOR3& target, const VECTOR3& up = VECTOR3(0, 1.0f, 0));
	void PerspectiveFov(float fovY, float aspect, float znear, float zfar);
	void Ortho(
		float w, float h,
		float zn, float zf);

	void MATRIX::RotationZXY(float x, float y, float z);

};
