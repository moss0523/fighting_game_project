//******************************************************************************
//
//
//      VECTOR
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "vector.h"
#include "Matrix.h"





//******************************************************************************
//
//      VECTOR2
//
//******************************************************************************

//内積
float VECTOR2::dot(const VECTOR2& v1, const VECTOR2& v2)
{
	return v1.x*v2.x + v1.y*v2.y;
}




//******************************************************************************
//
//      VECTOR3
//
//******************************************************************************

//行列とベクトルの合成
void VECTOR3::Transform(VECTOR3& vec, MATRIX& mat)
{
	x = vec.x * mat._11 + vec.y * mat._21 + vec.z * mat._31 + mat._41;
	y = vec.x * mat._12 + vec.y * mat._22 + vec.z * mat._32 + mat._42;
	z = vec.x * mat._13 + vec.y * mat._23 + vec.z * mat._33 + mat._43;
}
//行列とベクトルの合成(正規化)
void VECTOR3::TransformCoord(VECTOR3& vec, MATRIX& mat)
{
	float w = vec.x * mat._14 + vec.y * mat._24 + vec.z * mat._34 + mat._44;

	x = (vec.x * mat._11 + vec.y * mat._21 + vec.z * mat._31 + mat._41) / w;
	y = (vec.x * mat._12 + vec.y * mat._22 + vec.z * mat._32 + mat._42) / w;
	z = (vec.x * mat._13 + vec.y * mat._23 + vec.z * mat._33 + mat._43) / w;
}
//行列とベクトルの合成(3*3行列)
void VECTOR3::Transform3x3(VECTOR3& vec, MATRIX& mat)
{
	x = vec.x * mat._11 + vec.y * mat._21 + vec.z * mat._31;
	y = vec.x * mat._12 + vec.y * mat._22 + vec.z * mat._32;
	z = vec.x * mat._13 + vec.y * mat._23 + vec.z * mat._33;
}
//内積
float VECTOR3::dot(const VECTOR3& v1, const VECTOR3& v2)
{
	return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
}
//外積
VECTOR3 VECTOR3::cross(VECTOR3& out, const VECTOR3& v1, const VECTOR3& v2)
{
	out.x = v1.y*v2.z - v1.z*v2.y;
	out.y = v1.z*v2.x - v1.x*v2.z;
	out.z = v1.x*v2.y - v1.y*v2.x;
	return out;
}
