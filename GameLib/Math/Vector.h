#pragma once
#include <math.h>

class MATRIX;
//------------------------------------------------------
//	2Ｄベクトル計算クラス
//------------------------------------------------------
class VECTOR2
{
public:
	float x, y;

	//	コンストラクタ
	VECTOR2() { x = 0; y = 0; }
	inline VECTOR2(float x, float y) { this->x = x, this->y = y; }
	inline VECTOR2(const VECTOR2& v) { this->x = v.x, this->y = v.y; }

	//	距離計算
	inline float Length() { return sqrtf(x*x + y*y); }
	inline float LengthSq() { return x*x + y*y; }

	//	正規化
	void Normalize()
	{
		float l = Length();
		if (l != .0f) { x /= l; y /= l; }
	}
	//足し算
	void add(VECTOR2 val) { x += val.x; y += val.y; }
	//引き算
	void sub(VECTOR2 val) { x -= val.x; y -= val.y; }
	//掛け算(VECTOR*float)
	void mul(float val) { x *= val; y *= val; }


	static float dot(const VECTOR2& v1, const VECTOR2& v2);

	//************************
	//オペレーター
	//************************
	inline VECTOR2& operator  = (const VECTOR2& v) { x = v.x; y = v.y;  return *this; }
	inline VECTOR2& operator += (const VECTOR2& v) { x += v.x; y += v.y; return *this; }
	inline VECTOR2& operator -= (const VECTOR2& v) { x -= v.x; y -= v.y; return *this; }
	inline VECTOR2& operator *= (float v) { x *= v; y *= v; return *this; }
	inline VECTOR2& operator /= (float v) { x /= v; y /= v; return *this; }

	inline VECTOR2 operator + () const { VECTOR2 ret(x, y); return ret; }
	inline VECTOR2 operator - () const { VECTOR2 ret(-x, -y); return ret; }

	inline VECTOR2 operator + (const VECTOR2& v) const { return VECTOR2(x + v.x, y + v.y); }
	inline VECTOR2 operator - (const VECTOR2& v) const { return VECTOR2(x - v.x, y - v.y); }
	inline VECTOR2 operator * (float v) const { VECTOR2 ret(x*v, y*v); return ret; }
	inline VECTOR2 operator / (float v) const { VECTOR2 ret(x / v, y / v); return ret; }

	bool operator == (const VECTOR2& v) const { return (x == v.x) && (y == v.y); }
	bool operator != (const VECTOR2& v) const { return (x != v.x) || (y != v.y); }

};


//------------------------------------------------------
//	３Ｄベクトル計算クラス
//------------------------------------------------------
class VECTOR3
{
public:
	float x, y, z;

	//	コンストラクタ
	VECTOR3() { x = 0; y = 0; z = 0; }
	inline VECTOR3(float x, float y, float z) { this->x = x, this->y = y, this->z = z; }
	inline VECTOR3(const VECTOR3& v) { this->x = v.x, this->y = v.y, this->z = v.z; }

	//	距離計算
	inline float Length() { return sqrtf(x*x + y*y + z*z); }
	inline float LengthSq() { return x*x + y*y + z*z; }

	//	正規化
	void Normalize()
	{
		float l = Length();
		if (l != .0f) { x /= l; y /= l; z /= l; }
	}
	//足し算
	void add(VECTOR3 val) { x += val.x; y += val.y; z += val.z; }
	//引き算
	void sub(VECTOR3 val) { x -= val.x; y -= val.y; z -= val.z; }
	//掛け算(VECTOR*float)
	void mul(float val) { x *= val; y *= val; z *= val; }

	void Transform(VECTOR3& vec, MATRIX& mat);
	void TransformCoord(VECTOR3& vec, MATRIX& mat);
	void Transform3x3(VECTOR3& vec, MATRIX& mat);

	static float dot(const VECTOR3& v1, const VECTOR3& v2);
	static VECTOR3 cross(VECTOR3& out, const VECTOR3& v1, const VECTOR3& v2);

	//************************
	//オペレーター
	//************************
	inline VECTOR3& operator = (const VECTOR3& v) { x = v.x; y = v.y; z = v.z; return *this; }
	inline VECTOR3& operator += (const VECTOR3& v) { x += v.x; y += v.y; z += v.z; return *this; }
	inline VECTOR3& operator -= (const VECTOR3& v) { x -= v.x; y -= v.y; z -= v.z; return *this; }
	inline VECTOR3& operator *= (float v) { x *= v; y *= v; z *= v; return *this; }
	inline VECTOR3& operator /= (float v) { x /= v; y /= v; z /= v; return *this; }

	inline VECTOR3 operator + () const { VECTOR3 ret(x, y, z); return ret; }
	inline VECTOR3 operator - () const { VECTOR3 ret(-x, -y, -z); return ret; }

	inline VECTOR3 operator + (const VECTOR3& v) const { return VECTOR3(x + v.x, y + v.y, z + v.z); }
	inline VECTOR3 operator - (const VECTOR3& v) const { return VECTOR3(x - v.x, y - v.y, z - v.z); }
	inline VECTOR3 operator * (float v) const { VECTOR3 ret(x*v, y*v, z*v); return ret; }
	inline VECTOR3 operator / (float v) const { VECTOR3 ret(x / v, y / v, z / v); return ret; }

	bool operator == (const VECTOR3& v) const { return (x == v.x) && (y == v.y) && (z == v.z); }
	bool operator != (const VECTOR3& v) const { return (x != v.x) || (y != v.y) || (z != v.z); }

};


//------------------------------------------------------
//	色情報用4Dベクトル
//------------------------------------------------------
class VECTOR4
{
public:
	float x, y, z,w;

	//	コンストラクタ
	VECTOR4() { x = 0; y = 0; z = 0; w = 0; }
	inline VECTOR4(float x, float y, float z,float w) { this->x = x, this->y = y, this->z = z; this->w = w;}
	inline VECTOR4(const VECTOR4& v) { this->x = v.x, this->y = v.y, this->z = v.z; this->w = v.w;}

	
};





