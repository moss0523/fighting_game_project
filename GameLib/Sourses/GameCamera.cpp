#include "../Headers/GameCamera.h"
#include "../Headers/SceneBattle.h"
#include "../Headers/Common.h"

//***************************************************
//
// 定数
//
//***************************************************

static const float SCROLL_Y_MAX = 800.f;//この値までスクロール

static const VECTOR3 CAMERA_INIT_POS = VECTOR3(0, 0, -10.0f);//カメラ位置
static const VECTOR3 CAMERA_INIT_TARGET = VECTOR3(0, 0, -15.f);//注視点
static const VECTOR3 CAMERA_INIT_UP = VECTOR3(0, 1.f, 0);

static const float PERS_CAMERA_FOV = 1.3f;//視野角
static const float PERS_CAMERA_Z_NEAR = 0.1f;//Z値最小
static const float PERS_CAMERA_Z_FAR = 30.f;//Z値最大

static const float ORTHO_CAMERA_WIDTH = 2.f;//平行投影の幅
static const float ORTHO_CAMERA_Z_NEAR = -1.f;//Z値最小
static const float ORTHO_CAMERA_Z_FAR = 1000.f;//Z値最大

static const float CAMERA_BLEND_RATE = 0.8f;//カメラブレンド率


void GameCamera::scroll2D()
{
	VECTOR2 pl1Pos = sceneBattle->player1->GetPosition();

	VECTOR2 pl2Pos = sceneBattle->player2->GetPosition();

	//プレイヤーの位置の平均値
	VECTOR2 centerPos = (pl1Pos + pl2Pos) / 2.0f;


	if (!sceneBattle->isEdit) {
		// 変数
		if ((centerPos.x < SCROLL_MAX_X) &&
			(centerPos.x > SCROLL_MIN_X))
		{
			//常にプレイヤーの位置の平均値を中心にスクロール値を計算
			scroll.x = (centerPos.x) - DxSystem::SCREEN_WIDTH / 2.f;
		}
		//常にプレイヤーの位置の平均値を中心にスクロール値を計算
		scroll.y = 0;

	}
	else
	{
		scroll = {};
	}

}



void GameCamera::scroll3D()
{
	//常にプレイヤーの位置の平均値(クリップ座標系に変換
	VECTOR2 cPos = VECTOR2(
		(sceneBattle->player1->GetPosition().x + sceneBattle->player2->GetPosition().x) / 2.f,
		(sceneBattle->player1->GetPosition().y + sceneBattle->player2->GetPosition().y) / 2.f - (DxSystem::SCREEN_HEIGHT - SCROLL_Y_MAX));

	//常にプレイヤーの位置の平均値を中心にカメラ位置を計算
	if (cPos.x <SCROLL_MAX_X&&
		cPos.x >SCROLL_MIN_X) {

		pos.x = cPos.x;
		target.x = cPos.x;
	}

	
}





void GameCamera::Init()
{
	state = 0;
}

void GameCamera::Update()
{



	switch (state)
	{
	case MOVE_STATE::INIT:

		pos = CAMERA_INIT_POS;
		target = CAMERA_INIT_TARGET;
		up = CAMERA_INIT_UP;


		state++;
		//break;

	case MOVE_STATE::MAIN:



		if (!sceneBattle->isEdit&&isScroll) {
			scroll2D();
			scroll3D();
		}

		VECTOR3 p = VECTOR3(ScreenToClip(VECTOR2(pos.x, pos.y)).x,
			ScreenToClip(VECTOR2(pos.x, pos.y)).y, pos.z);
		VECTOR3 t = VECTOR3(ScreenToClip(VECTOR2(target.x, target.y)).x,
			ScreenToClip(VECTOR2(target.x, target.y)).y, target.z);

		// ビュー行列
		ViewMATRIX.LookAt(p, t, up);

		//アス比
		float aspect = (float)DxSystem::SCREEN_WIDTH / (float)DxSystem::SCREEN_HEIGHT;
		ProjectionMATRIX.PerspectiveFov(PERS_CAMERA_FOV, aspect, PERS_CAMERA_Z_NEAR, PERS_CAMERA_Z_FAR);


		// 平行投影行列作成ゲーム中のキャラクター
		ProjectionMATRIXOrtho.Ortho(ORTHO_CAMERA_WIDTH, ORTHO_CAMERA_WIDTH, ORTHO_CAMERA_Z_NEAR, ORTHO_CAMERA_Z_FAR);


		//視覚的違和感を減らすため、ビュー行列をブレンドします
		//通常時(Ortho:CAMERA_BLEND_RATE,Pers:1-CAMERA_BLEND_RATE)
		float rate = CAMERA_BLEND_RATE;

		MATRIX mO = ProjectionMATRIXOrtho;
		MATRIX mP = ProjectionMATRIX;
		//行列にブレンド率を掛け算する
		mP.multiply(1 - rate);
		mO.multiply(rate);
		//ブレンド行列作成
		ProjectionMATRIXBlend = mO + mP;
		break;
	}
}
