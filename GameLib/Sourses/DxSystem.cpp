#include "../Headers/DxSystem.h"

#pragma comment( lib, "d3d11.lib" )
#pragma comment( lib, "d3dcompiler.lib" )


//COMオブジェクトの宣言
ID3D11Device*				DxSystem::device;			
IDXGISwapChain*				DxSystem::swapChain;
ID3D11DeviceContext*		DxSystem::deviceContext;
ID3D11RenderTargetView*		DxSystem::renderTargetView;

ID3D11Texture2D*            DxSystem::depthStencilTexture;
ID3D11DepthStencilView*     DxSystem::depthStencilView;
ID3D11ShaderResourceView*   DxSystem::shaderResourceView;
ID3D11DepthStencilState*	DxSystem::depthStencilState;

int DxSystem::SCREEN_WIDTH = 1920;//画面横幅
int DxSystem::SCREEN_HEIGHT = 1080;//画面縦幅
//経過時間
float DxSystem::elapsedTime = 0;

//****************************************************************
//
//	初期化
//
//****************************************************************
bool DxSystem::Initialize(HWND hWnd, int width, int height)
{
	Createdevice(hWnd);
	InitializeRenderTarget();

	D3D11_RASTERIZER_DESC rasterizerDesc;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	rasterizerDesc.FrontCounterClockwise = true;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.SlopeScaledDepthBias = 0;
	rasterizerDesc.DepthClipEnable = true;
	rasterizerDesc.ScissorEnable = false;
	rasterizerDesc.MultisampleEnable = false;
	rasterizerDesc.AntialiasedLineEnable = false;
	rasterizerDesc.DepthBiasClamp = 0;
	rasterizerDesc.SlopeScaledDepthBias = 0;

	ID3D11RasterizerState * rState;
	device->CreateRasterizerState(&rasterizerDesc, &rState);
	deviceContext->RSSetState(rState);
	rState->Release();
	return false;
}

//カリングモード変更---------------------------------------------------------

void DxSystem::CullFront()
{
	D3D11_RASTERIZER_DESC rasterizerDesc;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_FRONT;
	rasterizerDesc.FrontCounterClockwise = true;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.SlopeScaledDepthBias = 0;
	rasterizerDesc.DepthClipEnable = true;
	rasterizerDesc.ScissorEnable = false;
	rasterizerDesc.MultisampleEnable = false;
	rasterizerDesc.AntialiasedLineEnable = false;
	rasterizerDesc.DepthBiasClamp = 0;
	rasterizerDesc.SlopeScaledDepthBias = 0;

	ID3D11RasterizerState * rState;
	device->CreateRasterizerState(&rasterizerDesc, &rState);
	deviceContext->RSSetState(rState);
	rState->Release();
}

void DxSystem::CullBack()
{
	D3D11_RASTERIZER_DESC rasterizerDesc;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	rasterizerDesc.FrontCounterClockwise = true;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.SlopeScaledDepthBias = 0;
	rasterizerDesc.DepthClipEnable = true;
	rasterizerDesc.ScissorEnable = false;
	rasterizerDesc.MultisampleEnable = false;
	rasterizerDesc.AntialiasedLineEnable = false;
	rasterizerDesc.DepthBiasClamp = 0;
	rasterizerDesc.SlopeScaledDepthBias = 0;

	ID3D11RasterizerState * rState;
	device->CreateRasterizerState(&rasterizerDesc, &rState);
	deviceContext->RSSetState(rState);
	rState->Release();
}
void DxSystem::CullNone()
{
	D3D11_RASTERIZER_DESC rasterizerDesc;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.FrontCounterClockwise = true;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.SlopeScaledDepthBias = 0;
	rasterizerDesc.DepthClipEnable = true;
	rasterizerDesc.ScissorEnable = false;
	rasterizerDesc.MultisampleEnable = false;
	rasterizerDesc.AntialiasedLineEnable = false;
	rasterizerDesc.DepthBiasClamp = 0;
	rasterizerDesc.SlopeScaledDepthBias = 0;

	ID3D11RasterizerState * rState;
	device->CreateRasterizerState(&rasterizerDesc, &rState);
	deviceContext->RSSetState(rState);
	rState->Release();
}



//****************************************************************
//
//	デバイス生成
//
//****************************************************************
HRESULT DxSystem::Createdevice( HWND hWnd )
{
	HRESULT hr = S_OK;

	UINT createdeviceFlags = 0;
	//createdeviceFlags |= D3D11_CREATE_device_DEBUG;

	// 機能レベル
	D3D_FEATURE_LEVEL featureLevels[] = {
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevels = sizeof(featureLevels) / sizeof(featureLevels[0]);

	D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;

	// スワップチェインの設定
	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 1;
	sd.BufferDesc.Width = SCREEN_WIDTH;
	sd.BufferDesc.Height = SCREEN_HEIGHT;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = hWnd;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE;

	// デバイス生成
	hr = D3D11CreateDeviceAndSwapChain(
		NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		0,
		NULL,
		0,
		D3D11_SDK_VERSION,
		&sd,
		&swapChain,
		&device,
		&featureLevel,
		&deviceContext);
	
	//エラー処理
	if (FAILED(hr))
	{
		return hr;
	}

	return S_OK;
}

//メンバの解放
void DxSystem::Release()
{

	if (depthStencilState){depthStencilState->Release();}
	if (depthStencilView) { depthStencilView->Release(); }
	if (shaderResourceView) { shaderResourceView->Release(); }
	if (depthStencilTexture) { depthStencilTexture->Release(); }
	if (deviceContext) { deviceContext->ClearState(); }
	if (renderTargetView) { renderTargetView->Release(); renderTargetView = NULL; }
	if (swapChain) { swapChain->Release(); swapChain = NULL; }
	if (deviceContext) { deviceContext->Release(); deviceContext = NULL; }
	if (device) { device->Release(); device = NULL; }
}

//****************************************************************
//
//	レンダーターゲット関連
//
//****************************************************************
//------------------------------------------------
//	初期化
//------------------------------------------------
bool DxSystem::InitializeRenderTarget()
{
	// バックバッファ取得
	ID3D11Texture2D* backBuffer = NULL;
	HRESULT hr = swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);
	if (FAILED(hr))
	{
		return false;
	}

	// レンダーターゲットビュー生成
	hr = device->CreateRenderTargetView(backBuffer, NULL, &renderTargetView);
	backBuffer->Release();
	backBuffer = NULL;
	if (FAILED(hr))
	{
		return false;
	}

	CreateDepthStencil();

	// レンダーターゲットビュー設定
	deviceContext->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

	// ビューポート設定
	D3D11_VIEWPORT vp;
	vp.Width = (float)SCREEN_WIDTH;
	vp.Height = (float)SCREEN_HEIGHT;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	deviceContext->RSSetViewports(1, &vp);

	return true;
}

//------------------------------------------------
//      深度ステンシルバッファ生成
//------------------------------------------------
bool DxSystem::CreateDepthStencil()
{
	// 深度ステンシル設定
	D3D11_TEXTURE2D_DESC td;
	ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
	td.Width = SCREEN_WIDTH;
	td.Height = SCREEN_HEIGHT;
	td.MipLevels = 1;
	td.ArraySize = 1;
	td.Format = DXGI_FORMAT_R24G8_TYPELESS;
	td.SampleDesc.Count = 1;
	td.SampleDesc.Quality = 0;
	td.Usage = D3D11_USAGE_DEFAULT;
	td.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	td.CPUAccessFlags = 0;
	td.MiscFlags = 0;

	// 深度ステンシルテクスチャ生成
	HRESULT hr = device->CreateTexture2D(&td, NULL, &depthStencilTexture);
	if (FAILED(hr))
	{
		return false;
	}

	// 深度ステンシルビュー設定
	D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
	ZeroMemory(&dsvd, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	dsvd.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	dsvd.Texture2D.MipSlice = 0;

	// 深度ステンシルビュー生成
	hr = device->CreateDepthStencilView(depthStencilTexture, &dsvd, &depthStencilView);
	if (FAILED(hr))
	{
		return false;
	}
	//デプスステンシルステート
	D3D11_DEPTH_STENCIL_DESC dsd;
	ZeroMemory(&dsd, sizeof(depthStencilState));
	dsd.DepthEnable = TRUE;
	dsd.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsd.DepthFunc = D3D11_COMPARISON_LESS;
	hr = device->CreateDepthStencilState(&dsd, &depthStencilState);


	if (FAILED(hr))
	{
		return false;
	}



	// シェーダリソースビュー設定
	D3D11_SHADER_RESOURCE_VIEW_DESC srvd;
	ZeroMemory(&srvd, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	srvd.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;

	srvd.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvd.Texture2D.MostDetailedMip = 0;
	srvd.Texture2D.MipLevels = 1;

	// シェーダリソースビュー生成
	hr = device->CreateShaderResourceView(depthStencilTexture, &srvd, &shaderResourceView);
	if (FAILED(hr))
	{
		return false;
	}

	return true;
}


//------------------------------------------------
//	画面クリア
//------------------------------------------------
void DxSystem::Clear()
{
	//クリアカラー
	float clearColor[4] = { 0.5f, 0.5f, 0.8f, 1.0f };
	//クリア処理
	deviceContext->ClearRenderTargetView(renderTargetView, clearColor);
	deviceContext->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}

//------------------------------------------------
//	フリップ
//------------------------------------------------
void DxSystem::Flip()
{
	//フリップ処理(60FPS固定
	swapChain->Present(1, 0);
}
