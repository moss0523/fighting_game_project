#include "../GameLib/Headers/Command.h"
//==============================================================================
//
//		コマンド入力
//
//==============================================================================

// コマンド解析クラス
void ComParse::Init(COMMAND_DATA* data)
{
	start =current = data;
	interval = 0;
}
bool ComParse::Parse(int key)
{
	//猶予フレームを減らす、0になったら最初から
	interval--;
	if (interval <= 0) {
		current = start;
		interval = 0;
	}

	//キー入力があった場合
	if (key) {

		//キー入力がコマンドと一致していれば
		if (key == current->key) {

			//猶予フレーム更新
			interval = current->interval;
			//次の猶予フレームが０なら最後のコマンド
			if (interval == 0) 
				return true;//コマンド成功！！

			//次に進める
			current++;

		}
	}

	return false;
}

