#include "../Headers/DxSystem.h"
#include "../Headers/RectRender.h"


RectRender::RectRender()//コンストラクタ
{


	//頂点情報設定
	VERTEX v[4] = {};

	//頂点バッファ作成
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(v);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA res;
	ZeroMemory(&res, sizeof(res));
	res.pSysMem = v;

	DxSystem::device->CreateBuffer(&bd, &res, &vertexBuffer);

	
	//深度ステンシル設定
	D3D11_DEPTH_STENCIL_DESC dsd;
	dsd.DepthEnable = FALSE;
	dsd.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	dsd.DepthFunc = D3D11_COMPARISON_ALWAYS;
	dsd.StencilEnable = FALSE;
	dsd.StencilReadMask = 0xFF;
	dsd.StencilWriteMask = 0xFF;
	dsd.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsd.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsd.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsd.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	dsd.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsd.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsd.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsd.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	HRESULT hr = DxSystem::device->CreateDepthStencilState(&dsd, &depthStencilState);
	if (FAILED(hr))
	{
		return;
	}

	//シェーダー作成
	shader = new Shader();
	shader->Create(L"Shaders/Rect.fx", "VSMain", "PSMain");
}

RectRender::~RectRender()
{
	if (vertexBuffer)vertexBuffer->Release();
	if (depthStencilState)depthStencilState->Release();
	if (shader)delete shader;
}



void RectRender::Render(VECTOR2 position, VECTOR2 size, VECTOR4 color)
{
	if (size.x*size.y == 0)return;
	if (color.w == 0)return;

	//シェーダー起動
	shader->Activate();

	//ビューポートの情報を取得
	D3D11_VIEWPORT viewport;
	UINT numViewports = 1;
	DxSystem::deviceContext->RSGetViewports(&numViewports, &viewport);

	//頂点初期位置設定
	VERTEX vertices[] = {
		VECTOR3(-0.0f, +1.0f, 0), VECTOR3(0,0,1), VECTOR2(0, 1),color, //左上
		VECTOR3(+1.0f, +1.0f, 0), VECTOR3(0,0,1), VECTOR2(1, 1),color, //右上
		VECTOR3(-0.0f, -0.0f, 0), VECTOR3(0,0,1), VECTOR2(0, 0),color, //左下
		VECTOR3(+1.0f, -0.0f, 0), VECTOR3(0,0,1), VECTOR2(1, 0),color, //右下
	};
	
	//NDC変換(頂点情報を引数をもとに計算しビューポートの比率に合わせる
	for (int i = 0; i < 4; i++)
	{
		vertices[i].position.x *= size.x;
		vertices[i].position.y *= size.y;
		vertices[i].position.x -= size.x/2.0f;
		vertices[i].position.y -= size.y/2.0f;
		vertices[i].position.x += position.x;
		vertices[i].position.y += position.y;

		vertices[i].position.x = vertices[i].position.x * 2 / viewport.Width - 1.0f;
		vertices[i].position.y = 1.0f - vertices[i].position.y * 2 / viewport.Height;
	}


	//頂点データ更新
	DxSystem::deviceContext->UpdateSubresource(vertexBuffer, 0, NULL, vertices, 0, 0);
	
	//	頂点バッファの指定
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	DxSystem::deviceContext->IASetVertexBuffers(
		0, 1, &vertexBuffer, // スロット, 数, バッファ
		&stride,		// １頂点のサイズ
		&offset			// 開始位置
	);
	//プリミティブトポロジー指定
	DxSystem::deviceContext->IASetPrimitiveTopology(
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
	);
	//深度ステンシルステート
	DxSystem::deviceContext->OMSetDepthStencilState(depthStencilState, 0);

	//描画
	DxSystem::deviceContext->Draw(4, 0);
}








