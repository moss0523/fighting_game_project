#include <windows.h>
#include <memory>
#include <assert.h>
#include <tchar.h>
#include <sstream>
#include <thread>


#include "../Headers/DxSystem.h"
#include "../Headers/SceneBattle.h"

#include "../Headers/SceneTitle.h"
#include "../Headers/SceneMenu.h"

#include "../Headers/InputManager.h"
#include "../Headers/Scene.h"
#include "../GameLib/Headers/Imgui.h"

//コールバック関数
LRESULT CALLBACK fnWndProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	if (ImGui_ImplWin32_WndProcHandler(hwnd, msg, wparam, lparam)) { return true; }

	switch (msg) {
	case WM_CLOSE:
		PostMessage(hwnd, WM_DESTROY, 0, 0);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:
		if (wparam == VK_ESCAPE)
		{
			PostMessage(hwnd, WM_CLOSE, 0, 0);
			break;
		}
	case WM_SYSKEYDOWN:
	case WM_KEYUP:
	case WM_SYSKEYUP:
		DirectX::Keyboard::ProcessMessage(msg, wparam, lparam);
		break;
	case WM_CREATE:
		break;
	}

	return(DefWindowProc(hwnd, msg, wparam, lparam));
}


//メイン関数
int WINAPI wWinMain(HINSTANCE instance, HINSTANCE prev_instance, LPWSTR cmd_line, INT cmd_show)
{
	// ウィンドウ生成
	TCHAR szWindowClass[] = TEXT("FIGHTING_GAME");
	WNDCLASS wcex;
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = fnWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = instance;
	wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);;
	wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szWindowClass;
	RegisterClass(&wcex);

	HWND hwnd;
	hwnd = CreateWindow(szWindowClass,
		TEXT("FIGHTING_GAME"),
		WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
		0, 0, 1920, 1080,
		NULL,
		NULL,
		instance,
		NULL);
	ShowWindow(hwnd, cmd_show);

	
	
	SceneManager scene;
	//ゲーム部分
	scene.Execute(hwnd, sceneMenu);
	
	// メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	return 0;


}
