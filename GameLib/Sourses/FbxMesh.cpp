#include "../Headers/FbxMesh.h"
#include "../../Headers/SceneBattle.h"

//****************************************************************
//
//	更新
//
//****************************************************************

//------------------------------------------------
//	更新
//------------------------------------------------
void FBXMesh::Update(bool isScroll)
{

	////回転
	transform.RotationZXY(rotation.x, rotation.y, rotation.z);

	//スケーリング
	transform._11 *= scale.x;
	transform._12 *= scale.x;
	transform._13 *= scale.x;
	transform._21 *= scale.y;
	transform._22 *= scale.y;
	transform._23 *= scale.y;
	transform._31 *= scale.z;
	transform._32 *= scale.z;
	transform._33 *= scale.z;



	//座標変換(クリップ座標ー>スクリーン座標)
	VECTOR2 pos = ScreenToClip(VECTOR2(position.x, position.y));


	transform._41 = pos.x;//X座標
	transform._42 = pos.y;//Y座標
	transform._43 = position.z;// Z座標


}

//------------------------------------------------
//	アニメーション
//------------------------------------------------
//60FPSでアニメーションする
static const float FPS60 = 1.f / 60.f;
//ループあり
bool FBXMesh::Animate(float late)
{
	if (isMotionStop)return false;


	//	モーション時間の更新
	nowFrame += FPS60 * 60 * late;
	//	ループチェック
	if (nowFrame >= motion[motionName].numFrame - 1)
	{
		// ループ
		nowFrame = 0;		// 全体をループ
		return true;
	}
	else
		return false;



}
//一度だけアニメーションしたいとき
bool FBXMesh::AnimateOnece()
{
	if (isMotionStop)return false;


	//	モーション時間の更新
	nowFrame += FPS60 * 60;
	//	ループチェック
	if (nowFrame >= motion[motionName].numFrame - 1)
	{
		// ループ
		nowFrame = (float)(motion[motionName].numFrame - 1);		// 全体をループ
		return true;
	}
	else
		return false;



}
//****************************************************************
//
//	描画
//
//****************************************************************


void FBXMesh::Render() {


	//	モーションが存在する場合はSkinning
	if (motion[motionName].numFrame > 0) {
		Skinning();
	}

	// 頂点バッファ設定
	UINT stride = sizeof(PolygonVertex);
	UINT offset = 0;
	DxSystem::deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	DxSystem::deviceContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	DxSystem::deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	int start = 0;
	for (int m = 0; m < numMesh; m++) {
		int material_no = meshMaterial[m];
		if (textures[material_no] != NULL) {
			textures[material_no]->Set();
		}
		//	描画
		DxSystem::deviceContext->DrawIndexed(materialFaces[m] * 3, start, 0);
		start += materialFaces[m] * 3;
	}
}


//****************************************************************
//テクスチャ関連処理
//****************************************************************
// テクスチャ多重読み込み禁止

char textureName[1000][128];
int  textureCount[1000]; // 参照カウンタ
Texture* textureSource[1000];
bool isInitializeTexture = false;

//テクスチャ初期設定
void InitTexture() {
	for (int i = 0; i < 1000; i++) {
		textureName[i][0] = '\0';
		textureSource[i] = NULL;
		textureCount[i] = 0;
	}
	isInitializeTexture = true;
}

//テクスチャ読み込み
Texture* LoadTexture(const char* filename)
{
	if (isInitializeTexture == false) {
		InitTexture();
	}
	//	既に存在する場合
	for (int i = 0; i < 1000; i++) {
		if (textureName[i][0] == '\0') continue;
		if (strcmp(textureName[i], filename) == 0) {
			textureCount[i]++;
			return textureSource[i];
		}
	}

	//	空き場所に読み込み
	for (int i = 0; i < 1000; i++) {
		if (textureName[i][0] != '\0') continue;

		Texture* tex;
		tex = new Texture();
		if (tex->Load(filename) == false) return NULL;
		textureSource[i] = tex;
		strcpy(textureName[i], filename);
		textureCount[i] = 1;
		return tex;
	}
	return NULL;
}

//テクスチャ解放処理
void ReleaseTexture(Texture* tex)
{
	//	検索
	for (int i = 0; i < 1000; i++) {
		if (textureSource[i] != tex) continue;
		textureCount[i]--;
		if (textureCount[i] <= 0) {
			if (textureSource[i] != NULL) {
				delete textureSource[i];
				textureSource[i] = NULL;
			}
			textureName[i][0] = '\0';
		}
		return;
	}
	//	管理外のテクスチャ
	if (tex != NULL) {
		delete tex;
	}
}



//****************************************************************
//
//	解放
//
//****************************************************************
#define DELETE_IF(pointer) {if(pointer)delete pointer; pointer = nullptr;}
#define DELETE_ARRAY_IF(pointer) {if(pointer)delete[] pointer; pointer = nullptr;}

FBXMesh::~FBXMesh()
{
	//	頂点情報解放
	DELETE_ARRAY_IF(vertices);
	DELETE_ARRAY_IF(indices);
	DELETE_ARRAY_IF(weights);
	DELETE_ARRAY_IF(verticesSrc);
	//	材質関連解放
	DELETE_ARRAY_IF(materialFaces);
	DELETE_ARRAY_IF(meshMaterial);
	//	テクスチャ解放
	for (int i = 0; i < numMesh; i++) {
		if (textures[i] != NULL)
			ReleaseTexture(textures[i]);
	}

	DELETE_ARRAY_IF(textures);

	//	モーション関連解放
	for (int i = 0; i < MOTION_MAX; i++) {
		Motion* M = &motion[i];

		for (int bone = 0; bone < numBone; bone++) {
			DELETE_ARRAY_IF(M->key[bone]);
		}
	}

	if (vertexBuffer) { vertexBuffer->Release(); vertexBuffer = NULL; }
	if (indexBuffer) { indexBuffer->Release(); indexBuffer = NULL; }

}

//****************************************************************
//
//	ファイル読み込み
//
//****************************************************************

void FBXMesh::Create(const char* filename, VECTOR4 vertexColor)
{
	//ファイル名を取り除く
	strcpy(fbxDir, filename);
	for (int n = strlen(fbxDir) - 1; n >= 0; n--)
	{
		if (fbxDir[n] == '/' || fbxDir[n] == '\\')
		{
			fbxDir[n + 1] = '\0';
			break;
		}
	}

	this->vertexColor = vertexColor;
	Load(filename);

	// 頂点バッファの生成.
	{
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(PolygonVertex) * numVertices;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		// サブリソースの設定.
		D3D11_SUBRESOURCE_DATA initData;
		ZeroMemory(&initData, sizeof(D3D11_SUBRESOURCE_DATA));
		initData.pSysMem = vertices;
		HRESULT hr = DxSystem::device->CreateBuffer(&bd, &initData, &vertexBuffer);
	}

	{
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(DWORD) * numFaces * 3;
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA initData;
		ZeroMemory(&initData, sizeof(initData));
		initData.pSysMem = indices;

		HRESULT hr = DxSystem::device->CreateBuffer(&bd, &initData, &indexBuffer);
	}
	//	情報初期化
	nowFrame = 0;
	position = VECTOR3(0, 0, 0);
	rotation = VECTOR3(0, 0, 0);
	scale = VECTOR3(1, 1, 1);
	Update(false);
}

void FBXMesh::Load(const char* filename)
{

	for (int i = 0; i < 32; i++) {
		textureName[i][0] = '\0';
	}
	//	最適化ファイル確認
	char name2[256];
	strcpy(name2, filename);
	name2[strlen(name2) - 3] = 'M';
	name2[strlen(name2) - 2] = 'A';
	name2[strlen(name2) - 1] = 'Y';
	if (LoadMAY(name2)) return;


	FbxManager* manager = FbxManager::Create();
	FbxScene* scene = FbxScene::Create(manager, "");
	//	ファイルからシーンに読み込み
	FbxImporter* importer = FbxImporter::Create(manager, "");
	importer->Initialize(filename);
	importer->Import(scene);
	importer->Destroy();

	//	モーション情報取得
	FbxArray<FbxString*> names;
	scene->FillAnimStackNameArray(names);

	if (names != NULL) {
		//	モーションが存在するとき
		FbxTakeInfo* take = scene->GetTakeInfo(names[0]->Buffer());
		FbxLongLong start = take->mLocalTimeSpan.GetStart().Get();
		FbxLongLong stop = take->mLocalTimeSpan.GetStop().Get();
		FbxLongLong fps60 = FbxTime::GetOneFrameValue(FbxTime::eFrames60);
		StartFrame = (int)(start / fps60);
		motion[0].numFrame = (int)((stop - start) / fps60);
	}
	else {
		StartFrame = 0;
		motion[0].numFrame = 0;
	}

	//	モデルを材質ごとに分割
	FbxGeometryConverter fgc(manager);
	fgc.SplitMeshesPerMaterial(scene, true);
	fgc.Triangulate(scene, true);

	numBone = 0;

	//	メッシュ数
	numMesh = scene->GetSrcObjectCount<FbxMesh>();

	//	頂点数計算
	int work = 0;
	for (int m = 0; m < numMesh; m++) {
		FbxMesh* mesh = scene->GetSrcObject<FbxMesh>(m);
		int num = mesh->GetPolygonVertexCount();
		work += num; // 合計頂点数
	}

	//	頂点確保
	vertices = new PolygonVertex[work];
	indices = new DWORD[work];
	weights = new WEIGHT[work];

	numVertices = 0;
	//	初期化
	for (int v = 0; v < work; v++) {
		weights[v].count = 0;
	}

	//材質ごとのポリゴン頂点数
	materialFaces = new int[numMesh];
	meshMaterial = new int[numMesh];
	textures = new Texture*[numMesh];

	for (int m = 0; m < numMesh; m++)
	{
		textures[m] = NULL;
	}

	//	頂点読み込み
	for (int m = 0; m < numMesh; m++) {
		FbxMesh* mesh = scene->GetSrcObject<FbxMesh>(m);
		int num = mesh->GetPolygonVertexCount();

		//	頂点情報読み込み
		LoadPosition(mesh);		//	座標読み込み
		LoadNormal(mesh);		//	法線読み込み
		LoadUV(mesh);			//	テクスチャUV
		LoadVertexColor(mesh);	//	頂点カラー読み込み

								//	インデックス設定(三角形ごと)
		for (int i = 0; i < num; i += 3) {
			indices[i + 0 + numVertices] = i + 0 + numVertices;
			indices[i + 1 + numVertices] = i + 1 + numVertices;
			indices[i + 2 + numVertices] = i + 2 + numVertices;
		}

		//	ボーン読み込み
		LoadBone(mesh);

		//	メッシュの使用材質取得
		FbxLayerElementMaterial* LEM = mesh->GetElementMaterial();
		if (LEM != NULL) {
			//	ポリゴンに貼られている材質番号
			int material_index = LEM->GetIndexArray().GetAt(0);
			//	メッシュ材質のmaterial_index番目を取得
			FbxSurfaceMaterial* material = mesh->GetNode()->GetMaterial(material_index);
			LoadMaterial(m, material);
		}
		//	使用材質設定
		meshMaterial[m] = m;
		materialFaces[m] = num / 3;

		numVertices += num;
	}

	numFaces = numVertices / 3;
	OptimizeVertices();


	//	頂点元データ保存
	verticesSrc = new PolygonVertex[numVertices];
	memcpy(verticesSrc, vertices, sizeof(PolygonVertex)*numVertices);

	//	ウェイト正規化
	// ５本以上にまたっがてる場合のため
	for (int v = 0; v < numVertices; v++) {
		float n = 0;
		//	頂点のウェイトの合計値
		for (int w = 0; w < weights[v].count; w++) {
			n += weights[v].weight[w];
		}
		//	正規化
		for (int w = 0; w < weights[v].count; w++) {
			weights[v].weight[w] /= n;
		}
	}

	//	解放
	scene->Destroy();
	manager->Destroy();

	Save(name2);
	Play(0);
}

//****************************************************************
//
//	頂点情報読み込み
//
//****************************************************************
//------------------------------------------------
//	座標読み込み
//------------------------------------------------
void FBXMesh::LoadPosition(FbxMesh* mesh) {
	int* index = mesh->GetPolygonVertices();
	FbxVector4* source = mesh->GetControlPoints();
	// メッシュのトランスフォーム

	FbxVector4 T = mesh->GetNode()->GetGeometricTranslation(FbxNode::eSourcePivot);
	FbxVector4 R = mesh->GetNode()->GetGeometricRotation(FbxNode::eSourcePivot);
	FbxVector4 S = mesh->GetNode()->GetGeometricScaling(FbxNode::eSourcePivot);
	FbxAMatrix TRS = FbxAMatrix(T, R, S);



	//	全頂点変換
	for (int v = 0; v < mesh->GetControlPointsCount(); v++) {
		source[v] = TRS.MultT(source[v]);
	}

	// 頂点座標読み込み
	int num = mesh->GetPolygonVertexCount();
	for (int v = 0; v < num; v++) {
		int vindex = index[v];

		vertices[v + numVertices].x = (float)source[vindex][0];
		vertices[v + numVertices].y = (float)source[vindex][1];
		vertices[v + numVertices].z = (float)source[vindex][2];

		vertices[v + numVertices].tu = 0;
		vertices[v + numVertices].tv = 0;
		vertices[v + numVertices].color = this->vertexColor;
	}
}

//------------------------------------------------
//	法線読み込み
//------------------------------------------------
void FBXMesh::LoadNormal(FbxMesh* mesh) {
	FbxArray<FbxVector4> normal;
	mesh->GetPolygonVertexNormals(normal);
	for (int v = 0; v < normal.Size(); v++)
	{
		vertices[v + numVertices].nx = (float)normal[v][0];
		vertices[v + numVertices].ny = (float)normal[v][1];
		vertices[v + numVertices].nz = (float)normal[v][2];
	}
}

//------------------------------------------------
//	ＵＶ読み込み
//------------------------------------------------
void FBXMesh::LoadUV(FbxMesh* mesh) {
	FbxStringList names;
	mesh->GetUVSetNames(names);
	FbxArray<FbxVector2> uv;
	mesh->GetPolygonVertexUVs(names.GetStringAt(0), uv);
	for (int v = 0; v < uv.Size(); v++) {
		vertices[v + numVertices].tu = (float)(uv[v][0]);
		vertices[v + numVertices].tv = (float)(1.0 - uv[v][1]);
	}
}

//------------------------------------------------
//	頂点カラー読み込み
//------------------------------------------------
void FBXMesh::LoadVertexColor(FbxMesh* mesh) {

	int vColorLayerCount = mesh->GetElementVertexColorCount();
	if (mesh->GetElementVertexColorCount() <= 0) return;
	//    頂点カラーレイヤー取得
	FbxGeometryElementVertexColor* element = mesh->GetElementVertexColor(0);

	//  保存形式の取得
	FbxGeometryElement::EMappingMode mapmode = element->GetMappingMode();
	FbxGeometryElement::EReferenceMode refmode = element->GetReferenceMode();

	//    ポリゴン頂点に対するインデックス参照形式のみ対応
	if (mapmode == FbxGeometryElement::eByPolygonVertex)
	{
		if (refmode == FbxGeometryElement::eIndexToDirect)
		{
			FbxLayerElementArrayTemplate<int>* index = &element->GetIndexArray();
			int indexCount = index->GetCount();
			for (int j = 0; j < indexCount; j++) {
				// FbxColor取得
				FbxColor c = element->GetDirectArray().GetAt(index->GetAt(j));
				vertices[j + numVertices].color = this->vertexColor;
			}
		}
	}
}
//****************************************************************
//	材質読み込み
//****************************************************************
void FBXMesh::LoadMaterial(int index, FbxSurfaceMaterial * material)
{
	FbxProperty prop = material->FindProperty(FbxSurfaceMaterial::sDiffuse);

	//	テクスチャ読み込み
	const char* path = NULL;
	int fileTextureCount = prop.GetSrcObjectCount<FbxFileTexture>();
	if (fileTextureCount > 0) {
		FbxFileTexture* FileTex = prop.GetSrcObject<FbxFileTexture>(0);
		path = FileTex->GetFileName();
	}
	else {
		int numLayer = prop.GetSrcObjectCount<FbxLayeredTexture>();
		if (numLayer > 0) {
			FbxLayeredTexture* LayerTex = prop.GetSrcObject<FbxLayeredTexture>(0);
			FbxFileTexture* FileTex = LayerTex->GetSrcObject<FbxFileTexture>(0);
			path = FileTex->GetFileName();
		}
	}
	if (path == NULL) return;

	//  C:\\AAA\\BBB\\a.fbx  C:/AAA/BBB/a.fbx
	const char* name = &path[strlen(path)];
	for (int i = 0; i < (int)strlen(path); i++)
	{
		name--;
		if (name[0] == '/') { name++; break; }
		if (name[0] == '\\') { name++; break; }
	}
	char work[128];
	strcpy(work, fbxDir);		//"AAA/BBB/";
								//	strcat(work, "texture/");	//"AAA/BBB/texture/"
	strcat(work, name);			//"AAA/BBB/texture/a.png

	char filename[128];
	strcpy(filename, work);
	textures[index] = LoadTexture(filename);




	//	テクスチャ名保存
	strcpy(textureName[index], name);
}

//****************************************************************
//	ボーン検索
//****************************************************************
int FBXMesh::FindBone(const char* name)
{
	int bone = -1; // 見つからない
	for (int i = 0; i < numBone; i++) {
		if (strcmp(name, boneArray[i].name) == 0) {
			bone = i;
			break;
		}
	}
	return bone;
}

void FBXMesh::LoadBone(FbxMesh* mesh)
{
	//	メッシュ頂点数
	int num = mesh->GetPolygonVertexCount();

	//	スキン情報の有無
	int skinCount = mesh->GetDeformerCount(FbxDeformer::eSkin);
	if (skinCount <= 0) {
		LoadMeshAnim(mesh);
		return;
	}
	FbxSkin* skin = static_cast<FbxSkin*>(mesh->GetDeformer(0, FbxDeformer::eSkin));
	//	ボーン数
	int nBone = skin->GetClusterCount();
	//	全ボーン情報取得
	for (int bone = 0; bone < nBone; bone++)
	{
		//	ボーン情報取得
		FbxCluster* cluster = skin->GetCluster(bone);
		FbxAMatrix trans;
		cluster->GetTransformMatrix(trans);
		/*		trans.mData[0][1] *= -1;
		trans.mData[0][2] *= -1;
		trans.mData[1][0] *= -1;
		trans.mData[2][0] *= -1;
		trans.mData[3][0] *= -1;
		*/
		//	ボーン名取得
		const char* name = cluster->GetLink()->GetName();

		//	ボーン検索
		bool isNewBone = false;
		int boneNo = FindBone(name);
		if (boneNo < 0) {
			boneNo = numBone;
			numBone++;
			isNewBone = true;
		}
		if (isNewBone) {
			strcpy(boneArray[boneNo].name, name);
			//	オフセット行列作成
			FbxAMatrix LinkMatrix;
			cluster->GetTransformLinkMatrix(LinkMatrix);
			/*			LinkMatrix.mData[0][1] *= -1;
			LinkMatrix.mData[0][2] *= -1;
			LinkMatrix.mData[1][0] *= -1;
			LinkMatrix.mData[2][0] *= -1;
			LinkMatrix.mData[3][0] *= -1;
			*/
			FbxAMatrix Offset = LinkMatrix.Inverse() * trans;
			FbxDouble* OffsetM = (FbxDouble*)Offset;

			//	オフセット行列保存
			for (int i = 0; i < 16; i++) {
				boneArray[boneNo].offsetMatrix.m[i] = (float)OffsetM[i];
			}

			//	キーフレーム読み込み
			LoadKeyFrames(0, boneNo, cluster->GetLink());
		}

		//	ウェイト読み込み
		int wgtcount = cluster->GetControlPointIndicesCount();
		int* wgtindex = cluster->GetControlPointIndices();
		double* wgt = cluster->GetControlPointWeights();

		int* index = mesh->GetPolygonVertices();

		for (int i = 0; i < wgtcount; i++) {
			int wgtindex2 = wgtindex[i];
			//	全ポリゴンからwgtindex2番目の頂点検索
			for (int v = 0; v < num; v++) {
				if (index[v] != wgtindex2) continue;
				//	頂点にウェイト保存
				int w = weights[v + numVertices].count;
				if (w >= 4) {
					continue;
				}
				weights[v + numVertices].bone[w] = boneNo;
				weights[v + numVertices].weight[w] = (float)wgt[i];
				weights[v + numVertices].count++;
			}
		}
	}
}

//	ボーンのないメッシュのアニメーション
void FBXMesh::LoadMeshAnim(FbxMesh* mesh)
{
	FbxNode* node = mesh->GetNode();

	int boneNo = numBone;
	strcpy(boneArray[boneNo].name, node->GetName());

	//	オフセット行列（原点に移動させる行列）
	boneArray[boneNo].offsetMatrix.identity();

	//	キーフレーム読み込み
	LoadKeyFrames(0, boneNo, node);

	//	ウェイト設定
	int num = mesh->GetPolygonVertexCount();
	for (int i = 0; i < num; i++) {
		weights[i + numVertices].bone[0] = boneNo;
		weights[i + numVertices].weight[0] = 1.0f;
		weights[i + numVertices].count = 1;
	}

	numBone++;
}



//	キーフレーム読み込み
void FBXMesh::LoadKeyFrames(int name, int bone, FbxNode* bone_node) {
	//	メモリ確保
	Motion* M = &motion[name];
	M->key[bone] = new MATRIX[M->numFrame + 1];

	double time = StartFrame*(1.0 / 60);
	FbxTime T;
	for (int f = 0; f < motion[name].numFrame; f++) {
		T.SetSecondDouble(time);
		//	T秒の姿勢行列をGet
		FbxMatrix m = bone_node->EvaluateGlobalTransform(T);


		FbxDouble* mat = (FbxDouble*)m;
		for (int i = 0; i < 16; i++) {
			motion[name].key[bone][f].m[i] = (float)mat[i];
		}

		time += 1.0 / 60.0;
	}
}


//	ボーン行列の補間
void MatrixInterporate(MATRIX& out, MATRIX& A, MATRIX& B, float rate)
{
	out = A * (1.0f - rate) + B * rate;
}


//モーションのスキニング
void FBXMesh::Skinning()
{
	//補間する場合はモーション補間
	/*if (isBlend) {
		MotionLerp();
	}
	else {*/
		Motion* M = &motion[motionName];
		if (M == NULL) return;

		//	配列用変数
		int f = (int)nowFrame;
		//	行列準備
		MATRIX keyMatrix[256];

		for (int b = 0; b < numBone; b++)
		{
			//	行列補間
			MATRIX m;
			MatrixInterporate(m, M->key[b][f], M->key[b][f + 1], nowFrame - (int)nowFrame);
			boneArray[b].transform = m;
			//	キーフレーム
			keyMatrix[b] = boneArray[b].offsetMatrix * m;
		}



		//	頂点変形
		for (int v = 0; v < numVertices; v++)
		{
			//	頂点 * ボーン行列
			// b = v番目の頂点の影響ボーン[n]
			if (weights[v].count <= 0) continue;

			vertices[v].x = 0;
			vertices[v].y = 0;
			vertices[v].z = 0;

			//	影響個数分ループ
			for (int n = 0; n < weights[v].count; n++) {
				int b = weights[v].bone[n];

				float x = verticesSrc[v].x;
				float y = verticesSrc[v].y;
				float z = verticesSrc[v].z;
				//	座標を影響力分移動
				vertices[v].x += (x*keyMatrix[b]._11 + y*keyMatrix[b]._21 + z*keyMatrix[b]._31 + 1 * keyMatrix[b]._41)*weights[v].weight[n];
				vertices[v].y += (x*keyMatrix[b]._12 + y*keyMatrix[b]._22 + z*keyMatrix[b]._32 + 1 * keyMatrix[b]._42)*weights[v].weight[n];
				vertices[v].z += (x*keyMatrix[b]._13 + y*keyMatrix[b]._23 + z*keyMatrix[b]._33 + 1 * keyMatrix[b]._43)*weights[v].weight[n];

				float nx = verticesSrc[v].nx;
				float ny = verticesSrc[v].ny;
				float nz = verticesSrc[v].nz;
				//	法線を影響力分変換
				vertices[v].nx += (nx*keyMatrix[b]._11 + ny*keyMatrix[b]._21 + nz*keyMatrix[b]._31)*weights[v].weight[n];
				vertices[v].ny += (nx*keyMatrix[b]._12 + ny*keyMatrix[b]._22 + nz*keyMatrix[b]._32)*weights[v].weight[n];
				vertices[v].nz += (nx*keyMatrix[b]._13 + ny*keyMatrix[b]._23 + nz*keyMatrix[b]._33)*weights[v].weight[n];
			}
		}

		DxSystem::deviceContext->UpdateSubresource(vertexBuffer, 0, NULL, vertices, 0, 0);
	//}
}

static const float LERP_TIME = 10.f;
//モーション補間
void FBXMesh::MotionLerp()
{
	Motion* M = &motion[motionNameOld];
	Motion* M2 = &motion[motionName];

	if (M == NULL) return;

	//	配列用変数
	int f = (int)nowFrame;
	//	行列準備
	MATRIX keyMatrix[256];

	if (++blendTime > LERP_TIME) {
		isBlend = false;
		blendTime = 0;
	}

	for (int b = 0; b < numBone; b++)
	{
		//	行列補間
		MATRIX m;

		MatrixInterporate(m, M->key[b][f], M2->key[b][1], (float)blendTime / LERP_TIME);
		boneArray[b].transform = m;
		//	キーフレーム
		keyMatrix[b] = boneArray[b].offsetMatrix * m;
	}



	//	頂点変形
	for (int v = 0; v < numVertices; v++)
	{
		//	頂点 * ボーン行列
		// b = v番目の頂点の影響ボーン[n]
		if (weights[v].count <= 0) continue;

		vertices[v].x = 0;
		vertices[v].y = 0;
		vertices[v].z = 0;

		//	影響個数分ループ
		for (int n = 0; n < weights[v].count; n++) {
			int b = weights[v].bone[n];

			float x = verticesSrc[v].x;
			float y = verticesSrc[v].y;
			float z = verticesSrc[v].z;
			//	座標を影響力分移動
			vertices[v].x += (x*keyMatrix[b]._11 + y*keyMatrix[b]._21 + z*keyMatrix[b]._31 + 1 * keyMatrix[b]._41)*weights[v].weight[n];
			vertices[v].y += (x*keyMatrix[b]._12 + y*keyMatrix[b]._22 + z*keyMatrix[b]._32 + 1 * keyMatrix[b]._42)*weights[v].weight[n];
			vertices[v].z += (x*keyMatrix[b]._13 + y*keyMatrix[b]._23 + z*keyMatrix[b]._33 + 1 * keyMatrix[b]._43)*weights[v].weight[n];

			float nx = verticesSrc[v].nx;
			float ny = verticesSrc[v].ny;
			float nz = verticesSrc[v].nz;
			//	法線を影響力分変換
			vertices[v].nx += (nx*keyMatrix[b]._11 + ny*keyMatrix[b]._21 + nz*keyMatrix[b]._31)*weights[v].weight[n];
			vertices[v].ny += (nx*keyMatrix[b]._12 + ny*keyMatrix[b]._22 + nz*keyMatrix[b]._32)*weights[v].weight[n];
			vertices[v].nz += (nx*keyMatrix[b]._13 + ny*keyMatrix[b]._23 + nz*keyMatrix[b]._33)*weights[v].weight[n];
		}
	}

	DxSystem::deviceContext->UpdateSubresource(vertexBuffer, 0, NULL, vertices, 0, 0);

}

//****************************************************************
//	モーション追加
//****************************************************************
void FBXMesh::AddMotion(int name, const char * filename)
{
	for (int i = 0; i < 32; i++) {
		textureName[i][0] = '\0';
	}
	//	最適化ファイル確認
	char name2[256];
	strcpy(name2, filename);
	name2[strlen(name2) - 3] = 'M';
	name2[strlen(name2) - 2] = 'A';
	name2[strlen(name2) - 1] = 'Y';
	if (LoadMAYAnim(name, name2)) return;



	FbxManager* manager = FbxManager::Create();
	FbxScene* scene = FbxScene::Create(manager, "");
	//	ファイルからシーンに読み込み
	FbxImporter* importer = FbxImporter::Create(manager, "");
	importer->Initialize(filename);
	importer->Import(scene);
	importer->Destroy();

	//	モーション情報取得
	FbxArray<FbxString*> names;
	scene->FillAnimStackNameArray(names);

	FbxTakeInfo* take = scene->GetTakeInfo(names[0]->Buffer());
	FbxLongLong start = take->mLocalTimeSpan.GetStart().Get();
	FbxLongLong stop = take->mLocalTimeSpan.GetStop().Get();
	FbxLongLong fps60 = FbxTime::GetOneFrameValue(FbxTime::eFrames60);

	StartFrame = (int)(start / fps60);
	motion[name].numFrame = (int)((stop - start) / fps60);
	//	ルートノード取得
	FbxNode* root = scene->GetRootNode();


	//	全ボーン読み込み
	for (int b = 0; b < numBone; b++) {
		//	ボーンノード検索
		FbxNode* bone = root->FindChild(boneArray[b].name);

		if (bone == NULL) continue;

		//	キーフレーム読み込み
		LoadKeyFrames(name, b, bone);

	}
	//	解放
	scene->Destroy();
	manager->Destroy();


	SaveAnim(name, name2);

}


//****************************************************************
//	テクスチャ張替え
//****************************************************************
void FBXMesh::SetTexture(Texture * tex)
{
	textures[0] = tex;
}



void FBXMesh::Save(const char* filename)
{
	FILE* fp = fopen(filename, "wb");

	//	頂点数保存
	fwrite(&numVertices, sizeof(numVertices), 1, fp);
	fwrite(verticesSrc, sizeof(PolygonVertex), numVertices, fp);

	//	インデックス数保存
	fwrite(&numFaces, sizeof(numFaces), 1, fp);
	fwrite(indices, sizeof(DWORD), numFaces * 3, fp);

	//	メッシュ数保存
	fwrite(&numMesh, sizeof(int), 1, fp);

	//	使用材質保存
	fwrite(meshMaterial, sizeof(int), numMesh, fp);
	fwrite(materialFaces, sizeof(int), numMesh, fp);

	//	テクスチャ名保存
	fwrite(textureName, 128, 32, fp);

	//	ボーン保存
	fwrite(&numBone, sizeof(int), 1, fp);
	fwrite(boneArray, sizeof(BONE), numBone, fp);

	//	ウェイト保存
	fwrite(weights, sizeof(WEIGHT), numVertices, fp);

	fclose(fp);
}

bool FBXMesh::LoadMAY(const char* filename)
{
	FILE* fp = fopen(filename, "rb");
	if (fp == NULL) return false;

	//	頂点読み込み
	fread(&numVertices, sizeof(numVertices), 1, fp);

	vertices = new PolygonVertex[numVertices];
	fread(vertices, sizeof(PolygonVertex), numVertices, fp);
	//	インデックス読み込み
	fread(&numFaces, sizeof(numFaces), 1, fp);

	indices = new DWORD[numFaces * 3];
	fread(indices, sizeof(DWORD), numFaces * 3, fp);

	//	メッシュ数読み込み
	fread(&numMesh, sizeof(int), 1, fp);

	materialFaces = new int[numMesh];
	meshMaterial = new int[numMesh];

	//	使用材質読み込み
	fread(meshMaterial, sizeof(int), numMesh, fp);
	fread(materialFaces, sizeof(int), numMesh, fp);

	//	テクスチャ名読み込み
	fread(textureName, 128, 32, fp);
	textures = new Texture *[numMesh];

	for (int i = 0; i < numMesh; i++) {
		if (textureName[i][0] == '\0') {
			textures[i] = NULL;
			continue;
		}
		char work[128];
		strcpy(work, fbxDir);		//"AAA/BBB/";
		strcat(work, textureName[i]);			//"AAA/BBB/texture/a.png

		char filename[128];
		strcpy(filename, work);
		textures[i] = new Texture();
		textures[i]->Load(filename);
	}

	//	ボーン読み込み
	fread(&numBone, sizeof(int), 1, fp);
	fread(boneArray, sizeof(BONE), numBone, fp);

	//	ウェイト読み込み
	weights = new WEIGHT[numVertices];
	fread(weights, sizeof(WEIGHT), numVertices, fp);

	//	頂点元データ保存
	verticesSrc = new PolygonVertex[numVertices];
	memcpy(verticesSrc, vertices, sizeof(PolygonVertex) * numVertices);


	fclose(fp);
	return true;
}


//アニメーションをバイナリに変換して保存
void FBXMesh::SaveAnim(int name, const char* filename)
{
	FILE* fp = fopen(filename, "wb");


	//モーションのフレーム数保存
	fwrite(&motion[name].numFrame, sizeof(int), 1, fp);

	//キーフレーム保存
	for (int bone = 0; bone < numBone; bone++) {
		//フレームごとに保存
		for (int f = 0; f < motion[name].numFrame; f++) {
			//4*4行列なので16回ループ
			for (int i = 0; i < 16; i++)
				fwrite(&motion[name].key[bone][f].m[i], sizeof(float), 1, fp);
		}

	}



	fclose(fp);
}

//バイナリ化したFBXを読み込み
bool FBXMesh::LoadMAYAnim(int name, const char* filename)
{

	FILE* fp = fopen(filename, "rb");
	if (fp == NULL) return false;

	//モーションのフレーム数保存
	fread(&motion[name].numFrame, sizeof(int), 1, fp);

	int Num = motion[name].numFrame;

	//キーフレーム読み込み
	for (int bone = 0; bone < numBone; bone++) {
		//ボーンの数だけ領域確保
		motion[name].key[bone] = new MATRIX[motion[name].numFrame + 1];
		//フレームごとに読み込み
		for (int f = 0; f < motion[name].numFrame; f++) {
			//バイナリから読み込み4*4行列なので16回ループ
			for (int i = 0; i < 16; i++)
				fread(&motion[name].key[bone][f].m[i], sizeof(float), 1, fp);

		}
	}
	fclose(fp);
	return true;
}


VECTOR2 ScreenToClip(VECTOR2 pos) {


	//スクリーン座標をクリップ空間へ変換
	return VECTOR2(pos.x* 2.0f / DxSystem::SCREEN_WIDTH - 1.0f,
		1.0f - pos.y * 2.0f / DxSystem::SCREEN_HEIGHT);

}
//****************************************************************
//	頂点最適化
//****************************************************************
void FBXMesh::OptimizeVertices()
{
	int currentNum = 0;
	for (int v = 0; v < numVertices; v++) {
		int sameIndex = -1;
		//	同一頂点検索
		for (int old = 0; old < currentNum; old++) {
			if (vertices[v].x != vertices[old].x) continue;
			if (vertices[v].y != vertices[old].y) continue;
			if (vertices[v].z != vertices[old].z) continue;
			if (vertices[v].nx != vertices[old].nx) continue;
			if (vertices[v].ny != vertices[old].ny) continue;
			if (vertices[v].nz != vertices[old].nz) continue;
			if (vertices[v].tu != vertices[old].tu) continue;
			if (vertices[v].tv != vertices[old].tv) continue;

			sameIndex = old;
			break;
		}

		int target = v;
		if (sameIndex == -1) {
			//	新規頂点
			CopyMemory(&vertices[currentNum], &vertices[v], sizeof(PolygonVertex));
			CopyMemory(&weights[currentNum], &weights[v], sizeof(WEIGHT));
			target = currentNum;
			currentNum++;
		}
		else {
			target = sameIndex;
		}
		//	インデックス更新
		for (int i = 0; i < numVertices; i++) {
			if (indices[i] == v) indices[i] = target;
		}
	}

	//	新バッファ確保
	PolygonVertex* buf = new PolygonVertex[currentNum];
	CopyMemory(buf, vertices, sizeof(PolygonVertex) * currentNum);
	numVertices = currentNum;

	delete[] vertices;
	vertices = buf;
}

