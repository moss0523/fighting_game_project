//******************************************************************************
//
//
//      入力マネージャ
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "../Headers/InputManager.h"

#include <Keyboard.h>
#include <GamePad.h>


using namespace Input;
using DirectX::Keyboard;

//------< データ >-----------------------------------------------------------

// キー割り当てデータ
PadAssign keyAssign00[] = {

	{ PAD_UP       , Keyboard::Up },            // VK_UP
	{ PAD_DOWN     , Keyboard::Down },          // VK_DOWN
	{ PAD_LEFT     , Keyboard::Left },          // VK_LEFT
	{ PAD_RIGHT    , Keyboard::Right },         // VK_RIGHT
	{ PAD_START    , Keyboard::Enter },         // VK_RETURN
	{ PAD_SELECT   , Keyboard::Space },         // VK_BACK
	{ PAD_TRG1     , Keyboard::Z },             // 'Z'
	{ PAD_TRG2     , Keyboard::X },             // 'X'
	{ PAD_TRG3     , Keyboard::C },             // 'C'
	{ PAD_TRG4     , Keyboard::V },             // 'V'
	{ PAD_L1        , Keyboard::B },     // Lボタン
	{ PAD_R1        , Keyboard::N },     // Rボタン
	{ PAD_R2        , Keyboard::M },     // Rボタン

	{ KEY_W		   , Keyboard::W },             
	{ KEY_R		   , Keyboard::R }, 
	{ KEY_D		   , Keyboard::D },
	{ KEY_S		   , Keyboard::S },
	{ KEY_DELETE   , Keyboard::Delete },
	{ KEY_TAB      , Keyboard::Tab },

	{ ARTS_1   , Keyboard::F1 },
	{ ARTS_2   , Keyboard::F2 },
	{ ARTS_3   , Keyboard::F3 },
	  
	{ RIGHT_CLICK  ,VK_RBUTTON },         
	{ LEFT_CLICK   ,VK_LBUTTON },         
	{ CENTER_CLICK ,VK_MBUTTON },         
	{ SHIFT_KEY    ,Keyboard::LeftShift },      // VK_BACK

	
	{ 0x00, 0x00 }                              // 終了コード
};

// ジョイスティック割り当てデータ
PadAssign joyAssign00[] = {
	{ PAD_UP        , GamePad::UP },            // 上キー
	{ PAD_DOWN      , GamePad::DOWN },          // 下キー
	{ PAD_LEFT    	, GamePad::LEFT },          // 左キー
	{ PAD_RIGHT     , GamePad::RIGHT },         // 右キー

	{ PAD_START     , GamePad::START },         // スタートボタン
	{ PAD_TRG1      , GamePad::X },             // Aボタン
	{ PAD_TRG2      , GamePad::Y },             // Bボタン
	{ PAD_TRG3      , GamePad::B },             // Xボタン
	{ PAD_TRG4      , GamePad::A },             // Yボタン
	{ PAD_L1        , GamePad::LSHOULDER },     // Lボタン
	{ PAD_R1        , GamePad::RSHOULDER },     // Rボタン
	
	{ PAD_SELECT    , GamePad::BACK },          // バック（セレクト）ボタン

	{ 0x00, 0x00 }                              // 終了コード
};

// キー割り当てデータ
PadAssign keyAssign01[] = {
	{ PAD_UP        , Keyboard::NumPad8 },            // VK_W
	{ PAD_DOWN      , Keyboard::NumPad2 },            // VK_S
	{ PAD_LEFT      , Keyboard::NumPad4 },            // VK_A
	{ PAD_RIGHT     , Keyboard::NumPad6 },            // VK_D

	{ PAD_TRG1      , Keyboard::J },            // 'J'
	{ PAD_TRG2      , Keyboard::K },            // 'K'
	{ PAD_TRG3      , Keyboard::L },            // 'L'
	{ PAD_TRG4      , Keyboard::G },            // 'L'

	{ PAD_SELECT    , Keyboard::F1 },           // VK_F1

	{ ARTS_1   , Keyboard::F4 },
	{ ARTS_2   , Keyboard::F5 },
	{ ARTS_3   , Keyboard::F6 },
												//      { PAD_START     , VK_LBUTTON },             // マウス左ボタン
												//      { PAD_SELECT    , VK_RBUTTON },             // マウス右ボタン
												//      { PAD_TRG1      , VK_MBUTTON },             // マウス中ボタン

	{ 0x00, 0x00 }                              // 終了コード
};

//--------------------------------
//  キー割り当ての設定
//--------------------------------
void InputManager::SetKeyAssign(int no, PadAssign *data)
{
	pad[no].keyAssign = data;
}

//--------------------------------
//  ジョイスティック割り当ての設定
//--------------------------------
void InputManager::SetJoyAssign(int no, PadAssign *data)
{
	pad[no].joyAssign = data;
}

//--------------------------------
//  デストラクタ
//--------------------------------
InputManager::~InputManager()
{
	if (keyboard)delete keyboard;
	if (gamePad)delete gamePad;
}

//--------------------------------
//  初期化
//--------------------------------
void InputManager::Init()
{
	keyboard = new DirectX::Keyboard;
	gamePad = new DirectX::GamePad;

	// コントローラ0の設定
	SetKeyAssign(0, keyAssign00);
	SetJoyAssign(0, joyAssign00);

	// コントローラ1の設定
	SetKeyAssign(1, keyAssign01);
	SetJoyAssign(1, joyAssign00);

	
}

//--------------------------------
//  入力情報の更新
//--------------------------------
void InputManager::Update(HWND hwnd)
{
	// キーボードの状態取得
	DirectX::Keyboard::State kb = keyboard->GetState();


	DirectX::GamePad::State gpad[GAMEPAD_NUM] = {};

	// ゲームパッドの状態取得
	for (int i = 0; i < GAMEPAD_NUM; i++)
		gpad[i] = gamePad->GetState(i);

	// 
	for (int i = 0; i < GAMEPAD_NUM; i++)
	{
		PadState* p = &pad[i];

		int old_state = p->state;

		// pad_stateの更新
		p->state = 0;

		// キーボード・マウス
		PadAssign *assign = p->keyAssign;
		if (assign)
		{
			while (assign->bit)
			{
				if ((assign->code == VK_LBUTTON) ||
					(assign->code == VK_RBUTTON) ||
					(assign->code == VK_MBUTTON))
				{	// Mouse
					if (GetAsyncKeyState(assign->code) < 0) p->state |= assign->bit;
				}
				else
				{	// Keyboard
					if (kb.IsKeyDown((Keyboard::Keys)assign->code)) p->state |= assign->bit;
				}

				assign++;
			}
		}

		// ジョイスティック
		assign = p->joyAssign;
		if (assign && gpad[i].IsConnected())
		{
			while (assign->bit)
			{
				struct hoge_t { bool b[10]; } hoge;
				if (assign->code >= GamePad::A)
				{	// buttons
					hoge = *(hoge_t*)&gpad[i].buttons;
					if (hoge.b[assign->code - GamePad::A]) p->state |= assign->bit;
				}
				else
				{	// dpad
					hoge = *(hoge_t*)&gpad[i].dpad;
					if (hoge.b[assign->code]) p->state |= assign->bit;
				}

				assign++;
			}

			// 左スティック
			p->leftX = gpad[i].thumbSticks.leftX;
			p->leftY = gpad[i].thumbSticks.leftY;

			// 右スティック
			p->rightX = gpad[i].thumbSticks.rightX;
			p->rightY = gpad[i].thumbSticks.rightY;

			// 左右トリガー
			p->left = gpad[i].triggers.left;
			p->right = gpad[i].triggers.right;

			// 遊びの設定
			// ※必要があれば設定する
		}

		// p->triggerの更新
		p->trigger = (~old_state) & p->state;

		// p->triggerUpの更新
		p->triggerUp = old_state & (~p->state);

		// p->repeatの計算
		p->repeat = p->state;
		for (int j = 8 - 1; j > 0; --j) {
			p->old[j] = p->old[j - 1];				// データをひとつずらす
			p->repeat &= p->old[j];					// repeatを計算する
		}
		p->old[0] = p->state & (~p->repeat);		// 最新のデータを保存
		p->repeat |= p->trigger;					// 本来のトリガー入力も設定する
	}

	// マウス座標の取得
	GetCursorPos(&mousePos);
	ScreenToClient(hwnd, &mousePos);
}

//--------------------------------
//  マウスカーソルのx座標を取得
//--------------------------------
int InputManager::GetCursorPosX()
{
	return mousePos.x;
}

//--------------------------------
//  マウスカーソルのy座標を取得
//--------------------------------
int InputManager::GetCursorPosY()
{
	return mousePos.y;
}

//--------------------------------
//  マウスカーソルの座標を取得
//--------------------------------
VECTOR2 InputManager::GetCursorPosXY()
{
	return VECTOR2((float)mousePos.x, (float)mousePos.y);
}
//--------------------------------
//  マウスカーソルの変化を取得
//--------------------------------
VECTOR2 InputManager::GetDeltaPos()
{ 
	return GetCursorPosXY() - oldCursorPos;
}



//******************************************************************************

//ガード用に後ろ歩き
 int Input::PAD_BACK(int dir)
{
	
		switch (dir)
		{
		case 1:
			return PAD_LEFT;

			break;

		case -1:
			return PAD_RIGHT;

			break;

		}

		return 0;
}



