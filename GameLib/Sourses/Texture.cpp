﻿#include <d3d11.h>
#include "../Headers/Texture.h"
#include "../Math/Vector.h"



Texture::Texture()
{
}


Texture::~Texture()
{
	if (ShaderResourceView)ShaderResourceView->Release();
	if (SamplerState)SamplerState->Release();

}

//テクスチャ読み込み
bool Texture::Load(const char* filename)
{
	wchar_t	wchar[256];
	size_t wLen = 0;
	errno_t err = 0;

	//変換
	mbstowcs_s(&wLen, wchar, 256, filename, _TRUNCATE);

	// 画像ファイル読み込み DirectXTex
	DirectX::TexMetadata metadata;
	DirectX::ScratchImage image;



	HRESULT hr;

	//読み込み(TGAの場合は例外処理)
	if (_strcmpi(&filename[strlen(filename) - 3], "TGA") == 0) {
		hr = LoadFromTGAFile(wchar, &metadata, image);
	}
	else {
		hr = LoadFromWICFile(wchar, 0, &metadata, image);
	}

	if (FAILED(hr)) {
		return false;
	}
	//縦幅横幅取得
	width = metadata.width;
	height = metadata.height;

	// 画像からシェーダリソースView
	hr = CreateShaderResourceView(DxSystem::device, image.GetImages(), image.GetImageCount(), metadata, &ShaderResourceView);
	if (FAILED(hr)) {
		return false;
	}

	// SamplerState作成
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = DxSystem::device->CreateSamplerState(&sampDesc, &SamplerState);
	
	if (FAILED(hr)) {
		return false;
	}

	return true;
}

//テクスチャセット
void Texture::Set(int slot)
{
	DxSystem::deviceContext->PSSetShaderResources(slot, 1, &ShaderResourceView);
	DxSystem::deviceContext->PSSetSamplers(slot, 1, &SamplerState);
}




