#include "../Headers/DxSystem.h"
#include "../Headers/DxShader.h"

Shader::~Shader()
{
	if (vertexLayout) { vertexLayout->Release(); vertexLayout = NULL; }
	if (vertexShader) { vertexShader->Release(); vertexShader = NULL; }
	if (pixelShader) { pixelShader->Release(); pixelShader = NULL; }

}

//****************************************************************
//
//シェーダークラス(fx)
//
//****************************************************************
//------------------------------------------------
//	シェーダー単体コンパイル
//------------------------------------------------
HRESULT Shader::Compile(WCHAR* filename, LPCSTR method, LPCSTR shaderModel, ID3DBlob** blobOut)
{
	DWORD ShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
	ID3DBlob* BlobError = NULL;
	// コンパイル
	HRESULT hr = D3DCompileFromFile(
		filename,
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		method,
		shaderModel,
		ShaderFlags,
		0,
		blobOut,
		&BlobError
	);

	// エラー出力
	if (BlobError != NULL)
	{
		OutputDebugStringA((char*)BlobError->GetBufferPointer());
		BlobError->Release();
		BlobError = NULL;
	}

	return hr;
}

//------------------------------------------------
//	シェーダーセットコンパイル
//------------------------------------------------
bool Shader::Create(WCHAR* filename, LPCSTR vertexShaderFunc, LPCSTR pixelShaderFunc)
{
	HRESULT hr = S_OK;

	ID3DBlob* vertexShaderBlob = NULL;
	// 頂点シェーダ
	hr = Compile(filename, vertexShaderFunc, "vs_4_0", &vertexShaderBlob);
	if (FAILED(hr))
	{
		return false;
	}

	// 頂点シェーダ生成
	hr = DxSystem::device->CreateVertexShader(
		vertexShaderBlob->GetBufferPointer(),
		vertexShaderBlob->GetBufferSize(), 
		NULL, 
		&vertexShader);

	if (FAILED(hr))
	{
		vertexShaderBlob->Release();
		return false;
	}

	// 入力レイアウト
	D3D11_INPUT_ELEMENT_DESC layout3D[] = {
		{ "POSITION",	0, DXGI_FORMAT_R32G32B32_FLOAT,		0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",		0, DXGI_FORMAT_R32G32B32_FLOAT,		0, 4 * 3, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0, 4 * 6, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, 4 * 8, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT numElements = ARRAYSIZE(layout3D);

	// 入力レイアウト生成
	hr = DxSystem::device->CreateInputLayout(
		layout3D,
		numElements,
		vertexShaderBlob->GetBufferPointer(),
		vertexShaderBlob->GetBufferSize(),
		&vertexLayout
	);

	vertexShaderBlob->Release();
	if (FAILED(hr))
	{
		return false;
	}

	// 入力レイアウト設定
	DxSystem::deviceContext->IASetInputLayout(vertexLayout);


	// ピクセルシェーダ
	ID3DBlob*pixelShaderBlob = NULL;
	hr = Compile(filename, pixelShaderFunc, "ps_4_0", &pixelShaderBlob);
	if (FAILED(hr))
	{
		return false;
	}

	// ピクセルシェーダ生成
	hr = DxSystem::device->CreatePixelShader(
		pixelShaderBlob->GetBufferPointer(),
		pixelShaderBlob->GetBufferSize(),
		NULL, 
		&pixelShader);

	pixelShaderBlob->Release();
	if (FAILED(hr))
	{
		return false;
	}

	return true;
}


//------------------------------------------------
//	有効化
//------------------------------------------------
void Shader::Activate()
{
	DxSystem::deviceContext->VSSetShader(vertexShader, NULL, 0);
	DxSystem::deviceContext->PSSetShader(pixelShader, NULL, 0);
}

