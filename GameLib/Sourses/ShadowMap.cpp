#include "../GameLib/Headers/ShadowMap.h"
#include "../GameLib/Headers/DxSystem.h"


//***************************************
//
//		シャドウマップクラス
//
//***************************************

ShadowMap::ShadowMap()
{
}


ShadowMap::~ShadowMap()
{
	if(rTVShadowMap)
		rTVShadowMap->Release();
	if (sRVShadowMap)
		sRVShadowMap->Release();
	if (dSVShadowMap)
		dSVShadowMap->Release();
	if (shadowMapTex)
		shadowMapTex->Release();
	if (shadowMapDepth)
		shadowMapDepth->Release();
	if (cBShadowMap)
		cBShadowMap->Release();
}


void ShadowMap::Initialize()
{
	shaderShadowMap.Create(L"Shaders/ShadowMap.fx", "VSMain", "PSMain");

	{
		// レンダーターゲット設定
		D3D11_TEXTURE2D_DESC td;
		ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
		td.Width = 2048;
		td.Height = 2048;
		td.MipLevels = 1;
		td.ArraySize = 1;
		td.Format = DXGI_FORMAT_R32_FLOAT;
		td.SampleDesc.Count = 1;
		td.SampleDesc.Quality = 0;
		td.Usage = D3D11_USAGE_DEFAULT;
		td.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		td.CPUAccessFlags = 0;
		td.MiscFlags = 0;
		// テクスチャ生成
		HRESULT hr = DxSystem::device->CreateTexture2D(&td, NULL, &shadowMapTex);
		if (FAILED(hr)) return;

		//	レンダーターゲットビュー
		D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
		memset(&rtvDesc, 0, sizeof(rtvDesc));
		rtvDesc.Format = DXGI_FORMAT_R32_FLOAT;
		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		hr = DxSystem::device->CreateRenderTargetView(shadowMapTex, &rtvDesc, &rTVShadowMap);

		// シェーダリソースビューの設定
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		memset(&srvDesc, 0, sizeof(srvDesc));
		srvDesc.Format = rtvDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels = 1;
		// シェーダリソースビューの生成
		hr = DxSystem::device->CreateShaderResourceView(shadowMapTex, &srvDesc, &sRVShadowMap);
		if (FAILED(hr)) return;

	}

	// 深度ステンシル設定
	{
		// 深度ステンシル設定
		D3D11_TEXTURE2D_DESC td;
		ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
		td.Width = 2048;
		td.Height = 2048;
		td.MipLevels = 1;
		td.ArraySize = 1;
		td.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		td.SampleDesc.Count = 1;
		td.SampleDesc.Quality = 0;
		td.Usage = D3D11_USAGE_DEFAULT;
		td.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		td.CPUAccessFlags = 0;
		td.MiscFlags = 0;

		// 深度ステンシルテクスチャ生成
		HRESULT hr = DxSystem::device->CreateTexture2D(&td, NULL, &shadowMapDepth);
		if (FAILED(hr)) return;

		// 深度ステンシルビュー設定
		D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
		ZeroMemory(&dsvd, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
		dsvd.Format = td.Format;
		dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		dsvd.Texture2D.MipSlice = 0;

		// 深度ステンシルビュー生成
		hr = DxSystem::device->CreateDepthStencilView(shadowMapDepth, &dsvd, &dSVShadowMap);
		if (FAILED(hr)) return;
	}

	// 定数バッファ生成
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.ByteWidth = sizeof(CBShadowMapParam);
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	DxSystem::device->CreateBuffer(&bd, NULL, &cBShadowMap);
	DxSystem::deviceContext->VSSetConstantBuffers(2, 1, &cBShadowMap);
	DxSystem::deviceContext->PSSetConstantBuffers(2, 1, &cBShadowMap);

}

void ShadowMap::Activate(
	const VECTOR3 & center,
	const VECTOR3 & lightVec)
{
	shaderShadowMap.Activate();
	
	MATRIX shadowView;
	shadowView.LookAt(
		center - lightVec*50,
		center
	);
	//距離のレンダリング用の平行投影行列(1-100)
	MATRIX shadowProj;
	shadowProj.Ortho(
		20,20, 1,200 );
	// シャドウマップ用コンスタントバッファ
	CBShadowMapParam cb;
	
	cb.shadowVP =
		shadowView * shadowProj;

	DxSystem::deviceContext->UpdateSubresource(cBShadowMap, 0, NULL,&cb, 0, 0);

	// レンダーターゲットビュー設定
	DxSystem::deviceContext->
		OMSetRenderTargets(1,
			&rTVShadowMap,
			dSVShadowMap);

	float clearDepth[4] = { 0.5f, 0.5f, 0.5f, 1.0f };

	//レンダーターゲットと深度ステンシルのクリア
	DxSystem::deviceContext->ClearRenderTargetView(rTVShadowMap, clearDepth);
	DxSystem::deviceContext->ClearDepthStencilView(dSVShadowMap, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	//８番スロットにシャドウマップの情報を渡す
	DxSystem::deviceContext->PSSetShaderResources(8,1,&sRVShadowMap);

	// ビューポート設定
	D3D11_VIEWPORT vp = {
		0,0,2048,2048, 0,1 };
	DxSystem::deviceContext->
		RSSetViewports(1, &vp);
}

//シャドウマップの再登録
void ShadowMap::Deactivate() {

	//８番スロットにシャドウマップの情報を渡す
	DxSystem::deviceContext->PSSetShaderResources(8, 1, &sRVShadowMap);

}

