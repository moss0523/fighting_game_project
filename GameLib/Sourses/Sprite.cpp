#include "../Headers/DxSystem.h"
#include "../Headers/Sprite.h"


//***************************************
//
//		スプライト描画クラス
//
//***************************************

Sprite::Sprite(const char* filename)//コンストラクタ
{

	//頂点情報設定
	VERTEX v[4] = {};

	//	頂点バッファ作成
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(v);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA res;
	ZeroMemory(&res, sizeof(res));
	res.pSysMem = v;

	DxSystem::device->CreateBuffer(&bd, &res, &vertexBuffer);

	//	テクスチャ読み込み

	if (filename) {
		texture = new Texture();
		texture->Load(filename);
	}

	//深度ステンシル設定
	D3D11_DEPTH_STENCIL_DESC dsd;
	dsd.DepthEnable = FALSE;
	dsd.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	dsd.DepthFunc = D3D11_COMPARISON_ALWAYS;
	dsd.StencilEnable = FALSE;
	dsd.StencilReadMask = 0xFF;
	dsd.StencilWriteMask = 0xFF;
	dsd.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsd.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsd.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsd.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	dsd.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsd.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsd.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsd.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	HRESULT hr = DxSystem::device->CreateDepthStencilState(&dsd, &depthStencilState);
	if (FAILED(hr))
	{
		return;
	}

	//スプライト用シェーダー

	shader = new Shader();

	shader->Create(L"Shaders/Sprite.fx", "VSMain", "PSMain");

	shaderPMA = new Shader();

	shaderPMA->Create(L"Shaders/Sprite.fx", "VSMain", "PSPMA");

}



void Sprite::Render(
	VECTOR2 position, 
	VECTOR2 scale,
	VECTOR2 texPos,
	VECTOR2 texSize,
	VECTOR2 center, 
	VECTOR4 color,
	float angle )
{
	//スケール０なら描画しない
	if (scale.x * scale.y == 0.0f) return;


	
	shader->Activate();

	D3D11_VIEWPORT viewport;
	UINT numViewports = 1;
	DxSystem::deviceContext->RSGetViewports(&numViewports, &viewport);

	VERTEX vertices[] = {
		VECTOR3(-0.0f, +1.0f, 0), VECTOR3(0,0,1), VECTOR2(0, 1),color, //左上
		VECTOR3(+1.0f, +1.0f, 0), VECTOR3(0,0,1), VECTOR2(1, 1),color, //右上
		VECTOR3(-0.0f, -0.0f, 0), VECTOR3(0,0,1), VECTOR2(0, 0),color, //左下
		VECTOR3(+1.0f, -0.0f, 0), VECTOR3(0,0,1), VECTOR2(1, 0),color, //右下
	};
	
	float tw = texSize.x;
	float th = texSize.y;

	float sinValue = sinf(angle);
	float cosValue = cosf(angle);
	float mx = (tw * scale.x) / tw * center.x;
	float my = (th * scale.y) / th * center.y;

	//NDC変換
	for (int i = 0; i < 4; i++)
	{
		vertices[i].position.x *= (tw * scale.x);
		vertices[i].position.y *= (th * scale.y);

		vertices[i].position.x -= mx;
		vertices[i].position.y -= my;

		float rx = vertices[i].position.x;
		float ry = vertices[i].position.y;
		vertices[i].position.x = rx * cosValue - ry * sinValue;
		vertices[i].position.y = rx * sinValue + ry * cosValue;

		vertices[i].position.x += mx;
		vertices[i].position.y += my;

		vertices[i].position.x += (position.x - scale.x * center.x);
		vertices[i].position.y += (position.y - scale.y * center.y);

		vertices[i].position.x = vertices[i].position.x * 2 / viewport.Width - 1.0f;
		vertices[i].position.y = 1.0f - vertices[i].position.y * 2 / viewport.Height;

		vertices[i].texcoord.x = (texPos.x + vertices[i].texcoord.x * tw) / texture->GetWidth();
		vertices[i].texcoord.y = (texPos.y + vertices[i].texcoord.y * th) / texture->GetHeight();
	}


	//頂点データ更新
	DxSystem::deviceContext->UpdateSubresource(vertexBuffer, 0, NULL, vertices, 0, 0);
	
	//	頂点バッファの指定
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	DxSystem::deviceContext->IASetVertexBuffers(
		0, 1, &vertexBuffer, // スロット, 数, バッファ
		&stride,		// １頂点のサイズ
		&offset			// 開始位置
	);
	//プリミティブトポロジー指定
	DxSystem::deviceContext->IASetPrimitiveTopology(
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
	);
	//深度ステンシルステート
	DxSystem::deviceContext->OMSetDepthStencilState(depthStencilState, 0);
	texture->Set();

	//描画
	DxSystem::deviceContext->Draw(4, 0);
}

//テキストアウト
void Sprite::Textout(VECTOR2 pos, VECTOR2 scale, VECTOR4 color, const char* s,...)
{
	char tmp[256];
	vsprintf_s(tmp, s, (char *)(&s + 1));
	std::string str = tmp;
	float tw = static_cast<float>(texture->GetWidth() / 16.0);
	float th = static_cast<float>(texture->GetHeight() / 16.0);
	float cursor = 0.0f;
	for (const auto& c : str)
	{
		Render(VECTOR2(pos.x + cursor, pos.y), scale, VECTOR2(tw*(c & 0x0F), th*(c >> 4)), VECTOR2(tw, th), {}, color, 0);
		cursor += tw * scale.x;
	}
}





void Sprite::TextRender(VECTOR2 position, VECTOR2 scale, VECTOR2 center,VECTOR4 color, VECTOR2 texPos, VECTOR2 texSize,float angle)
{


	shader->Activate();

	//スケール０なら描画しない
	if (scale.x * scale.y == 0.0f) return;

	D3D11_VIEWPORT viewport;
	UINT numViewports = 1;
	DxSystem::deviceContext->RSGetViewports(&numViewports, &viewport);

	VERTEX vertices[] = {
		VECTOR3(-0.0f, +1.0f, 0), VECTOR3(0,0,1), VECTOR2(0, 1),color, //左上
		VECTOR3(+1.0f, +1.0f, 0), VECTOR3(0,0,1), VECTOR2(1, 1),color, //右上
		VECTOR3(-0.0f, -0.0f, 0), VECTOR3(0,0,1), VECTOR2(0, 0),color, //左下
		VECTOR3(+1.0f, -0.0f, 0), VECTOR3(0,0,1), VECTOR2(1, 0),color, //右下
	};

	float tw = texSize.x;
	float th = texSize.y;


	float sinValue = sinf(angle);
	float cosValue = cosf(angle);
	float mx = (tw * scale.x) / tw * center.x;
	float my = (th * scale.y) / th * center.y;
	for (int i = 0; i < 4; i++)
	{
		vertices[i].position.x *= (tw * scale.x);
		vertices[i].position.y *= (th * scale.y);

		vertices[i].position.x -= mx;
		vertices[i].position.y -= my;

		float rx = vertices[i].position.x;
		float ry = vertices[i].position.y;
		vertices[i].position.x = rx * cosValue - ry * sinValue;
		vertices[i].position.y = rx * sinValue + ry * cosValue;

		vertices[i].position.x += mx;
		vertices[i].position.y += my;

		vertices[i].position.x += (position.x - scale.x * center.x);
		vertices[i].position.y += (position.y - scale.y * center.y);

		vertices[i].position.x = vertices[i].position.x * 2 / viewport.Width - 1.0f;
		vertices[i].position.y = 1.0f - vertices[i].position.y * 2 / viewport.Height;

		vertices[i].texcoord.x = (texPos.x + vertices[i].texcoord.x * tw) / texture->GetWidth();
		vertices[i].texcoord.y = (texPos.y + vertices[i].texcoord.y * th) / texture->GetHeight();
	}


	//頂点データ更新
	DxSystem::deviceContext->UpdateSubresource(vertexBuffer, 0, NULL, vertices, 0, 0);

	//	頂点バッファの指定
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	DxSystem::deviceContext->IASetVertexBuffers(
		0, 1, &vertexBuffer, // スロット, 数, バッファ
		&stride,		// １頂点のサイズ
		&offset			// 開始位置
	);
	//プリミティブトポロジー指定
	DxSystem::deviceContext->IASetPrimitiveTopology(
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
	);
	//深度ステンシルステート
	DxSystem::deviceContext->OMSetDepthStencilState(depthStencilState, 0);
	texture->Set();

	//描画
	DxSystem::deviceContext->Draw(4, 0);
}



Sprite::~Sprite()
{
	if(vertexBuffer)vertexBuffer->Release();
	if(depthStencilState)depthStencilState->Release();
	if (texture) delete texture;
	if (shaderPMA)delete 	shaderPMA;

	if(shader)delete 	shader;
}


//***************************************
//
//		スプライトデータクラス
//
//***************************************



void SpriteData::Init(Sprite* sprite,
	float texLeft,
	float texTop,
	float texWidth,
	float texHeight,
	float centerX,
	float centerY)
{
	this->sprite = sprite;
	this->texLeft = texLeft;
	this->texTop = texTop;
	this->texWidth = texWidth;
	this->texHeight = texHeight;
	this->centerX = centerX;
	this->centerY = centerY;
}


void SpriteData::Render(VECTOR2 position, VECTOR2 scale, VECTOR4 color, float angle, VECTOR2 center)
{
	if (!this)return;

	if (center == VECTOR2(0, 0))
		center = VECTOR2(centerX, centerY);

	if (sprite)
		sprite->Render(
			position,
			scale,
			VECTOR2(texLeft, texTop),
			VECTOR2(texWidth, texHeight),
			center,
			color,
			angle);
}


void SpriteData::AllInit(Sprite * instance, int sprNum, int row,VECTOR2 size)
{
	SpriteData* spr = this;
	for (int i = 0; i < sprNum; i++) {
		spr->Init(instance, size.x * (i % row), size.y * (i / row), size.x, size.y, size.x/2, size.y/2);
		++spr;
	}
}



