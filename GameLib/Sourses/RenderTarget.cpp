#include "../Headers/DxSystem.h"
#include "../Headers/RenderTarget.h"





//*************************************
//
//レンダーターゲット関連クラス
//
//*************************************
void RenderTarget::Initialize() {

	postShader = new Shader;
	postShader->Create(L"Shaders/PostEffect.fx", "VSMain", "PSMain");

	Vertex v[4] = {
		{  1, 1,0, 0,0,1, 1,0, 0xFFFFFFFF },
		{ -1, 1,0, 0,0,1, 0,0, 0xFFFFFFFF },
		{  1,-1,0, 0,0,1, 1,1, 0xFFFFFFFF },
		{ -1,-1,0, 0,0,1, 0,1, 0xFFFFFFFF }
	};
	// 頂点バッファの生成.
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(Vertex) * 4;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	// サブリソースの設定.
	D3D11_SUBRESOURCE_DATA initData;
	ZeroMemory(&initData, sizeof(D3D11_SUBRESOURCE_DATA));
	initData.pSysMem = v;
	DxSystem::device->CreateBuffer(
		&bd, &initData, &vertexBuffer);

	//	インデックス
	int index[4] = { 0,1,2,3 };
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(int) * 4;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	// サブリソースの設定.
	ZeroMemory(&initData, sizeof(D3D11_SUBRESOURCE_DATA));
	initData.pSysMem = index;
	DxSystem::device->CreateBuffer(&bd, &initData, &indexBuffer);

	{
		// レンダーターゲット設定
		D3D11_TEXTURE2D_DESC td;
		ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
		td.Width = DxSystem::SCREEN_WIDTH;
		td.Height = DxSystem::SCREEN_HEIGHT;
		td.MipLevels = 1;
		td.ArraySize = 1;
		td.Format = DXGI_FORMAT_R8G8B8A8_TYPELESS;
		td.SampleDesc.Count = 1;
		td.SampleDesc.Quality = 0;
		td.Usage = D3D11_USAGE_DEFAULT;
		td.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		td.CPUAccessFlags = 0;
		td.MiscFlags = 0;
		// テクスチャ生成
		HRESULT hr = DxSystem::device->CreateTexture2D(&td, NULL, &texture);
		if (FAILED(hr)) return;

		//	レンダーターゲットビュー
		D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
		memset(&rtvDesc, 0, sizeof(rtvDesc));
		rtvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		hr = DxSystem::device->CreateRenderTargetView(texture, &rtvDesc, &renderTargetView);
	
		// シェーダリソースビューの設定
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		memset(&srvDesc, 0, sizeof(srvDesc));
		srvDesc.Format = rtvDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels = 1;
		// シェーダリソースビューの生成
		hr = DxSystem::device->CreateShaderResourceView(texture, &srvDesc,&shaderResourceView);
		if (FAILED(hr)) return;

		// サンプラステートの設定
		D3D11_SAMPLER_DESC smpDesc;
		memset(&smpDesc, 0, sizeof(smpDesc));
		smpDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		smpDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		smpDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		smpDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		smpDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		smpDesc.MinLOD = 0;
		smpDesc.MaxLOD = D3D11_FLOAT32_MAX;

		// サンプラステート生成
		hr = DxSystem::device->CreateSamplerState(&smpDesc, &samplerState);

	}

	// 深度ステンシル設定
	{
		// 深度ステンシル設定
		D3D11_TEXTURE2D_DESC td;
		ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
		td.Width = DxSystem::SCREEN_WIDTH;
		td.Height = DxSystem::SCREEN_HEIGHT;
		td.MipLevels = 1;
		td.ArraySize = 1;
		td.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		td.SampleDesc.Count = 1;
		td.SampleDesc.Quality = 0;
		td.Usage = D3D11_USAGE_DEFAULT;
		td.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		td.CPUAccessFlags = 0;
		td.MiscFlags = 0;

		// 深度ステンシルテクスチャ生成
		HRESULT hr = DxSystem::device->CreateTexture2D(&td, NULL, &depth);
		if (FAILED(hr)) return;

		// 深度ステンシルビュー設定
		D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
		ZeroMemory(&dsvd, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
		dsvd.Format = td.Format;
		dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		dsvd.Texture2D.MipSlice = 0;

		// 深度ステンシルビュー生成
		hr = DxSystem::device->CreateDepthStencilView(depth, &dsvd, &depthStencilView);
		if (FAILED(hr)) return;
	}
}

//Initializeで作成したテクスチャに描画している
void RenderTarget::Activate()
{
	// レンダーターゲットビュー設定
	DxSystem::deviceContext->OMSetRenderTargets( 1, &renderTargetView, depthStencilView);
	float clearColor[4] = { 0.1f, 0.1f, 0.4f, 1.0f };
	DxSystem::deviceContext->ClearRenderTargetView(renderTargetView, clearColor);
	DxSystem::deviceContext->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	// ビューポート設定
	D3D11_VIEWPORT vp = {
		0,0,
		(float)DxSystem::SCREEN_WIDTH,
		(float)DxSystem::SCREEN_HEIGHT,
		0,1 };
	DxSystem::deviceContext->
		RSSetViewports(1, &vp);
}
//Zバッファのクリア
void RenderTarget::ClearZBuffer()
{
	DxSystem::deviceContext->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}
//画面にレンダリングを戻している
void RenderTarget::Deactivate()
{
	DxSystem::deviceContext->OMSetRenderTargets(1, &DxSystem::renderTargetView, DxSystem::depthStencilView );

}

//描画
void RenderTarget::Render()
{
	postShader->Activate();

	DxSystem::deviceContext->PSSetShaderResources(0, 1, &shaderResourceView);
	DxSystem::deviceContext->PSSetSamplers(0, 1, &samplerState);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	DxSystem::deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer,&stride, &offset);
	DxSystem::deviceContext->IASetIndexBuffer(indexBuffer,DXGI_FORMAT_R32_UINT, 0);
	DxSystem::deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	DxSystem::deviceContext->DrawIndexed(4,0,0);

	DxSystem::deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	
}




