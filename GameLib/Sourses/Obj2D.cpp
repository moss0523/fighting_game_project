#include "../Headers/Obj2D.h"
#include "../Headers/SceneBattle.h"
#include "../GameLib/Headers/GameCamera.h"
#include "../GameLib/Headers/RectRender.h"

void Obj2D::Init()
{

	Clear();



}

void Obj2D::Update()
{

	// 移動関数処理
	if (mover) mover(this);

	// 消去チェック
	if (eraser) {
		if (eraser(this)) Clear();
	}

}

void Obj2D::Render()
{
	if (data) {// Obj2Dのdataメンバにスプライトデータがあれば
		//スクロールするか否か
		if (isScroll)
			data->Render(position - gameCamera->scroll, scale, color, angle, center);
		else
			data->Render(position, scale, color, angle, center);



	}
}
//解放
void Obj2D::Clear()
{
	data = nullptr;
	position = {};
	scale = {};
	size = {};
	speed = {};
	center = {};
	mover = nullptr;
	eraser = nullptr;
	color = VECTOR4(1, 1, 1, 1);
	timer = 0;
	state = 0;
	index = 0;
	type = 0;
	isErase = false;
	isSelect = false;
	isScroll = false;
	moveAngle = 0;
	keepFrame = 0;
	startFrame = 0;
	isFront = false;
	isAdd = false;
	isRect = false;
	dir = 0;
	angle = 0;
	hitRect.size = {};
}

//初期化
void Obj2DManager::Init()
{
	objWork.resize(GetSize());
	for (auto& item : *this) item.Clear();

}

void Obj2DManager::Update()
{
	for (auto& item : *this) item.Update();
}

void Obj2DManager::Render()
{
	auto it = end();
	while (it != begin()) {
		it--;
		it->Render();
	};
}

void Obj2DManager::Render(bool isFront)
{
	if (isFront) {
		auto it = end();
		while (it != begin()) {

			it--;

			if (it->isFront) {
				it->Render();
			}
		}
	}
	else {
		auto it = end();
		while (it != begin()) {

			it--;

			if (!it->isFront) {
				it->Render();
			}
		}
	}
}

Obj2D * Obj2DManager::Create(Mover mover, VECTOR2 pos)
{
	for (auto& item : *this) {
		if (item.mover) continue;
		item.Clear();
		item.mover = mover;
		item.position = pos;
		return &item;
	}
	return nullptr;
}

bool Obj2D::AnimeUpdate(AnimeData* animeData)
{
	if (animeData == nullptr) assert(!"animeUpdate関数でanimeDataがnullptr");

	if (anime.pPrev != animeData)           // アニメデータが切り替わったとき
	{
		anime.pPrev = animeData;
		anime.patNum = 0;	                // 先頭から再生
		anime.frame = 0;
	}

	animeData += anime.patNum;
	data = animeData->data;                 // 現在のパターン番号に該当する画像を設定

	anime.frame++;
	if (anime.frame >= animeData->frame)    // 設定フレーム数表示したら
	{
		anime.frame = 0;
		anime.patNum++;                     // 次のパターンへ
		if ((animeData + 1)->frame < 0)     // 終了コードのとき
		{
			anime.patNum = 0;               // 先頭へ戻る
			return true;
		}
	}

	return false;
}